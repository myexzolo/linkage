<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$userId = isset($_POST['userId'])?$_POST['userId']:"";
$pid = isset($_POST['pid'])?$_POST['pid']:"";
$typeDate = isset($_POST['typeDate'])?$_POST['typeDate']:"";
$dateStart = isset($_POST['dateStart'])?$_POST['dateStart']:"";
$dateEnd = isset($_POST['dateEnd'])?$_POST['dateEnd']:"";

$con = "";

if($userId != ""){
  $con .= " and h.user_id = '$userId' ";
}

if($pid != ""){
  $con .= " and h.pid = '$pid' ";
}

if($typeDate == "1" && $dateStart != ""){
  $con .= " and h.create_date between '$dateStart 00:00:00' and '$dateEnd 23:59:59' ";
}

?>
<div class="box box-primary">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered table-striped table-hover" id="tableDisplay">
          <thead>
            <tr class="text-center">
              <th style="width:60px;vertical-align: middle;">ลำดับ</th>
              <th style="width:160px;">เลขที่ประจำตัวประชาชน</th>
              <th style="vertical-align: middle;">รายการบริการข้อมูล</th>
              <th style="width:160px;vertical-align: middle;">วันที่สืบค้น</th>
              <th style="width:200px;vertical-align: middle;">เจ้าหน้าที่</th>
              <th style="width:250px;vertical-align: middle;">ประเภทการสืบค้น</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $sqls   = "SELECT h.*, u.user_name , p.page_name, p.page_detail
                        FROM t_history_search h , t_user u , t_page p
                        where h.user_id = u.user_id and h.page_id = p.page_id and type_report = 'S' $con
                        ORDER BY h.create_date";
              $querys = DbQuery($sqls,null);
              $json   = json_decode($querys, true);
              $counts = $json['dataCount'];
              $rows   = $json['data'];

              //echo $sqls;

              for($x=0; $x < $counts; $x++)
              {
                $type_search  = $rows[$x]['type_search'];
                $pid          = $rows[$x]['pid'];
                $create_date  = $rows[$x]['create_date'];
                $user_name    = $rows[$x]['user_name'];
                $page_detail  = $rows[$x]['page_detail'];
                $page_name    = $rows[$x]['page_name'];

                if($type_search == "1"){
                  $nameSearch = "เลขที่บัตรประชาชน (MOU)";
                }else{
                  $nameSearch = "บัตรประจำตัวประชาชน (สิทธิประชาชน)";
                }
              ?>
                <tr class="text-center">
                  <td><?=$x+1;?></td>
                  <td style="text-align:center;"><?=$pid;?></td>
                  <td style="text-align:left;"><?=$page_name;?></td>
                  <td style="text-align:center;"><?=DateTimeThai($create_date);?></td>
                  <td style="text-align:left;"><?=$user_name;?></td>
                  <td style="text-align:left;"><?=$nameSearch;?></td>
                </tr>
              <?php
               }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'other': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  });
</script>
