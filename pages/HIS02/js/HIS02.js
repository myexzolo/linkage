$(function () {
  showTable();

});

function showTable()
{
  var userId = $("#userId").val();
  var pid = $("#pid").val();
  var typeDate = $("#typeDate").val();
  var dateStart = "";
  var dateEnd   = "";

  if(typeDate == 1){
    dateStart = dateThToEn($('#dateStart').val(),"dd/mm/yyyy","/");
    dateEnd   = dateThToEn($('#dateEnd').val(),"dd/mm/yyyy","/");
  }
  $.post( "ajax/showTable.php",{userId:userId,pid:pid,typeDate:typeDate,dateStart:dateStart,dateEnd:dateEnd})
  .done(function( data ) {
    $("#showTable").html( data );
  });
}


function changeTypedate(obj)
{
  var val = obj.value;
  if(val == '1'){
    $('#dateStart').removeAttr('disabled');
    $('#dateEnd').removeAttr('disabled');
  }else{
    $('#dateStart').attr('disabled','disabled');
    $('#dateEnd').attr('disabled','disabled');
  }
}
