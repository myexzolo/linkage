<!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบฐานข้อมูลประชาชนและการบริการภาครัฐ(Linkage Center) - รายงานประวัติการพิมพ์ข้อมูล</title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/HIS02.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              รายงานประวัติการพิมพ์ข้อมูล
              <small>HIS02</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active">รายงานประวัติการพิมพ์ข้อมูล</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
              include('../../inc/function/mainFunc.php');
              $dateStart  = DateThai(date("Y/m/01"));
              $dateEnd    = DateThai(date("Y/m/t"));
              $optionUser = getoptionUser("");
            ?>
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-primary">
                  <div class="box-header with-border">
                      <h3 class="box-title">ค้นหาประวัติการพิมพ์ข้อมูล</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>เจ้าหน้าที่</label>
                          <select class="form-control select2" style="width: 100%;" id="userId">
                            <option value="">เจ้าหน้าที่ทั้งหมด</option>
                            <?= $optionUser ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>เลขที่ประจำตัวประชาชน</label>
                          <input class="form-control datepicker" value="" id="pid" type="text">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>ประเภทค้นหา</label>
                          <select class="form-control" style="width: 100%;" id="typeDate" onchange="changeTypedate(this)">
                            <option value="1" >ตามช่วงวันที่</option>
                            <option value="2" >ค้นหาทั้งหมด</option>
                          </select>
                        </div>
                      </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>ตั้งแต่วันที่</label>
                            <input class="form-control datepicker" value="<?= $dateStart ?>" id="dateStart" type="text" data-provide="datepicker" data-date-language="th-th" >
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>ถึงวันที่</label>
                            <input class="form-control datepicker" value="<?= $dateEnd ?>" id="dateEnd" type="text" data-provide="datepicker" data-date-language="th-th" >
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="box-footer with-border" align="center">
                      <button class="btn btn-primary" onclick="showTable()" style="width:80px;">ค้นหา</button>
                  </div>
                </div>
                <div id="showTable"></div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/HIS02.js"></script>
      <script>
      $(function () {
        $('.select2').select2();
      });
      </script>
    </body>
  </html>
