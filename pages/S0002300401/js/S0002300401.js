var result = "";

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });
})

showForm();
function showForm(){
  $.get("ajax/showform.php")
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function checkdataAddress(data, name){
  var val = "";
  if(data != ""){
     val = " " + name + data;
  }
  return val;
}

function subName(name){
  var val = "";
  if(name != "")
  {
      var res = name.split("#");
      var middleName = res[2];
      if(middleName != ""){
         middleName = " " + middleName + " ";
      }else{
        middleName = " ";
      }
      val = res[0] + res[1] + middleName + res[3];
  }
  return val;
}

function readSmartCard(type){
  var pid = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  if(type == "R"){
    $("#pid").val("");
    $("#type_search").val("2");
    pid = "";
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
  }
  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  //console.log(url);
  $.get(url)
  .done(function( data ) {
    console.log(data);
    $.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
    if(data.Status == "Y")
    {

      if(data.data != "" && data.data.info == null)
      {
        result = data;

        var childDateOfBirth = dateThLinkage(data.data.childDateOfBirth);
        result.data.childDateOfBirth = childDateOfBirth;

        var docDate = dateThLinkage(data.data.docDate);
        result.data.docDate = docDate;

        var fatherDateOfBirth = dateThLinkage(data.data.fatherDateOfBirth);
        result.data.fatherDateOfBirth = fatherDateOfBirth;

        $("#personalId").val(data.PersonalID);

        $("#childAge").val(data.data.childAge);
        $("#childDateOfBirth").val(childDateOfBirth);
        //childFirstName
        $("#childFullnameAndRank").val(data.data.childFullnameAndRank);
        //childGenderCode
        $("#childGenderDesc").val(data.data.childGenderDesc);
        //childLastName
        //childMiddleName
        //childNationalityCode
        $("#childNationalityDesc").val(data.data.childNationalityDesc);
        $("#childPID").val(data.data.childPID);
        $("#childPassport").val(data.data.childPassport);
        //childTitleCode
        //childTitleDesc
        $("#docDate").val(docDate);
        $("#docID").val(data.data.docID);
        $("#docPlace").val(data.data.docPlace);
        $("#docPlaceDesc").val(data.data.docPlaceDesc);
        $("#docPlaceProvince").val(data.data.docPlaceProvince);
        $("#docSequence").val(data.data.docSequence);
        $("#docTime").val(data.data.docTime);
        $("#docType").val(data.data.docType);
        $("#fatherAge").val(data.data.fatherAge);
        $("#fatherDateOfBirth").val(fatherDateOfBirth);
        //fatherFirstName
        $("#fatherFullnameAndRank").val(data.data.fatherFullnameAndRank);
        //fatherLastName
        //fatherMiddleName
        //fatherNationalityCode
        $("#fatherNationalityDesc").val(data.data.fatherNationalityDesc);
        $("#fatherPassport").val(data.data.fatherPassport);
        //fatherTitleCode
        //fatherTitleDesc

        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();
        var pid          = $("#personalId").val();
        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});
      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      if(data.Message == ""){
        data.Message = "ไม่พบข้อมูล";
      }
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
      reset();
    }
  })
  .fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  });
}

function reset()
{
  $("#pid").val("");
  $("#personalId").val("");

  $("#childAge").val("");
  $("#childDateOfBirth").val("");
  $("#childFullnameAndRank").val("");
  $("#childGenderDesc").val("");
  $("#childNationalityDesc").val("");
  $("#childPID").val("");
  $("#childPassport").val("");
  $("#docDate").val("");
  $("#docID").val("");
  $("#docPlace").val("");
  $("#docPlaceDesc").val("");
  $("#docPlaceProvince").val("");
  $("#docSequence").val("");
  $("#docTime").val("");
  $("#docType").val("");
  $("#fatherAge").val("");
  $("#fatherDateOfBirth").val("");
  $("#fatherFullnameAndRank").val("");
  $("#fatherNationalityDesc").val("");
  $("#fatherPassport").val("");

  $("#type_search").val("");

  result = "";
}

function printPdf()
{
  //console.log(result);
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}
