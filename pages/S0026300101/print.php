  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $result         = "";
  $personalId     = "";
  $citizen_id     = "";
  $member_name    = "";
  $current_a      = "";
  $current_b      = "";
  $remark         = "";
  $summit_a       = "";
  $summit_b       = "";

  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];


  if(isset($_POST['result']))
  {

    $result     = json_decode($_POST['result'], true);
    //print_r($result);

    if(isset($result['data']))
    {

      $personalId     = $result['PersonalID'];
      $citizen_id     = $result['data']['citizen_id'];
      $member_name    = $result['data']['member_name'];
      $current_a      = $result['data']['current_a'];
      $current_b      = $result['data']['current_b'];
      $remark         = $result['data']['remark'];
      $summit_a       = $result['data']['summit_a'];
      $summit_b       = $result['data']['summit_b'];

      $page_id        = $result['page_id'];
      $type_search    = $result['type_search'];

      if($personalId != "")
      {
            $sql = "INSERT INTO t_history_search
            (user_id,pid,page_id,type_search,type_report)
            VALUES
            ('$user_id','$personalId','$page_id','$type_search','P')";
            DbQuery($sql,null);
      }
    }
  }

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลการนำส่งเงินสะสมและเงินสมทบของสมาชิก</div>
      <br>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td>เลขที่ประจำตัวประชาชน</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $personalId ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล</td>
          <td> : </td>
          <td><?= $member_name ?></td>
        </tr>
        <tr>
          <td>ยอดรวมเงินสะสมปีปัจจุบัน</td>
          <td> : </td>
          <td><?= $current_a ?></td>
        </tr>
        <tr>
          <td>ยอดรวมเงินสมทบปีปัจจุบัน</td>
          <td> : </td>
          <td><?= $current_b ?></td>
        </tr>
        <tr>
          <td>ยอดรวมเงินสะสมยกมา</td>
          <td> : </td>
          <td><?= $summit_a ?></td>
        </tr>
        <tr>
          <td>ยอดรวมเงินสมทบ</td>
          <td> : </td>
          <td><?= $summit_b ?></td>
        </tr>
        <tr>
          <td>หมายเหตุ (ข้อมูลรายละเอียดเพิ่มเติม)</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $remark ?></td>
        </tr>
      </table>
      <div style="height:100px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $member_name ?> )</td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลการนำส่งเงินสะสมและเงินสมทบของสมาชิก');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
