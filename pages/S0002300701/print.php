  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';
  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];
  $fullname    = "";

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
  ?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลทะเบียนบ้าน (รายการคนในบ้าน)</div>
      <br>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
        <thead>
          <tr class="text-center">
            <th class="thStyle" style="width:55px;">ลำดับ</th>
            <th class="thStyle" style="width:160px;">เลขที่ประจำตัวประชาชน</th>
            <th class="thStyle">ชื่อ - สกุล</th>
            <th class="thStyle" style="width:55px;">เพศ</th>
            <th class="thStyle" style="width:55px;">อายุ</th>
            <th class="thStyle" style="width:200px;">สถานภาพบุคคล</th>
          </tr>
        </thead>
        <tbody>
          <?php
          //echo $_POST['result'];
          if($_POST['result'] != "")
          {
            $result = json_decode($_POST['result'], true);

            $personalId     = $result['PersonalID'];
            $page_id        = $result['page_id'];
            $type_search    = $result['type_search'];

            if(isset($result['data'])){

              $data   = $result['data'];
              $personalInHouse  = $data['personalInHouse'];

              for($i = 0; $i < count($personalInHouse); $i++)
              {
                  if($personalInHouse[$i]['personalID'] == $personalId){
                    $fullname = $personalInHouse[$i]['fullName'];
                  }
              ?>
                <tr class="text-center">
                  <td class="content" align="center"><?=$i+1;?></td>
                  <td class="content" align="center"><?= $personalInHouse[$i]['personalID'];?></td>
                  <td class="content" style="text-align:left;"><?=$personalInHouse[$i]['fullName'];?></td>
                  <td class="content" align="center"><?= $personalInHouse[$i]['gender'];?></td>
                  <td class="content" align="center"><?= $personalInHouse[$i]['age'];?></td>
                  <td class="content" style="text-align:left;"><?= $personalInHouse[$i]['statusOfPersonDesc'];?></td>
                </tr>
              <?php
                }

                if($personalId != "")
                {
                      $sql = "INSERT INTO t_history_search
                      (user_id,pid,page_id,type_search,type_report)
                      VALUES
                      ('$user_id','$personalId','$page_id','$type_search','P')";
                      DbQuery($sql,null);
                }
              }
            }
          ?>
        </tbody>
      </table>
      <div style="height:100px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $fullname ?> )</td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลทะเบียนบ้าน (รายการคนในบ้าน)');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
