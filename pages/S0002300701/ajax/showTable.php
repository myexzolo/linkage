<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<div class="box-body">
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:80px;">ลำดับ</th>
      <th style="width:200px;">เลขที่ประจำตัวประชาชน</th>
      <th>ชื่อ - สกุล</th>
      <th style="width:100px;">เพศ</th>
      <th style="width:80px;">อายุ</th>
      <th style="width:20%;">สถานภาพบุคคล</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //echo $_POST['result'];
    if($_POST['result'] != "")
    {
      $result = json_decode($_POST['result'], true);
      $data   = $result['data'];
      $personalInHouse  = $data['personalInHouse'];

      for($i = 0; $i < count($personalInHouse); $i++)
      {

      ?>
        <tr class="text-center">
          <td><?=$i+1;?></td>
          <td><?= $personalInHouse[$i]['personalID'];?></td>
          <td style="text-align:left;"><?=$personalInHouse[$i]['fullName'];?></td>
          <td><?= $personalInHouse[$i]['gender'];?></td>
          <td><?= $personalInHouse[$i]['age'];?></td>
          <td><?= $personalInHouse[$i]['statusOfPersonDesc'];?></td>
        </tr>
      <?php
        }
      }
    ?>
  </tbody>
</table>
</div>
<div class="box-footer">
  <div class="col-md-12" style="text-align:center">
    <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
    <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
    <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
  </div>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
