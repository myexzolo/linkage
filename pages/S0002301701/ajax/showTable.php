<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">ลำดับ</th>
      <th>เลขประจำตัวประชาชน</th>
      <th>ชื่อ-นามสกุล</th>
      <th>วันเดือนปี เกิด</th>
      <th>เพศ</th>
      <th>สัญชาติ</th>
      <th>ชื่อบิดา</th>
      <th>ชื่อมารดา</th>
      <th>ที่อยู่</th>
      <th>สาเหตุการเปลี่ยนชื่อสกุล</th>
      <th>สนท.ที่ออกหนังสือ</th>
      <th>วันเดือนปี ใบคำขอ</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //echo $_POST['result'];
    if($_POST['result'] != "")
    {
      $result = json_decode($_POST['result'], true);
      $data   = $result['data'];
      for($i = 0; $i < count($data); $i++)
      {
          $allName = $data[$i]['allName'];

          // allName.age
          // allName.amphorDesc
          // allName.coFirstName
          // allName.coFullNameAndRank
          // allName.coLastName
          // allName.coMiddleName
          // allName.coTitleCode
          // allName.coTitleDesc
          // allName.dateOfBirth
          // allName.districtDesc
          // allName.docDate
          // allName.docID
          // allName.docPlace
          // allName.docPlaceDesc
          // allName.docPlaceProvince
          // allName.fatherFirstName
          // allName.firstName
          // allName.fullNameAndRank
          // allName.genderCode
          // allName.genderDesc
          // allName.hno
          // allName.hrcode
          // allName.hrcodeDesc
          // allName.lastName
          // allName.middleName
          // allName.motherFirstName
          // allName.nationalityCode
          // allName.nationalityDesc
          // allName.newName
          // allName.pid
          // allName.provinceDesc
          // allName.requestDate
          // allName.requestID
          // allName.requestYear
          // allName.soiDesc
          // allName.thanonDesc
          // allName.titleCode
          // allName.titleDesc
          // allName.trokDesc
          // total


          $address = "";

          if($allName['hno'] != "")
          {
            $address .= "เลขที่ ".$allName['hno'];
          }

          if($allName['trokDesc'] != "")
          {
            $address .= " ".$allName['trokDesc'];
          }

          if($allName['soiDesc'] != "")
          {
            $address .= " ".$allName['soiDesc'];
          }

          if($allName['thanonDesc'] != "")
          {
            $address .= " ".$allName['thanonDesc'];
          }

          if($allName['districtDesc'] != "")
          {
            $address .= " ".$allName['districtDesc'];
          }

          if($allName['amphorDesc'] != "")
          {
            $address .= " ".$allName['amphorDesc'];
          }

          if($allName['provinceDesc'] != "")
          {
            $address .= " ".$allName['provinceDesc'];
          }

      ?>
        <tr class="text-center">
          <td><?=$i+1;?></td>
          <td><?= $allName['pid'];?></td>
          <td style="text-align:left;"><?= $allName['fullNameAndRank'] ?></td>
          <td style="text-align:left;"><?= dateThLinkage2($allName['dateOfBirth']) ?></td>
          <td ><?= $allName['genderDesc'];?></td>
          <td ><?= $allName['nationalityDesc'];?></td>
          <td ><?= $allName['fatherFirstName'];?></td>
          <td ><?= $allName['motherFirstName'];?></td>
          <td style="text-align:left;"><?= $address;?></td>
          <td ><?= $allName['causeOfChange'];?></td>
          <td><?= $allName['docPlaceDesc'];?></td>
          <td><?= dateThLinkage2($allName['requestDate']) ?></td>
        </tr>
      <?php
        }
      }
    ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
