<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">ลำดับ</th>
      <th>เลขประจำตัวประชาชน บุตรบุญธรรม</th>
      <th>ชื่อ บุตรบุญธรรม</th>
      <th>วันเดือนปี เกิด บุตรบุญธรรม</th>
      <th>เพศ บุตรบุญธรรม</th>
      <th>สัญชาติ บุตรบุญธรรม</th>
      <th>ชื่อ ผู้รับบุตรบุญธรรม</th>
      <th>เลขเลิกรับบุตรบุญธรรม</th>
      <th>สนท.ที่จดทะเบียนเลิกรับบุตร</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //echo $_POST['result'];
    if($_POST['result'] != "")
    {
      $result = json_decode($_POST['result'], true);
      $data   = $result['data'];
      for($i = 0; $i < count($data); $i++)
      {
          $allName = $data[$i]['allName'];

          // allUnadopt.adoptDate
          // allUnadopt.adoptID
          // allUnadopt.adoptPlace
          // allUnadopt.adoptPlaceDesc
          // allUnadopt.adoptPlaceProvince
          // allUnadopt.adoptSequance
          // allUnadopt.childAge
          // allUnadopt.childDateOfBirth
          // allUnadopt.childFirstName
          // allUnadopt.childFullnameAndRank
          // allUnadopt.childGenderCode
          // allUnadopt.childGenderDesc
          // allUnadopt.childLastName
          // allUnadopt.childMiddleName
          // allUnadopt.childNationalityCode
          // allUnadopt.childNationalityDesc
          // allUnadopt.childPID
          // allUnadopt.childPassport
          // allUnadopt.childTitleCode
          // allUnadopt.childTitleDesc
          // allUnadopt.parentAge
          // allUnadopt.parentDateOfBirth
          // allUnadopt.parentFirstName
          // allUnadopt.parentFullnameAndRank
          // allUnadopt.parentGenderCode
          // allUnadopt.parentGenderDesc
          // allUnadopt.parentLastName
          // allUnadopt.parentMiddleName
          // allUnadopt.parentNationalityCode
          // allUnadopt.parentNationalityDesc
          // allUnadopt.parentPID
          // allUnadopt.parentPassport
          // allUnadopt.parentTitleCode
          // allUnadopt.parentTitleDesc
          // allUnadopt.unadoptDate
          // allUnadopt.unadoptID
          // allUnadopt.unadoptPlace
          // allUnadopt.unadoptPlaceDesc
          // allUnadopt.unadoptPlaceProvince
          // total


      ?>
        <tr class="text-center">
          <td><?=$i+1;?></td>
          <td><?= $allName['childPID'];?></td>
          <td style="text-align:left;"><?= $allName['childFullnameAndRank'] ?></td>
          <td style="text-align:left;"><?= dateThLinkage2($allName['childDateOfBirth']) ?></td>
          <td ><?= $allName['childGenderDesc'];?></td>
          <td ><?= $allName['childNationalityDesc'];?></td>
          <td ><?= $allName['parentFullnameAndRank'];?></td>
          <td ><?= $allName['unadoptID'];?></td>
          <td ><?= $allName['unadoptPlaceDesc'];?></td>
        </tr>
      <?php
        }
      }
    ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
