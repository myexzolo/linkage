$(function () {
  showTable("");
});

function showTable(id){
  $.post( "ajax/showTable.php",{agency_id:id})
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function setTableDisplayOther(id)
{
  $.post( "ajax/showAgency.php",{agency_id:id})
  .done(function( data ) {
    $("#tableDisplay_other").html(data);
  });
}
