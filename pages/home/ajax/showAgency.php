<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$agency_id = isset($_POST['agency_id'])?$_POST['agency_id']:"";

?>
<select name="agency_id" class="form-control select2" style="width: 100%;" required onchange="showTable(this.value)">
  <option value="">หน่วยงานทั้งหมด</option>
  <?php
    $str    = '';
    $sqlr   = "SELECT * FROM t_agency where is_active = 'Y' order by agency_seq";
    $queryr = DbQuery($sqlr,null);
    $json   = json_decode($queryr, true);
    $rowr   = $json['data'];

    foreach ($rowr as $value) {
  ?>
  <option value="<?=$value['agency_id']?>" <?=$value['agency_id']==@$agency_id?"selected":""?>><?="({$value['agency_code']}) {$value['agency_name']}"?></option>
  <?php } ?>
</select>
<script>
$(function () {
$('.select2').select2();
});
</script>
