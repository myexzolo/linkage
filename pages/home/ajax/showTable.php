<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$agency_id = isset($_POST['agency_id'])?$_POST['agency_id']:"";

$con = "";

if($agency_id != ""){
  $con = " and p.agency_id = '$agency_id' ";
}
?>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:80px;">ลำดับ</th>
      <th>รายการบริการข้อมูล</th>
      <th style="width:80px;">Office ID</th>
      <th style="width:80px;">Version</th>
      <th style="width:80px;">Service</th>
      <th style="width:80px;">ข้อมูล</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT p.*, a.agency_code FROM t_page p , t_agency a
                where p.module_id = 2 and p.agency_id = a.agency_id $con and p.is_active = 'Y'
                ORDER BY module_id,cast(page_seq as unsigned), page_code asc";
      $querys = DbQuery($sqls,null);
      $json   = json_decode($querys, true);
      $counts = $json['dataCount'];
      $rows   = $json['data'];

      for($x=0; $x < $counts; $x++)
      {
        $agency_code  = $rows[$x]['agency_code'];
        $service_code = $rows[$x]['service_code'];
        $version_code = $rows[$x]['version_code'];
        $page_detail  = $rows[$x]['page_detail'];
        $page_id      = $rows[$x]['page_id'];

        $path = "../".$rows[$x]["page_path"]."/index.php?officeCode=".$agency_code.
                "&serviceCode=".$service_code."&versionCode=".$version_code."&pageDetail=".$page_detail."&pageId=".$page_id;
      ?>
        <tr class="text-center">
          <td><?=$x+1;?></td>
          <td style="text-align:left;"><?=$rows[$x]['page_name'];?></td>
          <td style="text-align:center;"><?=$agency_code;?></td>
          <td style="text-align:center;"><?=$rows[$x]['service_code'];?></td>
          <td style="text-align:center;"><?=$rows[$x]['version_code'];?></td>
          <td>
            <a class="btn_point" onclick="postURL('<?=$path?>')"><i class="fa  fa-search"></i></a>
          </td>
        </tr>
      <?php
       }
    ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'other': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
    setTableDisplayOther(<?=$agency_id?>);
  });
</script>
