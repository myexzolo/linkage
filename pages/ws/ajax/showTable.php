<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>Num</th>
      <th>Code</th>
      <th>รายการบริการข้อมูล</th>
      <th>รายการแสดง</th>
      <th>officeCode</th>
      <th>serviceCode</th>
      <th>versionCode</th>
      <th>Path</th>
      <th>ลำดับการแสดง</th>
      <th>สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
        <th style="width:70px;">Edit</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
        <th style="width:70px;">Del</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT p.*, a.agency_code FROM t_page p , t_agency a
                where p.module_id = 2 and p.agency_id = a.agency_id
                ORDER BY module_id,cast(page_seq as unsigned), page_code asc";
      $querys = DbQuery($sqls,null);
      $json   = json_decode($querys, true);
      $counts = $json['dataCount'];
      $rows   = $json['data'];

      if($counts > 0){
      foreach ($rows as $key => $value) {
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="center"><?=$value['page_code'];?></td>
      <td align="left"><?=$value['page_name'];?></td>
      <td align="left"><?=$value['page_detail'];?></td>
      <td align="center"><?=$value['agency_code'];?></td>
      <td align="center"><?=$value['service_code'];?></td>
      <td align="center"><?=$value['version_code'];?></td>
      <td  align="center"><?=$value['page_path'];?></td>
      <td><?=$value['page_seq'];?></td>
      <td><?=$value['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['page_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="delModule('<?=$value['page_id']?>','<?=$value['page_name']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
  <?php } } ?>
  </tbody>
</table>
<script>
$(function () {
  $('#tableDisplay').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : true,
    'ordering'    : false,
    'info'        : true,
    'autoWidth'   : false
  })
})
</script>
