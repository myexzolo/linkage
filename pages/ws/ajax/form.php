<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id = $_POST['id'];
$page_icon    = 'fa fa-circle-o';
$countNum = 1;
if($action == 'EDIT'){
  $btn = 'Update changes';


  $sql   = "SELECT * FROM t_page WHERE page_id = '$id' ORDER BY page_id DESC";
  $query = DbQuery($sql,null);
  $json   = json_decode($query, true);
  $counts = $json['dataCount'];
  $row    = $json['data'];

  $page_code    = $row[0]['page_code'];
  $page_name    = $row[0]['page_name'];
  $page_icon    = $row[0]['page_icon'];
  $page_path    = $row[0]['page_path'];
  $page_detail  = $row[0]['page_detail'];
  $module_id    = $row[0]['module_id'];
  $page_seq     = $row[0]['page_seq'];
  $is_active    = $row[0]['is_active'];
  $page_detail  = $row[0]['page_detail'];
  $agency_id    = $row[0]['agency_id'];
  $service_code  = $row[0]['service_code'];
  $version_code  = $row[0]['version_code'];

  $sqlc   = "SELECT COUNT(page_id) AS countnum FROM t_page WHERE module_id = '$module_id'";
  $queryc = DbQuery($sqlc,null);
  $json   = json_decode($queryc, true);
  $rowc   = $json['data'];

  //echo $sqlc;

  $countNum  = $rowc[0]['countnum'];

}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="page_id" value="<?=@$id?>">
<input value="<?=@$page_icon?>" name="page_icon" type="hidden">
<input value="<?=@$page_detail?>" name="page_detail" type="hidden">
<input value="2" name="module_id" type="hidden">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>Service Code</label>
        <input value="<?=@$page_code?>" name="page_code" type="text" maxlength="10" class="form-control" placeholder="Code" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ลำดับการแสดง</label>
        <input value="<?=@$page_seq?>" name="page_seq" type="number" max="<?=$countNum?>" min="1" class="form-control" placeholder="Sequence" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Page Path</label>
        <input value="<?=@$page_path?>" name="page_path" type="text" class="form-control" placeholder="Page Path" required>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>รายการบริการข้อมูล</label>
        <input value="<?=@$page_name?>" name="page_name" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>รายการแสดงบริการข้อมูล</label>
        <input value="<?=@$page_detail?>" name="page_detail" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>หน่วยงาน</label>
        <select name="agency_id" class="form-control select2" style="width: 100%;" required>
          <option value="">&nbsp;</option>
          <?php
            $str    = '';
            $sqlr   = "SELECT * FROM t_agency where is_active = 'Y' order by agency_seq";
            $queryr = DbQuery($sqlr,null);
            $json   = json_decode($queryr, true);
            $rowr   = $json['data'];

            foreach ($rowr as $value) {
          ?>
          <option value="<?=$value['agency_id']?>" <?=$value['agency_id']==@$agency_id?"selected":""?>><?="({$value['agency_code']}) {$value['agency_name']}"?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>serviceCode</label>
        <input value="<?=@$service_code?>" maxlength="3" name="service_code" type="text" class="form-control" placeholder="Service Code" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>versionCode</label>
        <input value="<?=@$version_code?>" maxlength="2" name="version_code" type="text" class="form-control" placeholder="Version Code" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>

<script>
$(function () {
  $('.select2').select2();
})

</script>
