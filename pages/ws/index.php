<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>กรมพลศึกษา - จัดการรายการบริการข้อมูล </title>
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <?php
      include("../../inc/css-header.php");
      $_SESSION['RE_URI'] = $_SERVER['REQUEST_URI'];
    ?>
    <link rel="stylesheet" href="css/ws.css">
  </head>
  <body class="hold-transition skin-purple-light  sidebar-mini" onload="showProcessbar();showSlidebar();">
    <div class="wrapper">
      <?php include("../../inc/header.php"); ?>

      <?php include("../../inc/sidebar.php"); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>จัดการรายการบริการข้อมูล</h1>
          <ol class="breadcrumb">
            <li><a href="../../pages/home/"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">รายการบริการข้อมูล</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <?php //include("../../inc/boxes.php"); ?>
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"></h3>
                  <?php if($_SESSION['ROLE_USER']['is_insert'])
                  {
                  ?>
                    <a class="btn btn-social btn-success btnAdd pull-right" style="width:200px;" onclick="showForm('ADD')">
                      <i class="fa fa-plus"></i> รายการบริการข้อมูล
                    </a>
                  <?php
                  }
                  ?>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div id="showTable"></div>
                </div>
              </div>
              <!-- /.box -->

              <!-- Modal -->
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel">จัดการรายการบริการข้อมูล</h4>
                    </div>
                    <form id="formAddPages" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
                    <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                      <div id="show-form"></div>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <!-- /.row -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <?php include("../../inc/footer.php"); ?>
    </div>
    <!-- ./wrapper -->
    <?php include("../../inc/js-footer.php"); ?>
    <script src="js/ws.js"></script>

  </body>
</html>
