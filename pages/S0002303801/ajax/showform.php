<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<div class="box-body no-padding">
  <ul class="users-list clearfix">
<?php
if($_POST['result'] != "")
{

  $result = json_decode($_POST['result'], true);
  $data     = $result['data'];
  $image    = "data:image/png;base64, ".$data['image'];
?>
  <li>
    <img src="<?=$image ?>" id="image" style="height:150px;">
    <span class="users-list-date"><?= $data['mineType'] ?></span>
  </li>
<?php
}else{
?>
  <li>
    <img src="../../image/user1.jpg" id="image" style="height:150px;">
    <span class="users-list-date">ชนิดไฟล์รูปภาพ</span>
  </li>
<?php
}
?>
  </ul>
</div>
