<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">ลำดับ</th>
      <th>เลขที่เอกสาร</th>
      <th>ชื่อ - สกุล</th>
      <th style="width:50px;">เพศ</th>
      <th style="width:60px;">สัญชาติ</th>
      <th >ที่อยู่</th>
      <th >เลขที่ใบอนุญาต</th>
      <th >ชนิดใบอนุญาต</th>
      <th >วันที่ออกใบอนุญาต</th>
      <th >วันที่สิ้นอายุใบอนุญาต</th>

    </tr>
  </thead>
  <tbody>
    <?php
    //echo $_POST['result'];
    if($_POST['result'] != "")
    {
      $result = json_decode($_POST['result'], true);
      $data   = $result['data'];



      for($i = 0; $i < count($data); $i++)
      {
          $driverLicenceInfo = $data[$i]['driverLicenceInfo'];
          // print_r($driverLicenceInfo);
          $gender = "";
          if($driverLicenceInfo['sex'] == '1')
          {
            $gender = "ชาย";
          }else if($driverLicenceInfo['sex'] == '2'){
            $gender = "หญิง";
          }else if($driverLicenceInfo['sex'] == '0'){
            $gender = "ไม่ระบุ";
          }

          $address = " ";
          if($driverLicenceInfo['bldName'] != "")
          {
            $address .= $driverLicenceInfo['bldName']." ";
          }

          if($driverLicenceInfo['villageName'] != "")
          {
            $address .= $driverLicenceInfo['villageName']." ";
          }

          if($driverLicenceInfo['villageNo'] != "")
          {
            $address .= "หมู่ที่ ".$driverLicenceInfo['villageNo']." ";
          }

          if($driverLicenceInfo['soi'] != "")
          {
            $address .= $driverLicenceInfo['soi']." ";
          }

          if($driverLicenceInfo['street'] != "")
          {
            $address .= $driverLicenceInfo['street']." ";
          }

          $zipCode = "";
          if($driverLicenceInfo['zipCode'] != "")
          {
            $zipCode = " ".$driverLicenceInfo['zipCode'];
          }

      ?>
        <tr class="text-center">
          <td><?=$i+1;?></td>
          <td><?= $driverLicenceInfo['docNo'];?></td>
          <td style="text-align:left;"><?=$driverLicenceInfo['titleDesc']."".$driverLicenceInfo['fName']." ".$driverLicenceInfo['lName'];?></td>
          <td><?= $gender;?></td>
          <td><?= $driverLicenceInfo['natDesc'];?></td>
          <td style="text-align:left;"><?= $driverLicenceInfo['addrNo'].$address.$driverLicenceInfo['locFullDesc'].$zipCode;?></td>
          <td><?= $driverLicenceInfo['pltNo'];?></td>
          <td style="text-align:left;"><?= $driverLicenceInfo['pltDesc'];?></td>
          <td><?= dateThLinkage2($driverLicenceInfo['issDate']);?></td>
          <td><?= dateThLinkage2($driverLicenceInfo['expDate']);?></td>
        </tr>
      <?php
        }
      }
    ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
