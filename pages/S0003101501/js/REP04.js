var result = "";

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });
});

function readSmartCard(type){
  var pid = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();

  if(type == "R"){
    $("#pid").val("");
    pid = "";
    $("#type_search").val("2");
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
  }

  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  //console.log(url);
  $.get(url)
  .done(function( data ) {
    console.log(data);
    $.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
    if(data.data != "" && data.data.info == null)
    {
      result = data;

      $("#CRIME_NUMBER").val(data.data.CRIME_NUMBER);
      $("#WARRANT_NO").val(data.data.WARRANT_NO);
      $("#WANT_D").val(dateThLinkage(data.data.WANT_D));
      $("#COMPLAIN_DATE").val(dateThLinkage(data.data.COMPLAIN_DATE));
      $("#ID_NO").val(data.data.ID_NO);
      $("#TITLE").val(data.data.TITLE);
      $("#FIRST_NAME").val(data.data.FIRST_NAME);
      $("#LAST_NAME").val(data.data.LAST_NAME);
      $("#BIRTH_DATE").val(dateThLinkage(data.data.BIRTH_DATE));
      $("#NATION").val(data.data.NATION);
      $("#ADDRESS").val(data.data.ADDRESS);
      $("#CRIME_CASE").val(data.data.CRIME_CASE);
      $("#ORG").val(data.data.ORG);

      var page_id      = $("#pageId").val();
      var type_search  = $("#type_search").val();
      var pid          = result.PersonalID;

      $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});

    }else if(data.data.info != null && data.data.info != ""){
      $.smkAlert({text: data.data.info,type: "warning"});
      reset();
    }else{
      $.smkAlert({text: data.Message,type: "warning"});
      reset();
    }

  });
}

function reset()
{
  $("#CRIME_NUMBER").val("");
  $("#WARRANT_NO").val("");
  $("#WANT_D").val("");
  $("#COMPLAIN_DATE").val("");
  $("#ID_NO").val("");
  $("#TITLE").val("");
  $("#FIRST_NAME").val("");
  $("#LAST_NAME").val("");
  $("#BIRTH_DATE").val("");
  $("#NATION").val("");
  $("#ADDRESS").val("");
  $("#CRIME_CASE").val("");
  $("#ORG").val("");
  $("#type_search").val("");
  result = "";
}


// function printPdf()
// {
//   console.log(result);
//   var pram = "?result="+ JSON.stringify(result);
//   var url = 'print.php'+ pram;
//   postURL_blank(url);
// }

function printPdf()
{
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }
}
