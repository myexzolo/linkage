  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $result         = "";
  $personalId     = "";

  $CRIME_NUMBER   = "";
  $WARRANT_NO     = "";
  $WANT_D         = "";
  $COMPLAIN_DATE  = "";
  $ID_NO          = "";
  $TITLE          = "";
  $FIRST_NAME     = "";
  $LAST_NAME      = "";
  $BIRTH_DATE     = "";
  $NATION         = "";
  $ADDRESS        = "";
  $CRIME_CASE     = "";
  $ORG            = "";
  $type_search    = "";

  $fullname       = str_repeat('&nbsp;', 40);


  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];


  if(isset($_POST['result']))
  {

    $result     = json_decode($_POST['result'], true);
    //print_r($result);

    if(isset($result['data']))
    {
      $personalId     = $result['PersonalID'];

      $houseID        = $result['data']['houseID'];
      $rcodeDesc      = $result['data']['rcodeDesc'];
      $houseTypeDesc  = $result['data']['houseTypeDesc'];


      $CRIME_NUMBER   = $result['data']['CRIME_NUMBER'];
      $WARRANT_NO     = $result['data']['WARRANT_NO'];
      $WANT_D         = dateThLinkage($result['data']['WANT_D']);
      $COMPLAIN_DATE  = dateThLinkage($result['data']['COMPLAIN_DATE']);
      $ID_NO          = $result['data']['ID_NO'];
      $TITLE          = $result['data']['TITLE'];
      $FIRST_NAME     = $result['data']['FIRST_NAME'];
      $LAST_NAME      = $result['data']['LAST_NAME'];
      $BIRTH_DATE     = dateThLinkage($result['data']['BIRTH_DATE']);
      $NATION         = $result['data']['NATION'];
      $ADDRESS        = $result['data']['ADDRESS'];
      $CRIME_CASE     = $result['data']['CRIME_CASE'];
      $ORG            = $result['data']['ORG'];

      $fullname       = $TITLE.$FIRST_NAME." ".$LAST_NAME;


      $page_id                = $result['page_id'];
      $type_search            = $result['type_search'];

      if($personalId != "")
      {
            $sql = "INSERT INTO t_history_search
            (user_id,pid,page_id,type_search,type_report)
            VALUES
            ('$user_id','$personalId','$page_id','$type_search','P')";
            DbQuery($sql,null);
      }
    }
  }

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลหมายจับ สำนักงานตำรวจแห่งชาติ</div>
      <br>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td>คดีที่</td>
          <td> : </td>
          <td><?= $CRIME_NUMBER ?></td>
        </tr>
        <tr>
          <td>หมายจับที่</td>
          <td> : </td>
          <td><?= $WARRANT_NO ?></td>
        </tr>
        <tr>
          <td>วันที่ออกหมายจับ</td>
          <td> : </td>
          <td><?= $WANT_D ?></td>
        </tr>
        <tr>
          <td>วัน เดือน ปี ที่ต้องคดี</td>
          <td> : </td>
          <td><?= $COMPLAIN_DATE ?></td>
        </tr>
        <tr>
          <td>เลขที่ประจำตัวประชาชน</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $personalId ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $fullname ?></td>
        </tr>
        <tr>
          <td>วัน เดือน ปี เกิด</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $BIRTH_DATE ?></td>
        </tr>
        <tr>
          <td>สัญชาติ</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $NATION ?></td>
        </tr>
        <tr>
          <td>ที่อยู่</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $ADDRESS ?></td>
        </tr>
        <tr>
          <td>กระทำความผิดฐาน</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $CRIME_CASE ?></td>
        </tr>
        <tr>
          <td>สถานที่ออกหมายจับ</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $ORG ?></td>
        </tr>
      </table>
      <div style="height:100px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $fullname ?> )</td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลหมายจับ สำนักงานตำรวจแห่งชาติ');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
