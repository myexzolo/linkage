<div class="box-body">
  <div class="row">
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขจดทะเบียนการหย่า</label>
        <input value="" id="divorceID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วันที่จดทะเบียนการหย่า</label>
        <input value="" id="divorceDate" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เวลาจดทะเบียนการหย่า</label>
        <input value="" id="divorceTime" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สนท.ที่จดทะเบียนการหย่า</label>
        <input value="" id="divorcePlaceDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>จังหวัดที่จดทะเบียนการหย่า</label>
        <input value="" id="divorcePlaceProvince" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ประเภทของการหย่า</label>
        <input value="" id="divorceType" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขจดทะเบียนสมรส</label>
        <input value="" id="marryID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วันที่จดทะเบียนสมรส</label>
        <input value="" id="marryDate" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เวลาจดทะเบียนสมรส</label>
        <input value="" id="marryTime" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สนท.ที่จดทะเบียนสมรส</label>
        <input value="" id="marryPlaceDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>จังหวัดที่จดทะเบียนสมรส</label>
        <input value="" id="marryPlaceProvince" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ประเภทของการสมรส</label>
        <input value="" id="marryType" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขประจำตัวประชาชน (ชาย)</label>
        <input value="" id="malePID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ - สกุล (ชาย)</label>
        <input value="" id="maleFullnameAndRank" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>อายุ (ชาย)</label>
        <input value="" id="maleAge" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วัน เดือน ปีเกิด (ชาย)</label>
        <input value="" id="maleDateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สัญชาติ (ชาย)</label>
        <input value="" id="maleNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขประจำตัวประชาชน (หญิง)</label>
        <input value="" id="femalePID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ - สกุล (หญิง)</label>
        <input value="" id="femaleFullnameAndRank" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>อายุ (หญิง)</label>
        <input value="" id="femaleAge" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วัน เดือน ปีเกิด (หญิง)</label>
        <input value="" id="femaleDateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สัญชาติ (หญิง)</label>
        <input value="" id="femaleNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
  </div>
</div>
<div class="box-footer">
    <div class="col-md-12" style="text-align:center">
      <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
      <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
      <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
    </div>
</div>
