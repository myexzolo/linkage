var result = "";

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });

  showForm();
})


function showForm(){
  $.get("ajax/showform.php")
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function subName(name){
  var val = "";
  if(name != "")
  {
      var res = name.split("#");
      var middleName = res[2];
      if(middleName != ""){
         middleName = " " + middleName + " ";
      }else{
        middleName = " ";
      }
      val = res[0] + res[1] + middleName + res[3];
  }
  return val;
}

function readSmartCard(type){
  var pid = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  if(type == "R"){
    $("#pid").val("");
    $("#type_search").val("2");
    pid = "";
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
  }
  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  //console.log(url);
  $.get(url)
  .done(function( data ) {
    console.log(data);
    $.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
    if(data.Status == "Y")
    {

      if(data.data != "" && data.data.info == null)
      {
        result = data;

        var femaleDateOfBirth = dateThLinkage(data.data.femaleDateOfBirth);
        result.data.femaleDateOfBirth = femaleDateOfBirth;

        var maleDateOfBirth = dateThLinkage(data.data.maleDateOfBirth);
        result.data.maleDateOfBirth = maleDateOfBirth;

        var marryDate = dateThLinkage(data.data.marryDate);
        result.data.marryDate = marryDate;

        var divorceDate = dateThLinkage(data.data.divorceDate);
        result.data.divorceDate = divorceDate;


        $("#personalId").val(data.PersonalID);

        $("#femaleAge").val(data.data.femaleAge);
        $("#femaleDateOfBirth").val(femaleDateOfBirth);
        //$("#femaleFirstName").val(data.data.childFullnameAndRank);
        $("#femaleFullnameAndRank").val(data.data.femaleFullnameAndRank);
        //$("#femaleLastName").val(data.data.childFullnameAndRank);
        //$("#femaleMiddleName").val(data.data.childFullnameAndRank);
        //$("#femaleNationalityCode").val(data.data.childFullnameAndRank);
        $("#femaleNationalityDesc").val(data.data.femaleNationalityDesc);
        //$("#femaleOtherDocID").val(data.data.childFullnameAndRank);
        $("#femalePID").val(data.data.femalePID);
        //$("#femaleTitleCode").val(data.data.childFullnameAndRank);
        //$("#femaleTitleDesc").val(data.data.childFullnameAndRank);
        $("#maleAge").val(data.data.maleAge);
        $("#maleDateOfBirth").val(maleDateOfBirth);
        //$("#maleFirstName").val(data.data.childFullnameAndRank);
        $("#maleFullnameAndRank").val(data.data.maleFullnameAndRank);
        //$("#maleLastName").val(data.data.childFullnameAndRank);
        //$("#maleMiddleName").val(data.data.childFullnameAndRank);
        //$("#maleNationalityCode").val(data.data.childFullnameAndRank);
        $("#maleNationalityDesc").val(data.data.maleNationalityDesc);
        //$("#maleOtherDocID").val(data.data.childFullnameAndRank);
        $("#malePID").val(data.data.malePID);
        //$("#maleTitleCode").val(data.data.childFullnameAndRank);
        //$("#maleTitleDesc").val(data.data.childFullnameAndRank);
        $("#marryDate").val(marryDate);
        $("#marryID").val(data.data.marryID);
        //$("#marryPlace").val(data.data.marryPlace);
        $("#marryPlaceDesc").val(data.data.marryPlaceDesc);
        $("#marryPlaceProvince").val(data.data.marryPlaceProvince);
        $("#marryTime").val(data.data.marryTime);
        $("#marryType").val(data.data.marryType);

        $("#divorceDate").val(divorceDate);
        $("#divorceID").val(data.data.divorceID);
        //$("#divorcePlace").val(data.data.divorcePlace);
        $("#divorcePlaceDesc").val(data.data.divorcePlaceDesc);
        $("#divorcePlaceProvince").val(data.data.divorcePlaceProvince);
        $("#divorceTime").val(data.data.divorceTime);
        $("#divorceType").val(data.data.divorceType);




        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();
        var pid          = $("#personalId").val();
        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});
      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      if(data.Message == ""){
        data.Message = "ไม่พบข้อมูล";
      }
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
      reset();
    }
  })
  .fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  });
}

function reset()
{
  $("#pid").val("");
  $("#personalId").val("");

  $("#femaleAge").val("");
  $("#femaleDateOfBirth").val("");
  $("#femaleFullnameAndRank").val("");
  $("#femaleNationalityDesc").val("");
  $("#femalePID").val("");
  $("#maleAge").val("");
  $("#maleDateOfBirth").val("");
  $("#maleFullnameAndRank").val("");
  $("#maleNationalityDesc").val("");
  $("#malePID").val("");
  $("#marryDate").val("");
  $("#marryID").val("");
  $("#marryPlaceDesc").val("");
  $("#marryPlaceProvince").val("");
  $("#marryTime").val("");
  $("#marryType").val("");
  $("#divorceDate").val("");
  $("#divorceID").val("");
  $("#divorcePlaceDesc").val("");
  $("#divorcePlaceProvince").val("");
  $("#divorceTime").val("");
  $("#divorceType").val("");

  $("#type_search").val("");

  result = "";
}

function printPdf()
{
  //console.log(result);
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}
