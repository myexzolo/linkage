<div class="box-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขที่ประจำตัวประชาชน</label>
        <input value="" id="personalId" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ - สกุล</label>
        <input value="" id="fullname" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขสมาชิก</label>
        <input value="" id="cid" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่ รับเป็นสมาชิก</label>
        <input value="" id="approvedate" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ตำแหน่ง</label>
        <input value="" id="position" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานภาพของสมาชิก</label>
        <input value="" id="status" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่ตาย / ให้ออก / ลาออก</label>
        <input value="" id="outdate" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
  </div>
</div>
<div class="box-footer">  
    <div class="col-md-12" style="text-align:center">
      <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
      <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
      <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
    </div>
  </div>
