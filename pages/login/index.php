<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบฐานข้อมูลประชาชนและการบริการภาครัฐ(Linkage Center) - เข้าสู่ระบบ</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php include('../../inc/css-header.php'); session_destroy(); ?>
  <link rel="stylesheet" href="css/login.css">

</head>
<body class="hold-transition login-page" onload="showProcessbar();">
<div class="login-box">
  <div class="login-logo">

  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <div align="center">
    <img src="../../image/logo.png" style="height:160px;">
    <h2 style="color:#111;">ระบบฐานข้อมูลประชาชน<br>และการบริการภาครัฐ(Linkage Center)</h2>
    <br>
    </div>

    <p class="login-box-msg">ล็อกอินเข้าสู่ระบบ</p>
    <form id="formLogin" autocomplete="off" novalidate >
    <!-- <form method="post" action="ajax/AEDLogin.php" data-smk-icon="glyphicon-remove-sign" novalidate> -->
        <div class="row">
          <!-- /.col -->
          <div class="col-md-12" style="height:60px;">
            <button type="submit" id="sendForm" class="btn" >เข้าสู่ระบบ</button>
          </div>
          <div class="col-md-12">
            <!-- <a href="../index" class="pull-left" style="margin-top:5px;color:#8A2BE2">ย้อนกลับ</a> -->
            <a onclick="openDownload()" class="pull-right btn_point" style="margin-top:5px;">Download โปรแกรมและคู่มือการใช้งาน</a>
          </div>
          <!-- /.col -->
        </div>
    </form>

    <div class="modal fade bs-example-modal" id="myModalDownload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">DownLoad</h4>
          </div>
            <div class="modal-body">
              <table id="tableExcel" class="table table-bordered table-striped">
              <thead>
                <tr>
                    <th data-align="center">ลำดับ</th>
                    <th data-align="center">รายการ</th>
                    <th data-align="center">DownLoad</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                    <td style="text-align: center;">1</td>
                    <td>โปรแกรม Agent</td>
                    <td style="text-align: center;">
                      <a class="btn" onclick="postURL_blank('../../upload/AgentReadSmartCard.exe')"><i class="fa fa-file-archive-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr>
                    <td align="center">2</td>
                    <td>.Net framework 4.5</td>
                    <td align="center">
                        <a class="btn" onclick="postURL_blank('../../upload/dotnetfx45_full_x86_x64.exe')"><i class="fa fa-file-archive-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
                <tr style="display:none">
                    <td align="center">3</td>
                    <td>คู่มือการใช้งาน</td>
                    <td align="center">
                        <a class="btn" onclick=""><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
                    </td>
                </tr>
              </tbody>
              </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
            </div>
        </div>
      </div>
    </div>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<?php include('../../inc/js-footer.php'); ?>
<script src="js/login.js"></script>
</body>
</html>
