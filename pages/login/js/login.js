// JS Login //
// showForm('LOGIN');
//
// function showForm(action){
//   $.post("ajax/formLogin.php",{action:action})
//     .done(function( data ) {
//       $('#action').remove();
//       $('#show-form').html(data);
//   });
// }


function openDownload(){
  $('#myModalDownload').modal({backdrop:'static'});
}


$('#formLogin').on('submit', function(event) {
  event.preventDefault();
  $('.icons').remove('span');
  var flag = false;
  var url = "http://localhost:20001/Readcard/?type=login";
  var idcard =  "";
  var firstName =  "";
  var lastName =  "";
  $.get(url)
    .done(function( data ) {
      if(data.Status == "Y")
      {
        idcard = data.data.personalId;
        firstName = data.data.firstName;
        lastName = data.data.lastName;
        flag = true;
      }else{
        if(data.Message != ""){
          $.smkAlert({text: data.Message,type: "danger"});
        }

        // cuteAlert({
        //   type: "error",
        //   title: "Error",
        //   message: data.Message,
        //   buttonText: "ตกลง"
        // });
      }
  }).fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  }).always(function() {
    // console.log(flag);
    if(flag){
      $.post("ajax/AEDLogin.php",{idcard:idcard,firstName:firstName,lastName:lastName})
        .done(function( data ) {
            $.smkAlert({text: data.message,type: data.status});
            if(data.status == 'success'){
              window.location = '../../pages/home/';
            }
      }).fail(function (jqXHR, textStatus) {
          console.log(jqXHR);
      });
    }
  });
});
