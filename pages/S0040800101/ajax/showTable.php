<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">ลำดับ</th>
      <th>ชื่อมูลนิธิ</th>
      <th >ชื่อ-นามสกุลผู้ลงนาม</th>
      <th>เลขที่ใบจัดตั้ง</th>
      <th>ประเภทใบจัดตั้ง</th>
      <th>ตำแหน่ง</th>
      <th>วันที่ออกใบจัดตั้ง</th>
      <th>เลขรหัสประจำบ้าน</th>
      <th>ที่อยู่</th>
      <th>สำนักทะเบียน</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //echo $_POST['result'];
    if($_POST['result'] != "")
    {
      $result = json_decode($_POST['result'], true);
      $data   = $result['data'];
      for($i = 0; $i < count($data); $i++)
      {
          $allName = $data[$i]['allName'];

          $address = "";

          if($allName['hno'] != "")
          {
            $address .= "เลขที่ ".$allName['hno'];
          }

          if($allName['trok'] != "")
          {
            $address .= " ".$allName['trok'];
          }

          if($allName['soi'] != "")
          {
            $address .= " ".$allName['soi'];
          }

          if($allName['thanon'] != "")
          {
            $address .= " ".$allName['thanon'];
          }

          if($allName['districtDesc'] != "")
          {
            $address .= " ".$allName['districtDesc'];
          }

          if($allName['amphorDesc'] != "")
          {
            $address .= " ".$allName['amphorDesc'];
          }

          if($allName['provinceDesc'] != "")
          {
            $address .= " ".$allName['provinceDesc'];
          }

      ?>
        <tr class="text-center">
          <td><?=$i+1;?></td>
          <td><?= $allName['associationName'];?></td>
          <td style="text-align:left;"><?=$allName['signTitleDesc']."".$allName['signFullName']?></td>
          <td style="text-align:left;"><?= $allName['docID'];?></td>
          <td style="text-align:left;"><?= $allName['associationType'];?></td>
          <td style="text-align:left;"><?= $allName['associationPosition'];?></td>
          <td><?= dateThLinkage2($allName['docDate']);?></td>
          <td><?= $allName['hid'];?></td>
          <td style="text-align:left;"><?= $address;?></td>
          <td><?= $allName['docPlaceDesc'];?></td>
        </tr>
      <?php
        }
      }
    ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
