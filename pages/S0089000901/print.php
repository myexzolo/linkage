  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $result           = "";
  $personalId       = "";

  $BIRTHDAY           = "";
  $EXPIRED_DATE       = "";
  $fullname           = "";
  $ID_CARD            = "";
  $ISSUSE_CARD_DATE   = "";
  $SKILL_HAND_CENTER  = "";
  $SKILL_HAND_NORTH   = "";
  $SKILL_HAND_NORTHEAST   = "";
  $SKILL_HAND_OTHER       = "";
  $SKILL_HAND_OTHER_TEXT  = "";
  $SKILL_HAND_SOUTH       = "";
  $SKILL_HAND_THAI        = "";


  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];




  if(isset($_POST['result']))
  {

    $result     = json_decode($_POST['result'], true);

    if(isset($result['data']))
    {
      $personalId     = $result['PersonalID'];
      if(isset($result['data']['result']))
      {
        $res = $result['data']['result'];

        $BIRTHDAY           = dateThLinkage($res['BIRTHDAY']);
        $EXPIRED_DATE       = dateThLinkage($res['EXPIRED_DATE']);
        $fullname           = $res['FIRST_NAME_THAI']." ".$res['LAST_NAME_THAI'];
        $ID_CARD            = $res['ID_CARD'];
        $ISSUSE_CARD_DATE   = dateThLinkage($res['ISSUSE_CARD_DATE']);
        $SKILL_HAND_CENTER  = $res['SKILL_HAND_CENTER'];
        $SKILL_HAND_NORTH   = $res['SKILL_HAND_NORTH'];
        $SKILL_HAND_NORTHEAST   = $res['SKILL_HAND_NORTHEAST'];
        $SKILL_HAND_OTHER       = $res['SKILL_HAND_OTHER'];
        $SKILL_HAND_OTHER_TEXT  = $res['SKILL_HAND_OTHER_TEXT'];
        $SKILL_HAND_SOUTH       = $res['SKILL_HAND_SOUTH'];
        $SKILL_HAND_THAI        = $res['SKILL_HAND_THAI'];


      }

      $page_id                = $result['page_id'];
      $type_search            = $result['type_search'];

      if($personalId != "")
      {
            $sql = "INSERT INTO t_history_search
            (user_id,pid,page_id,type_search,type_report)
            VALUES
            ('$user_id','$personalId','$page_id','$type_search','P')";
            DbQuery($sql,null);
      }
    }
  }

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลล่ามภาษามือ</div>
      <br>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td>เลขที่ประจำตัวประชาชน</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $personalId ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล</td>
          <td> : </td>
          <td><?= $fullname ?></td>
        </tr>
        <tr>
          <td>วันเดือนปี เกิด</td>
          <td> : </td>
          <td><?= $BIRTHDAY ?></td>
        </tr>
        <tr>
          <td>ISSUSE_CARD_DATE</td>
          <td> : </td>
          <td><?= $ISSUSE_CARD_DATE ?></td>
        </tr>
        <tr>
          <td>EXPIRED_DATE</td>
          <td> : </td>
          <td><?= $EXPIRED_DATE ?></td>
        </tr>
        <tr>
          <td>SKILL_HAND_CENTER</td>
          <td> : </td>
          <td><?= $SKILL_HAND_CENTER ?></td>
        </tr>
        <tr>
          <td>SKILL_HAND_NORTH</td>
          <td> : </td>
          <td><?= $SKILL_HAND_NORTH ?></td>
        </tr>
        <tr>
          <td>SKILL_HAND_NORTHEAST</td>
          <td> : </td>
          <td><?= $SKILL_HAND_NORTHEAST ?></td>
        </tr>
        <tr>
          <td>SKILL_HAND_OTHER</td>
          <td> : </td>
          <td><?= $SKILL_HAND_OTHER ?></td>
        </tr>
        <tr>
          <td>SKILL_HAND_OTHER_TEXT</td>
          <td> : </td>
          <td><?= $SKILL_HAND_OTHER_TEXT ?></td>
        </tr>
        <tr>
          <td>SKILL_HAND_SOUTH</td>
          <td> : </td>
          <td><?= $SKILL_HAND_SOUTH ?></td>
        </tr>
        <tr>
          <td>SKILL_HAND_THAI</td>
          <td> : </td>
          <td><?= $SKILL_HAND_THAI ?></td>
        </tr>
      </table>
      <div style="height:100px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $fullname ?> )</td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลล่ามภาษามือ');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
