<div class="box-body">
  <div class="row">
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขที่ประจำตัวประชาชน</label>
        <input value="" id="personalId" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อ - สกุล</label>
        <input value="" id="fullname" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วันเดือนปี เกิด</label>
        <input value="" id="BIRTHDAY" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ISSUSE_CARD_DATE</label>
        <input value="" id="ISSUSE_CARD_DATE" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>EXPIRED_DATE</label>
        <input value="" id="EXPIRED_DATE" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>SKILL_HAND_CENTER</label>
        <input value="" id="SKILL_HAND_CENTER" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>SKILL_HAND_NORTH</label>
        <input value="" id="SKILL_HAND_NORTH" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>SKILL_HAND_NORTHEAST</label>
        <input value="" id="SKILL_HAND_NORTHEAST" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>SKILL_HAND_OTHER</label>
        <input value="" id="SKILL_HAND_OTHER" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>SKILL_HAND_OTHER_TEXT</label>
        <input value="" id="SKILL_HAND_OTHER_TEXT" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>SKILL_HAND_SOUTH</label>
        <input value="" id="SKILL_HAND_SOUTH" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>SKILL_HAND_THAI</label>
        <input value="" id="SKILL_HAND_THAI" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
</div>
 <div class="box-footer">  
    <div class="col-md-12" style="text-align:center">
      <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
      <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
      <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
    </div>
</div>
