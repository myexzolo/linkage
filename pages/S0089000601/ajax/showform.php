<div class="box-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขที่ประจำตัวประชาชน</label>
        <input value="" id="personalId" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ - สกุล</label>
        <input value="" id="PERSON_NAME" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันเดือนปี เกิด</label>
        <input value="" id="BIRTH_DATE" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ประเภทความพิการ</label>
        <input value="" id="DEFORM_NAME" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันออกบัตร</label>
        <input value="" id="CARD_ISSUE_DATE" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันหมดอายุบัตร</label>
        <input value="" id="CARD_EXPIRE_DATE" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
  </div>
</div>
 <div class="box-footer">
    <div class="col-md-12" style="text-align:center">
      <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
      <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
      <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
    </div>
</div>
