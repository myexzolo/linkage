<?php
session_start();
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<div class="row">
  <div class="col-md-12">
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="form-group col-md-12">
                    <label class="col-sm-4 control-label">นำเข้า ไฟล์ Excel</label>
                    <div class="col-sm-8">
                      <input type="file" name="filepath" accept=".xlsx,.xls" required>
                    </div>
                  </div>
                  <div class="text-center col-md-12">
                    <button type="submit" class="btn btn-success btn-flat" style="width:80px;">นำเข้า</button>
                  </div>
              </div>
            </div>
          </div>
    </div>
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title">แสดงรายการ</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body" id="historyUpload"></div>
    </div>
  </div>
</div>
<script>
   showHistoryUpload();
</script>
