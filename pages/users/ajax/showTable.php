<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th>No.</th>
      <th>User Login</th>
      <th>ชื่อ</th>
      <th>สถานะ</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:100px;">Re Password</th>
      <th style="width:50px;">Edit</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">Del</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $str    = $_SESSION['member'][0]['user_id'] == 0 ?"":" and user_id > 0 ";
      //print_r($_SESSION['member'][0]);
      $sqls   = "SELECT u.*
                 FROM t_user u
                 where u.is_active  not in ('D','R','C')
                 and type_user in ('ADMIN','USER') $str
                 ORDER BY u.user_id DESC";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$value['user_login']?></td>
      <td align="left"><?=$value['user_name']?></td>
      <td><?=$value['is_active']=='Y'?"ใช้งาน":"ไม่ใช้งาน";?></td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point text-yellow"><i class="fa fa-key" onclick="resetPass('<?=$value['user_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['user_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['user_id']?>','<?=$value['user_login']?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
