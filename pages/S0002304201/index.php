<!DOCTYPE html>
  <?php
  $officeCode   = @$_POST["officeCode"];
  $serviceCode  = @$_POST["serviceCode"];
  $versionCode  = @$_POST["versionCode"];
  $pageDetail   = @$_POST["pageDetail"];
  $pageId       = @$_POST["pageId"];

  $code = $officeCode.$serviceCode.$versionCode;
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบฐานข้อมูลประชาชนและการบริการภาครัฐ(Linkage Center) - <?=$pageDetail ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/S0002304201.css">
    </head>
      <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->

          <section class="content-header">
            <h1>
              <?=$pageDetail ?>
              <small><?=$code ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?=$pageDetail ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
                  <div class="box-header with-border">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>ชื่อ</label>
                          <input value="" id="firstName" style="width:100%" type="text" class="form-control" data-smk-msg="&nbsp;">
                          <input value="<?=$officeCode ?>" id="officeCode" type="hidden">
                          <input value="<?=$serviceCode ?>" id="serviceCode" type="hidden">
                          <input value="<?=$versionCode ?>" id="versionCode" type="hidden">
                          <input value="<?=$pageId ?>" id="pageId" type="hidden">
                          <input value="" id="type_search" type="hidden">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>นามสกุล</label>
                          <input value="" id="lastName" type="text" class="form-control" data-smk-msg="&nbsp;">
                        </div>
                      </div>
                      <div class="col-md-2">
                          <button type="button" class="btn btn-primary btn-flat btn-search" onclick="readData('')" style="width:100px;font-size:20px;height:40px;line-height:20px">ค้นหา</button>
                      </div>
                      <div class="col-md-4 ">
                          <button type="button" class="btn btn-info btn-flat btn-search pull-right" onclick="calRec('M')"
                          style="font-size:20px;height:40px;line-height:0px;width:50px" id="btnRight" disabled>
                            <i class="fa fa-chevron-right"></i>
                          </button>
                          <button type="button" class="btn btn-info btn-flat btn-search pull-right" onclick="calRec('T')"
                          style="font-size:20px;height:40px;line-height:0px;margin-right:5px;width:50px;" id="btnleft" disabled>
                            <i class="fa fa-chevron-left"></i>
                          </button>

                      </div>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div id="show-form"></div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/S0002304201.js"></script>
    </body>
  </html>
