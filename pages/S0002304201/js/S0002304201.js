var result = "";
var record = 1;
var totalRecord = 0;

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });

})

showForm();
function showForm(){
  $.get("ajax/showform.php")
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function calRec(str)
{
  if(str == "M")
  {
      record++;

  }else if(str == "T" && record > 1)
  {
      record--;
  }

  if(totalRecord > 1 & record != totalRecord)
  {
    //console.log("totalRecord > 1 & record != totalRecord");
    $("#btnRight").prop('disabled', false);
  }

  if(record == 1)
  {
    //console.log("record == 1");
    $("#btnleft").prop('disabled', true);
  }

  if(record > 1)
  {
    //console.log("record > 1");
    $("#btnleft").prop('disabled', false);
  }

  if(record == totalRecord)
  {
    //console.log("record == totalRecord");
    $("#btnRight").prop('disabled', true);
  }
  console.log(record + " " + totalRecord);
  readSmartCard("");
}

function readData(type)
{
  record = 1;
  totalRecord = 0;
  readSmartCard(type);
}

function readSmartCard(type){
  var pid = "";

  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  if(type == "R"){
    $("#pid").val("");
    $("#type_search").val("2");
    pid = "";
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    // if(pid == "")
    // {
    //     $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
    //     return;
    // }else if(pid != "" && pid.length < 13){
    //     $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
    //     return;
    // }
  }
  var firstName  = Base64.encode($("#firstName").val());
  var lastName   = Base64.encode($("#lastName").val());

  if(firstName == "" && lastName == ""){
    $.smkAlert({text: "กรุณากรอก ชื่อ และ สกุล",type: "warning"});
    return;
  }

  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();



  var input = "&limit=1&firstName="+firstName+"&lastName="+lastName+"&recordNumber="+record;
  var url = "http://localhost:20001/Readcard/?officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode+input;
  // console.log(url);
  $.get(url)
  .done(function( data ) {
    //console.log(data);
    //$.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
    if(data.Status == "Y")
    {

      if(data.data != "" && data.data.info == null)
      {
        result = data;
        var middleName = data.data.middleName;
        if(middleName != ""){
           middleName = " " + middleName + " ";
        }else{
          middleName = " ";
        }
        var middleName_en = data.data.englishMiddleName;
        if(middleName_en != ""){
           middleName_en = " " + middleName_en + " ";
        }else{
          middleName_en = " ";
        }

        totalRecord = data.data.totalRecord;

        if(totalRecord == 1)
        {
          $("#btnleft").prop('disabled', true);
          $("#btnRight").prop('disabled', true);
        }else if(totalRecord > record)
        {
          $("#btnRight").prop('disabled', false);
        }

        $("#personalId").val(data.data.pid);
        $("#fullname").val(data.data.titleDesc +" "+ data.data.firstName + middleName + data.data.lastName);
        $("#fullname_en").val(data.data.englishTitleDesc +" "+ data.data.englishFirstName + middleName_en + data.data.englishLastName);

        $("#dateOfBirth").val(dateThLinkage(data.data.dateOfBirth));
        $("#dateOfMoveIn").val(dateThLinkage(data.data.dateOfMoveIn));
        $("#age").val(data.data.age);
        $("#genderDesc").val(data.data.genderDesc);
        $("#nationalityDesc").val(data.data.nationalityDesc);
        $("#statusOfPersonDesc").val(data.data.statusOfPersonDesc);
        $("#ownerStatusDesc").val(data.data.ownerStatusDesc);
        $("#fatherPersonalID").val(data.data.fatherPersonalID);
        $("#fatherName").val(data.data.fatherName);
        $("#fatherNationalityDesc").val(data.data.fatherNationalityDesc);
        $("#motherPersonalID").val(data.data.motherPersonalID);
        $("#motherName").val(data.data.motherName);
        $("#motherNationalityDesc").val(data.data.motherNationalityDesc);

        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();
        var pid          = $("#personalId").val();
        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});
      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      if(data.Message == ""){
        data.Message = "ไม่พบข้อมูล";
      }
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
      reset();
    }

  })
  .fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  });
}

function reset()
{
  $("#pid").val("");
  $("#personalId").val("");
  $("#fullname").val("");
  $("#fullname_en").val("");

  $("#dateOfBirth").val("");
  $("#dateOfMoveIn").val("");
  $("#age").val("");
  $("#genderDesc").val("");
  $("#nationalityDesc").val("");
  $("#statusOfPersonDesc").val("");
  $("#ownerStatusDesc").val("");
  $("#fatherPersonalID").val("");
  $("#fatherName").val("");
  $("#fatherNationalityDesc").val("");
  $("#motherPersonalID").val("");
  $("#motherName").val("");
  $("#motherNationalityDesc").val("");
  $("#type_search").val("");

  result = "";
}

function printPdf()
{
  //console.log(result);
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}
