  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $result         = "";
  $middleName     = "";
  $middleName_en  = "";
  $personalId     = "";
  $fullname       = "";
  $fullname_en    = "";
  $dateOfBirth    = "";
  $dateOfMoveIn   = "";
  $age            = "";
  $genderDesc     = "";
  $nationalityDesc      = "";
  $statusOfPersonDesc   = "";
  $ownerStatusDesc      = "";
  $fatherPersonalID     = "";
  $fatherName           = "";
  $fatherNationalityDesc = "";
  $motherPersonalID       = "";
  $motherName     ="";
  $motherNationalityDesc = "";

  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];




  if(isset($_POST['result']))
  {

    $result     = json_decode($_POST['result'], true);
    //print_r($result);

    if(isset($result['data']))
    {
      $middleName = $result['data']['middleName'];
      if($middleName != ""){
         $middleName = " " . $middleName . " ";
      }else{
        $middleName = " ";
      }
      $middleName_en = $result['data']['englishMiddleName'];
      if($middleName_en != ""){
         $middleName_en = " " + $middleName_en + " ";
      }else{
        $middleName_en = " ";
      }


      $personalId     = $result['data']['pid'];
      $fullname       = $result['data']['titleDesc']."".$result['data']['firstName'].$middleName.$result['data']['lastName'];
      $fullname_en    = $result['data']['englishTitleDesc']." ".$result['data']['englishFirstName'].$middleName_en.$result['data']['englishLastName'];
      $dateOfBirth    = dateThLinkage($result['data']['dateOfBirth']);
      $dateOfMoveIn   = dateThLinkage($result['data']['dateOfMoveIn']);
      $age            = $result['data']['age'];
      $genderDesc     = $result['data']['genderDesc'];
      $nationalityDesc      = $result['data']['nationalityDesc'];
      $statusOfPersonDesc   = $result['data']['statusOfPersonDesc'];
      $ownerStatusDesc      = $result['data']['ownerStatusDesc'];
      $fatherPersonalID     = $result['data']['fatherPersonalID'];
      $fatherName           = $result['data']['fatherName'];
      $fatherNationalityDesc  = $result['data']['fatherNationalityDesc'];
      $motherPersonalID       = $result['data']['motherPersonalID'];
      $motherName             = $result['data']['motherName'];
      $motherNationalityDesc  = $result['data']['motherNationalityDesc'];

      $page_id                = $result['page_id'];
      $type_search            = $result['type_search'];

      if($personalId != "")
      {
            $sql = "INSERT INTO t_history_search
            (user_id,pid,page_id,type_search,type_report)
            VALUES
            ('$user_id','$personalId','$page_id','$type_search','P')";
            DbQuery($sql,null);
      }
    }
  }

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลทะเบียนราษฎร</div>
      <br>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td>เลขที่ประจำตัวประชาชน</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $personalId ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล (ภาษาไทย)</td>
          <td> : </td>
          <td><?= $fullname ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล (ภาษาอังกฤษ)</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $fullname_en ?></td>
        </tr>
        <tr>
          <td>วันเดือนปี เกิด</td>
          <td> : </td>
          <td><?= $dateOfBirth ?></td>
        </tr>
        <tr>
          <td>อายุ</td>
          <td> : </td>
          <td><?= $age ?></td>
        </tr>
        <tr>
          <td>เพศ</td>
          <td> : </td>
          <td><?= $genderDesc ?></td>
        </tr>
        <tr>
          <td>สัญชาติ</td>
          <td> : </td>
          <td><?= $nationalityDesc ?></td>
        </tr>
        <tr>
          <td>สถานภาพบุคคล</td>
          <td> : </td>
          <td><?= $statusOfPersonDesc ?></td>
        </tr>
        <tr>
          <td>สถานะภาพเจ้าบ้าน</td>
          <td> : </td>
          <td><?= $ownerStatusDesc ?></td>
        </tr>
        <tr>
          <td>วันเดือนปี ที่ย้ายเข้ามาในบ้าน</td>
          <td> : </td>
          <td><?= $dateOfMoveIn ?></td>
        </tr>
        <tr>
          <td>เลขประจำตัวประชาชน บิดา</td>
          <td> : </td>
          <td><?= $fatherPersonalID ?></td>
        </tr>
        <tr>
          <td>ชื่อบิดา</td>
          <td> : </td>
          <td><?= $fatherName ?></td>
        </tr>
        <tr>
          <td>สัญชาติ บิดา</td>
          <td> : </td>
          <td><?= $fatherNationalityDesc ?></td>
        </tr>
        <tr>
          <td>เลขประจำตัวประชาชน มารดา</td>
          <td> : </td>
          <td><?= $motherPersonalID ?></td>
        </tr>
        <tr>
          <td>ชื่อมารดา</td>
          <td> : </td>
          <td><?= $motherName ?></td>
        </tr>
        <tr>
          <td>สัญชาติ มารดา</td>
          <td> : </td>
          <td><?= $motherNationalityDesc ?></td>
        </tr>
      </table>
      <div style="height:100px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $fullname ?> )</td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลทะเบียนราษฎร');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
