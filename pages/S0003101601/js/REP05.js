var result = "";

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });
});

function readSmartCard(type){
  var pid = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  if(type == "R"){
    $("#pid").val("");
    pid = "";
    $("#type_search").val("2");
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
  }
  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  //console.log(url);
  $.get(url)
  .done(function( data ) {
    console.log(data);
    $.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
    if(data.data != "" && data.data.info == null)
    {
      result = data;

      $("#ADDRESS").val(data.data.ADDRESS);
      $("#CRIME_CASE").val(data.data.CRIME_CASE);
      $("#CRIME_NUMBER").val(data.data.CRIME_NUMBER);
      $("#CRIME_YEAR").val(data.data.CRIME_YEAR);
      $("#FIRST_NAME").val(data.data.FIRST_NAME);
      $("#ID_NO").val(data.data.ID_NO);
      $("#NATION").val(data.data.NATION);
      $("#LAST_NAME").val(data.data.LAST_NAME);

      var page_id      = $("#pageId").val();
      var type_search  = $("#type_search").val();
      var pid          = result.PersonalID;
      //console.log(pid);
      $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});

    }else if(data.data.info != null && data.data.info != ""){
      $.smkAlert({text: data.data.info,type: "warning"});
      reset();
    }else{
      $.smkAlert({text: data.Message,type: "warning"});
      reset();
    }

  });
}

function reset()
{
  $("#ADDRESS").val("");
  $("#CRIME_CASE").val("");
  $("#CRIME_NUMBER").val("");
  $("#CRIME_YEAR").val("");
  $("#FIRST_NAME").val("");
  $("#ID_NO").val("");
  $("#NATION").val("");
  $("#LAST_NAME").val("");

  $("#type_search").val("");
  result = "";
}

// function printPdf()
// {
//   console.log(result);
//   var pram = "?result="+ JSON.stringify(result);
//   var url = 'print.php'+ pram;
//   postURL_blank(url);
// }



function printPdf()
{
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }
}
