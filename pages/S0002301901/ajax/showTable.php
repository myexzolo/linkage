<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">ลำดับ</th>
      <th>เลขประจำตัวประชาชน บุตรบุญธรรม</th>
      <th>ชื่อ บุตรบุญธรรม</th>
      <th>วันเดือนปี เกิด บุตรบุญธรรม</th>
      <th>เพศ บุตรบุญธรรม</th>
      <th>สัญชาติ บุตรบุญธรรม</th>
      <th>ชื่อ ผู้รับบุตรบุญธรรม</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //echo $_POST['result'];
    if($_POST['result'] != "")
    {
      $result = json_decode($_POST['result'], true);
      $data   = $result['data'];
      for($i = 0; $i < count($data); $i++)
      {
          $allName = $data[$i]['allAdopt'];

          // allAdopt.adoptDate
          // allAdopt.adoptID
          // allAdopt.adoptPlace
          // allAdopt.adoptPlaceDesc
          // allAdopt.adoptPlaceProvince
          // allAdopt.adoptSequance
          // allAdopt.childAge
          // allAdopt.childDateOfBirth
          // allAdopt.childFirstName
          // allAdopt.childFullnameAndRank
          // allAdopt.childGenderCode
          // allAdopt.childGenderDesc
          // allAdopt.childLastName
          // allAdopt.childMiddleName
          // allAdopt.childNationalityCode
          // allAdopt.childNationalityDesc
          // allAdopt.childPID
          // allAdopt.childPassport
          // allAdopt.childTitleCode
          // allAdopt.childTitleDesc
          // allAdopt.parentAge
          // allAdopt.parentDateOfBirth
          // allAdopt.parentFirstName
          // allAdopt.parentFullnameAndRank
          // allAdopt.parentGenderCode
          // allAdopt.parentGenderDesc
          // allAdopt.parentLastName
          // allAdopt.parentMiddleName
          // allAdopt.parentNationalityCode
          // allAdopt.parentNationalityDesc
          // allAdopt.parentPID
          // allAdopt.parentPassport
          // allAdopt.parentTitleCode
          // allAdopt.parentTitleDesc
          // total

      ?>
        <tr class="text-center">
          <td><?=$i+1;?></td>
          <td><?= $allName['childPID'];?></td>
          <td style="text-align:left;"><?= $allName['childFullnameAndRank'] ?></td>
          <td style="text-align:left;"><?= dateThLinkage2($allName['childDateOfBirth']) ?></td>
          <td ><?= $allName['childGenderDesc'];?></td>
          <td ><?= $allName['childNationalityDesc'];?></td>
          <td ><?= $allName['parentFullnameAndRank'];?></td>
        </tr>
      <?php
        }
      }
    ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
