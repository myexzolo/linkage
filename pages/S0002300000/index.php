<!DOCTYPE html>
<?php
$officeCode   = @$_POST['officeCode'];
$serviceCode  = @$_POST['serviceCode'];
$versionCode  = @$_POST['versionCode'];
$pageDetail   = @$_POST['pageDetail'];
$pageId       = @$_POST['pageId'];

$code = $officeCode.$serviceCode.$versionCode;
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบฐานข้อมูลประชาชนและการบริการภาครัฐ(Linkage Center) - <?= $pageDetail ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/REP01.css">
      <style>
      .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #fff;
            opacity: 1;
            border-top: 0px;
            border-right: 0px;
            border-left: 0px;
        }
      </style>
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageDetail ?>
              <small><?= $code ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?= $pageDetail ?></li>
            </ol>
          </section>
          <!-- Main content -->
          <section class="content">
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
                  <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-success btn-flat" onclick="readSmartCard()" style="width:160px;font-size:20px;height:40px;">อ่านบัตรสมาร์ทการ์ด</button>
                      <input value="<?=$officeCode ?>" id="officeCode" type="hidden">
                      <input value="<?=$serviceCode ?>" id="serviceCode" type="hidden">
                      <input value="<?=$versionCode ?>" id="versionCode" type="hidden">
                      <input value="<?=$pageId ?>" id="pageId" type="hidden">
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-9">
                        <div class="row">
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>เลขที่ประจำตัวประชาชน</label>
                              <input value="" id="personalId" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>ชื่อ - สกุล (ภาษาไทย)</label>
                              <input value="" id="fullname" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>ชื่อ - สกุล (ภาษาอังกฤษ)</label>
                              <input value="" id="fullname_en" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>ที่อยู่</label>
                              <input value="" id="address" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>เพศ</label>
                              <input value="" id="gender" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>วันเดือนปี เกิด</label>
                              <input value="" id="dateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label>วันเดือนปี ที่ออกบัตร</label>
                              <input value="" id="dateIssue" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
                            </div>
                          </div>
                          <div class="col-md-8">
                            <div class="form-group">
                              <label>สถานที่/หน่วยงานที่ออกบัตร</label>
                              <input value="" id="placeIssue" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
                            </div>
                          </div>

                          <div class="col-md-4">
                            <div class="form-group">
                              <label>วันเดือนปี บัตรหมดอายุ</label>
                              <input value="" id="dateExpiry" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
                            </div>
                          </div>

                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group" align="center">
                              <label>รูปภาพ</label>
                              <p><img src="../../image/user1.jpg" id="image" style="height:150px;"></p>
                              <div id="numberImage" style="font-size:18px;"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer">
                      <div class="col-md-12" style="text-align:center">
                        <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
                        <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
                        <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/REP01.js"></script>
    </body>
  </html>
