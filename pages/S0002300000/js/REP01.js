
var result = "";
function readSmartCard(){
  var url = "http://localhost:20001/Readcard/?type=readSmartCard";
  $.get(url)
  .done(function( data ) {
    // console.log(data);
    if(data.Status == "Y")
    {
      result = data.data;
      var middleName = data.data.middleName;
      if(middleName != ""){
         middleName = " " + middleName + " ";
      }else{
        middleName = " ";
      }
      var middleName_en = data.data.middleName_en;
      if(middleName_en != ""){
         middleName_en = " " + middleName_en + " ";
      }else{
        middleName_en = " ";
      }
      var gender = data.data.genderCode;
      if(gender == 1){
        gender = "ชาย";
      }else
      {
        gender = "หญิง";
      }
      var road = data.data.road;
      if(road != ""){
        road = " " + road + " ";
      }else{
        road = "";
      }
      var villageNo = data.data.villageNo;
      if(villageNo != ""){
        villageNo = " " + villageNo + " ";
      }else{
        villageNo = "";
      }
      $("#personalId").val(data.data.personalId);
      $("#fullname").val(data.data.titleName +" "+ data.data.firstName + middleName + data.data.lastName);
      $("#fullname_en").val(data.data.titleName_en +" "+ data.data.firstName_en + middleName_en + data.data.lastName_en);

      $("#dateOfBirth").val(dateThLinkage(data.data.dateOfBirth));
      $("#dateIssue").val(dateThLinkage(data.data.dateIssue));
      $("#dateExpiry").val(dateThLinkage(data.data.dateExpiry));
      $("#placeIssue").val(data.data.placeIssue);
      $("#gender").val(gender);
      $("#numberImage").html(data.data.numberImage);

      $("#address").val(data.data.houseNo + " " + road + villageNo + " "+ data.data.subdistrict + " " + data.data.district + " " + data.data.province);
      $("#image").attr("src","data:image/png;base64, "+ data.data.image);

      var page_id      = $("#pageId").val();
      var type_search  = "2"; //สิทธิประชาชน
      var pid          = $("#personalId").val();
      $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});

    }else{
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
    }

  }).fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  });
}

function reset()
{
  $("#personalId").val("");
  $("#fullname").val("");
  $("#fullname_en").val("");
  $("#dateOfBirth").val("");
  $("#dateIssue").val("");
  $("#dateExpiry").val("");
  $("#placeIssue").val("");
  $("#gender").val("");
  $("#numberImage").html("");
  $("#address").val("");
  $("#image").attr("src","../../image/user1.jpg");
}

function printPdf()
{
  var personalId  = $("#personalId").val();
  var fullname    = $("#fullname").val();

  // if(fullname != "" && personalId != "")
  // {
    var fullname_en = $("#fullname_en").val();
    var dateOfBirth = $("#dateOfBirth").val();
    var dateIssue   = $("#dateIssue").val();
    var dateExpiry  = $("#dateExpiry").val();
    var placeIssue  = $("#placeIssue").val();
    var gender      = $("#gender").val();
    var numberImage = $("#numberImage").html();
    var address     = $("#address").val();
    var image       = $("#image").attr("src");

    var page_id      = $("#pageId").val();
    var type_search  = "2"; //สิทธิประชาชน

    // console.log(image);

    image = encodeURIComponent(image);


    var pram = "?personalId="+personalId+"&fullname="+ fullname +"&fullname_en="+ fullname_en
                 + "&dateOfBirth=" + dateOfBirth +"&dateIssue="+ dateIssue + "&dateExpiry="+ dateExpiry+ "&gender="+ gender
                 + "&placeIssue="+ placeIssue + "&numberImage="+ numberImage + "&address="+ address + "&page_id="+ page_id + "&type_search="+ type_search
                 + "&image="+ image;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  // }else{
  //   $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  // }
}
