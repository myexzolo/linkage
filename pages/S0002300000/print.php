  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $personalId  = $_POST['personalId'];
  $fullname    = $_POST['fullname'];
  $fullname_en = $_POST['fullname_en'];
  $dateOfBirth = $_POST['dateOfBirth'];
  $dateIssue   = $_POST['dateIssue'];
  $dateExpiry  = $_POST['dateExpiry'];
  $placeIssue  = $_POST['placeIssue'];
  $gender      = $_POST['gender'];
  $numberImage = $_POST['numberImage'];
  $address     = $_POST['address'];
  $image       = $_POST['image'];
  $page_id     = $_POST['page_id'];
  $type_search = $_POST['type_search'];

  $nameUser   = $_SESSION['member'][0]['user_name'];
  $user_id    = $_SESSION['member'][0]['user_id'];

  $image =  rawurldecode($image);
  $imagePath = "";
  $pathImg     = "images/";

  if($image != "" && strpos($image,"user1.jpg") == 0)
  {
    $imagePath = Base64ToImage($image,$pathImg,$personalId);
    $imagePath = $pathImg.$imagePath;
  }else{
    $imagePath = $image;
  }

  if($personalId != "")
  {
        $sql = "INSERT INTO t_history_search
        (user_id,pid,page_id,type_search,type_report)
        VALUES
        ('$user_id','$personalId','$page_id','$type_search','P')";
        DbQuery($sql,null);
  }
  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  //print_r($_POST);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลบัตรประจำตัวประชาชน</div>
      <br>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td>เลขที่ประจำตัวประชาชน</td>
          <td style="width:3px"> : </td>
          <td style="width:55%"><?= $personalId ?></td>
          <td align="center" rowspan="9" style="vertical-align:top;">
            <label>รูปภาพ</label>
            <div style="height:10px">&nbsp;</div>
            <p><img src='<?= $imagePath ?>' style="height:150px;"></p>
            <div style="font-size:14pt;"><?= $numberImage ?></div>
          </td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล (ภาษาไทย)</td>
          <td> : </td>
          <td><?= $fullname ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล (ภาษาอังกฤษ)</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $fullname_en ?></td>
        </tr>
        <tr>
          <td>วันเดือนปี เกิด</td>
          <td> : </td>
          <td><?= $dateOfBirth ?></td>
        </tr>
        <tr>
          <td>เพศ</td>
          <td> : </td>
          <td><?= $gender ?></td>
        </tr>
        <tr>
          <td style="vertical-align:top;">ที่อยู่</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $address ?></td>
        </tr>
        <tr>
          <td>วันเดือนปี ที่ออกบัตร</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $dateIssue ?></td>
        </tr>
        <tr>
          <td>สถานที่/หน่วยงานที่ออกบัตร</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $placeIssue ?></td>
        </tr>
        <tr>
          <td>วันเดือนปี บัตรหมดอายุ</td>
          <td> : </td>
          <td><?= $dateExpiry ?></td>
        </tr>
      </table>
      <div style="height:100px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $fullname ?> )</td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลบัตรประจำตัวประชาชน');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
