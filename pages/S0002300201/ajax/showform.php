<div class="box-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขที่ประจำตัวประชาชน</label>
        <input value="" id="personalId" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ - สกุล</label>
        <input value="" id="fullname" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วันเดือนปี เกิด</label>
        <input value="" id="dateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เพศ</label>
        <input value="" id="gender" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สัญชาติ</label>
        <input value="" id="nationality" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>ที่อยู่ (ที่เพิ่มชื่อเข้า)</label>
        <input value="" id="houseRegistration" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อนายทะเบียนผู้รับแจ้งการเกิด</label>
        <input value="" id="officerName" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>รหัสสำนักทะเบียนที่แจ้งเกิด</label>
        <input value="" id="authorityIssuing" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>วันที่แจ้งเกิด</label>
        <input value="" id="dateOfNotifying" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>

    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อโรงพยาบาล</label>
        <input value="" id="hospitalName" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานที่เกิด</label>
        <input value="" id="placeOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <label>ที่อยูู่ที่เกิด</label>
        <input value="" id="placeOfBirthAddress" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขประจำตัวประชาชนผู้แจ้งการเกิด</label>
        <input value="" id="personInformPersonalID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อผู้แจ้งการเกิด</label>
        <input value="" id="personInformName" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>อายุผู้แจ้งการเกิด</label>
        <input value="" id="personInformAge" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ความสัมพันธ์ผู้แจ้งการเกิด</label>
        <input value="" id="personInformRelation" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขประจำตัวประชาชน บิดา</label>
        <input value="" id="fatherPersonalID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อบิดา - สกุล</label>
        <input value="" id="fatherName" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สัญชาติ บิดา</label>
        <input value="" id="fatherNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขประจำตัวประชาชน มารดา</label>
        <input value="" id="motherPersonalID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อมารดา - สกุล[ก่อนสมรส]</label>
        <input value="" id="motherName" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สัญชาติ มารดา</label>
        <input value="" id="motherNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
  </div>
</div>
<div class="box-footer">
  <div class="col-md-12" style="text-align:center">
    <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
    <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
    <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
  </div>
</div>
