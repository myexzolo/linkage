var result = "";


$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });
});

showForm();
function showForm(){
  $.get("ajax/showform.php")
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function checkdataAddress(data){
  var val = "";
  if(data != ""){
     val = " " + data;
  }
  return val;
}

function subName(name){
  var val = "";
  if(name != "")
  {
      var res = name.split("#");
      var middleName = res[2];
      if(middleName != ""){
         middleName = " " + middleName + " ";
      }else{
        middleName = " ";
      }
      val = res[0] + res[1] + middleName + res[3];
  }
  return val;
}

function readSmartCard(type){
  var pid = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  if(type == "R"){
    $("#pid").val("");
    $("#type_search").val("2");
    pid = "";
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
  }
  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  $.get(url)
  .done(function( data ) {
    console.log(data);
    $.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
    if(data.Status == "Y")
    {

      if(data.data != "" && data.data.info == null)
      {
        result = data;
        var middleName = data.data.middleName;
        if(middleName != ""){
           middleName = " " + middleName + " ";
        }else{
          middleName = " ";
        }

        var hNo               = checkdataAddress(data.data.houseRegistrationNo); //บ้านเลขที่
        var hVillageNo        = checkdataAddress(data.data.houseRegistrationVillageNo); //หมู่
        var hAlleyWayDesc     = checkdataAddress(data.data.houseRegistrationAlleyWayDesc);//ตรอก
        var hAlleyDesc        = checkdataAddress(data.data.houseRegistrationAlleyDesc);//ซอย
        var hRoadDesc         = checkdataAddress(data.data.houseRegistrationRoadDesc);//ถนน
        var hSubdistrictDesc  = checkdataAddress(data.data.houseRegistrationSubdistrictDesc);//ตำบล
        var hDistrictDesc     = checkdataAddress(data.data.houseRegistrationDistrictDesc);//อำเภอ
        var hProvinceDesc     = checkdataAddress(data.data.houseRegistrationProvinceDesc);//จังหวัด

        var houseRegistration =  hNo + hVillageNo + hAlleyWayDesc + hAlleyDesc + hRoadDesc + hSubdistrictDesc + hDistrictDesc + hProvinceDesc;

        result.data.houseRegistration  = houseRegistration;

        var pNo               = checkdataAddress(data.data.placeOfBirthNo); //บ้านเลขที่
        var pVillageNo        = checkdataAddress(data.data.placeOfBirthVillageNo); //หมู่
        var pAlleyWayDesc     = checkdataAddress(data.data.placeOfBirthAlleyWayDesc);//ตรอก
        var pAlleyDesc        = checkdataAddress(data.data.placeOfBirthAlleyDesc);//ซอย
        var pRoadDesc         = checkdataAddress(data.data.placeOfBirthRoadDesc);//ถนน
        var pSubdistrictDesc  = checkdataAddress(data.data.placeOfBirthSubdistrictDesc);//ตำบล
        var pDistrictDesc     = checkdataAddress(data.data.placeOfBirthDistrictDesc);//อำเภอ
        var pProvinceDesc     = checkdataAddress(data.data.placeOfBirthProvinceDesc);//จังหวัด


        var placeOfBirthAddress = pNo + pVillageNo + pAlleyWayDesc + pAlleyDesc + pRoadDesc + pSubdistrictDesc + pDistrictDesc + pProvinceDesc;

        result.data.placeOfBirthAddress  = placeOfBirthAddress;

        var fullname = data.data.titleDesc +" "+ data.data.firstName + middleName + data.data.lastName;
        result.data.fullname  = fullname;

        var dateOfBirth = dateThLinkage(data.data.dateOfBirth);
        result.data.dateOfBirth = dateOfBirth;

        var dateOfNotifying = dateThLinkage(data.data.dateOfNotifying);
        result.data.dateOfNotifying = dateOfNotifying;

        var fatherName = subName(data.data.fatherName);
        result.data.fatherName = fatherName;

        var motherName = subName(data.data.motherName);
        result.data.motherName = motherName;

        $("#personalId").val(data.data.personalID);
        $("#fullname").val(fullname);

        $("#dateOfBirth").val(dateOfBirth);
        $("#gender").val(data.data.gender);
        $("#nationality").val(data.data.nationality);
        $("#houseRegistration").val(houseRegistration);
        $("#officerName").val(data.data.officerName);
        $("#authorityIssuing").val(data.data.authorityIssuing);
        $("#dateOfNotifying").val(dateOfNotifying);
        $("#hospitalName").val(data.data.hospitalName);
        $("#placeOfBirth").val(data.data.placeOfBirth);
        $("#placeOfBirthAddress").val(placeOfBirthAddress);
        $("#personInformPersonalID").val(data.data.personInformPersonalID);
        $("#personInformName").val(data.data.personInformName);
        $("#personInformAge").val(data.data.personInformAge);
        $("#personInformRelation").val(data.data.personInformRelation);
        $("#fatherPersonalID").val(data.data.fatherPersonalID);
        $("#fatherName").val(fatherName);
        $("#fatherNationalityDesc").val(data.data.fatherNationalityDesc);
        $("#motherPersonalID").val(data.data.motherPersonalID);
        $("#motherName").val(motherName);
        $("#motherNationalityDesc").val(data.data.motherNationalityDesc);

        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();
        var pid          = $("#personalId").val();
        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});
      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      if(data.Message == ""){
        data.Message = "ไม่พบข้อมูล";
      }
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
      reset();
    }

  })
  .fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  });
}

function reset()
{
  $("#pid").val("");
  $("#personalId").val("");
  $("#fullname").val("");

  $("#dateOfBirth").val("");
  $("#gender").val("");
  $("#nationality").val("");
  $("#houseRegistration").val("");
  $("#officerName").val("");
  $("#authorityIssuing").val("");
  $("#dateOfNotifying").val("");
  $("#hospitalName").val("");
  $("#placeOfBirth").val("");
  $("#placeOfBirthAddress").val("");
  $("#personInformPersonalID").val("");
  $("#personInformName").val("");
  $("#personInformAge").val("");
  $("#personInformRelation").val("");
  $("#fatherPersonalID").val("");
  $("#fatherName").val("");
  $("#fatherNationalityDesc").val("");
  $("#motherPersonalID").val("");
  $("#motherName").val("");
  $("#motherNationalityDesc").val("");

  $("#type_search").val("");

  result = "";
}

function printPdf()
{
  //console.log(result);
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}
