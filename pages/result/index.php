<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  include("../../THSplitLib/segment.php");

  $projectCode    = $_POST['project_code'];
  $person_number  = $_POST['person_number'];



  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      ['format' => 'A4'
      ],
      'default_font' => 'chatthai'
  ]);

  ob_start();

  //echo $projectCode.",".$person_number;

  $sql = "SELECT TO_CHAR(r.date_create,'YYYY-MM-DD HH24:MI:SS') as date_create ,p.person_number,p.person_name,p.person_lname,p.person_gender,".getDateOracle('date_of_birth')."
          FROM pfit_t_person p
          LEFT JOIN pfit_t_result r ON  r.person_number = p.person_number and r.project_code = '$projectCode'
          WHERE p.project_code = '$projectCode' and p.person_number = '$person_number'
          group by r.date_create, p.person_number,p.person_name,p.person_lname,p.person_gender,p.date_of_birth";

  //echo $sql;

  $query_date   = DbQuery($sql,null);
  $json         = json_decode($query_date, true);
  $row          = $json['data'];
  $num          = $json['dataCount'];


  $name = "";
  $lname = "";
  $gender = "";
  $person_gender = "";
  $dob = "";
  $age = "";
  $dateCreate = '...../...../..........';

  if($num > 0){
    if(isset($row[0]['date_create']) && !empty($row[0]['date_create'])){
      $dateCreate = DateTimeThai($row[0]['date_create']);
    }
    $name     = $row[0]['person_name'];
    $lname    = $row[0]['person_lname'];
    $gender   = $row[0]['person_gender'];
    $dob      = $row[0]['date_of_birth'];


    if(!empty($dob) && isset($dob)){
      $age      = yearBirth($dob);
    }

    $person_gender  = $gender=='M'?"ชาย":"หญิง";
  }

  $sql = "SELECT r.result, r.result_cal, r.test_criteria_code,cd.category_criteria_detail_name
          FROM pfit_t_result r
          LEFT JOIN pfit_t_test_criteria tc ON r.test_criteria_code = tc.test_criteria_code
          LEFT JOIN pfit_t_cat_criteria_detail cd ON tc.category_criteria_detail_code = cd.category_criteria_detail_code
          WHERE r.person_number = '$person_number' and r.project_code = '$projectCode'
          and r.test_code = 'BMI'";
  //echo $sql;
  $query   = DbQuery($sql,null);
  $json    = json_decode($query, true);
  $row     = $json['data'];
  $num     = $json['dataCount'];

  $wiegth         = "";
  $height         = "";
  $BMI            = array('','');
  $result         = isset($row[0]['result'])?$row[0]['result']:"";

  if($num > 0 && $result != "-" && $result != "")
  {
    $value  = explode("|",$result);
    $wiegth = $value[0];
    $height = $value[1];

    $BMI[0] = isset($row[0]['result_cal']) && !empty($row[0]['result_cal'])?$row[0]['result_cal']:"-";
    $BMI[1] = isset($row[0]['category_criteria_detail_name']) && !empty($row[0]['category_criteria_detail_name'])?$row[0]['category_criteria_detail_name']:"-";
  }


  ?>
  <!DOCTYPE html>
  <html lang="en" dir="ltr">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link rel="shortcut icon" type="image/png" href="../../image/favicon.png"/>
    </head>
    <body>
  <table style="width: 100%;" border="0" >
      <tr>
        <td align="left" style="width:180px"><img src="../../image/dpe_logo.png" style="height:100px;width:100px;"></td>
        <td colspan="3" style="font-size:18pt;font-weight:500;padding-top:20px;" align="center"><b>ผลการทดสอบสมรรถภาพทางกาย</b></td>
        <td align="right" style="width:180px;font-size:14pt;vertical-align: text-top;";>วันที่ทดสอบ : <?= $dateCreate; ?></td>
      </tr>
      <tr style="padding-top:20px;">
        <td colspan="5" style="padding-top:20px;font-size:14pt;" class="info">
          <b>ชื่อ :</b> <?=$name,' ',$lname; ?> &nbsp;&nbsp;
          <b>เพศ :</b> <?= $person_gender; ?> &nbsp;&nbsp;
          <b>อายุ :</b> <?= $age; ?> ปี
        </td>
      </tr>
      <tr>
        <td colspan="5" style="width:250px;font-size:14pt;"class="info">
          <b>น้ำหนัก :</b> <?= $wiegth; ?> กิโลกรัม &nbsp;
          <b>ส่วนสูง :</b> <?= $height; ?> เซนติเมตร &nbsp;
          <b>ดัชนีมวลกาย :</b> <?=  $BMI[0] ?> &nbsp;
          <b>อยู่ในเกณฑ์ :</b> <?= $BMI[1]; ?>
        </td>
      </tr>
    </table>
    <br>
    <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
      <thead>
        <tr>
    			<th class="thStyle" style="width: 190px">รายการทดสอบ</th>
    			<th colspan="2" class="thStyle"> ผลการทดสอบ</th>
          <th class="thStyle" style="width:80px">เกณฑ์<br>มาตรฐาน</th>
    			<th class="thStyle" style="width:95px">ผลการประเมิน</th>
    			<th class="thStyle" >คำแนะนำ</th>
          <th class="thStyle" style="width:95px">การเสริมสร้าง<br>สรรถภาพ</th>
    		</tr>
      </thead>
      <tbody>
  <?php
      $sql = "SELECT r.result, r.result_cal,r.test_code, r.test_criteria_code,cd.category_criteria_detail_name,t.test_name,t.test_unit,tc.test_suggest,t.link_youtube2
              FROM pfit_t_result r
              LEFT JOIN pfit_t_test_criteria tc ON r.test_criteria_code = tc.test_criteria_code
              LEFT JOIN pfit_t_cat_criteria_detail cd ON tc.category_criteria_detail_code = cd.category_criteria_detail_code
              LEFT JOIN pfit_t_test t ON r.test_code = t.test_code
              WHERE r.person_number = '$person_number' and r.project_code = '$projectCode'
              and r.test_code <> 'BMI' and $age >= t.test_age_min and $age <= t.test_age_max
              group by r.result, r.result_cal,r.test_code, r.test_criteria_code,cd.category_criteria_detail_name,t.test_name,t.test_unit,tc.test_suggest,t.link_youtube2";

        // $sql = "SELECT rh.result, rh.resulthis_cal,rh.test_code, rh.test_criteria_code,cd.category_criteria_detail_name,t.test_name,t.test_unit,tc.test_suggest
        //         FROM pfit_t_result_history rh
        //         LEFT JOIN pfit_t_test_criteria tc ON rh.test_criteria_code = tc.test_criteria_code
        //         LEFT JOIN pfit_t_cat_criteria_detail cd ON tc.category_criteria_detail_code = cd.category_criteria_detail_code
        //         LEFT JOIN pfit_t_test t ON rh.test_code = t.test_code
        //         WHERE rh.person_number = '$person_number' and rh.project_code = '$projectCode'
        //         and rh.time = '$time' and rh.test_code <> 'BMI'";
        //echo $sql;
        $query   = DbQuery($sql,null);
        $json    = json_decode($query, true);
        $row     = $json['data'];
        $num     = $json['dataCount'];

        for($i=0;$i<$num ;$i++)
        {
          $test_name  = $row[$i]['test_name'];
          $test_code  = $row[$i]['test_code'];
          $test_unit  = $row[$i]['test_unit'];

          $result_cal = isset($row[$i]['result_cal']) && ($row[$i]['result_cal'] != null)?$row[$i]['result_cal']:"-";
          $link_youtube2 = isset($row[$i]['link_youtube2']) && !empty($row[$i]['link_youtube2'])?$row[$i]['link_youtube2']:"";

          $category_criteria_detail_name = isset($row[$i]['category_criteria_detail_name']) && !empty($row[$i]['category_criteria_detail_name'])?$row[$i]['category_criteria_detail_name']:"-";
          $test_suggest = isset($row[$i]['test_suggest']) && !empty($row[$i]['test_suggest'])?strip_tags($row[$i]['test_suggest']):"";
          //$test_suggest = mb_substr($test_suggest,0,-1,'UTF-8');

          //iconv_substr($massage, 0,25, "UTF-8");
          // if($test_suggest != ""){
          //   $segment = new Segment();
          //   $arrTestSuggest = $segment->get_segment_array($test_suggest);
          //   $test_suggest = "";
          //   $x = 0;
          //   for($i=0; $i < count($arrTestSuggest); $i++)
          //   {
          //        $test_suggest .= $arrTestSuggest[$i];
          //        $x++;
          //        if($x == 10)
          //        {
          //          $x = 0;
          //          $test_suggest .= "<br>";
          //        }
          //   }
          // }
          $criteriaS = "";

          if($test_code == "ABP"){
            $criteriaS = "120/80";
            if($resultTest != ""){
              $result_cal = str_replace("|", "/", $resultTest);
            }
          }else{
            $sql_m = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$test_code' AND age = '$age' AND gender = '$gender' order by category_criteria_detail_code";
            $query_m = DbQuery($sql_m,null);
            $row_m  = json_decode($query_m, true);
            $num_m = $row_m['dataCount'];
            $postion =  ceil($num_m/2)-1;

            $min = "";
            $max = "";
            if(isset($row_m['data'][$postion]['min'])){
              $min = $row_m['data'][$postion]['min']!=''?$row_m['data'][$postion]['min']:"";
            }
            if(isset($row_m['data'][$postion]['max'])){
              $max = $row_m['data'][$postion]['max']!=''?$row_m['data'][$postion]['max']:"";
            }

            if(substr($min,0,1) == "."){
              $min = "0".$min;
            }

            if(substr($max,0,1) == "."){
              $max = "0".$max;
            }
            $criteriaS = $min.' - '.$max;
          }

  ?>
        <tr>
    			<td class="content"><?=$test_name ?></td>
          <td class="content" align="right" style="border-right:0px;width:35px"><?= $result_cal; ?>&nbsp;</td>
          <td class="content" align="left" style="border-left:0px;width:60px"><?= $test_unit ?></td>
    			<td class="content" align="center"><?= $criteriaS; ?></td>
    			<td class="content" align="center"><?= $category_criteria_detail_name ?></td>
    			<td class="content"><p><?= $test_suggest; ?></p></td>
          <td class="content">
            <?php
                if($link_youtube2 != ""){
                  $qr = "https://youtu.be/".$link_youtube2;
            ?>
            <div class="barcode" align="center">
              <barcode code="<?=$qr?>" type="QR" size="0.9" error="M" disableborder = "1"/>
              <!-- <barcode code="testtttt" size="0.8" height="1.8" type="C128A"/> -->
            </div>
            <?php } ?>
          </td>
    		</tr>
  <?php
        }
        if($num == 0){
  ?>
  <tr>
    <td class="content">&nbsp;</td>
    <td class="content" colspan="2"></td>
    <td class="content"></td>
    <td class="content"></td>
    <td class="content"></td>
    <td class="content"></td>
  </tr>
  <?php
        }
  ?>
      </tbody>
    </table>
    <div align="center" style="font-size:12pt;padding-top:10px;">
      * หมายเหตุ ค่าเกณฑ์สมรรถภาพทางกายสำหรับแต่ละช่วงวัยดูได้ที่ http://sportscience.dpe.go.th/
    </div>
    <div align="center" style="font-size:12pt;font-weight: bold;">
      กรมพลศึกษา กระทรวงการท่องเที่ยวและกีฬา โทร. 0 2214 2577
    </div>
    <div class="break"></div>
  <?php
    // $url = "../result/".$person_number."_".$projectCode.".pdf";

    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ผลการทดสอบสมรรถภาพทางกาย');
    $mpdf->AddPage('P','','','','',5,5,10,10,10,10);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
