var result = "";

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });
})

function readSmartCard(type){
  var pid = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  if(type == "R"){
    $("#pid").val("");
    pid = "";
    $("#type_search").val("2");
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
  }
  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  //console.log(url);
  $.get(url)
  .done(function( data ) {
    //console.log(data);
    if(data.Status == "Y")
    {
      if(data.data != "" && data.data.info == null)
      {
        result = data;

        var titleProvince = "";
        var titleDistrict = "";
        var titleSubdistrict = "";

        var province = data.data.provinceDesc;

        if(province.indexOf("กรุงเทพ") >= 0){
          titleProvince = "";
          titleDistrict = "";
          titleSubdistrict = "แขวง";
        }else{
          titleProvince = "จังหวัด";
          titleDistrict = "อำเภอ";
          titleSubdistrict = "ตำบล";
        }

        var alleyDesc        = checkdataAddress(data.data.alleyDesc,"ซอย");//ชื่อซอย
        var alleyWayDesc     = checkdataAddress(data.data.alleyWayDesc,"ตรอก");//ชื่อตรอก
        var districtDesc     = checkdataAddress(data.data.districtDesc,titleDistrict);//ชื่ออำเภอ
        var houseNo          = checkdataAddress(data.data.houseNo,"เลขที่ ");//บ้านเลขที่
        var provinceDesc     = checkdataAddress(data.data.provinceDesc,titleProvince);//ชื่อจังหวัด
        var roadDesc         = checkdataAddress(data.data.roadDesc,"ถนน");//ชื่อถนน
        var subdistrictDesc  = checkdataAddress(data.data.subdistrictDesc,titleSubdistrict);//ชื่อตำบล
        var villageNo        = checkdataAddress(data.data.villageNo,"หมู่ ");//หมู่ที่

        var address =  houseNo + villageNo + alleyWayDesc + alleyDesc + roadDesc + subdistrictDesc + districtDesc + provinceDesc;
        result.data.address  = address;

        $("#houseID").val(data.data.houseID);
        $("#rcodeDesc").val(data.data.rcodeDesc);
        $("#houseTypeDesc").val(data.data.houseTypeDesc);
        $("#address").val(address);

        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();
        var pid          = result.PersonalID;
        //console.log(pid);
        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});

      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
    }

  });
}


function checkdataAddress(data, name){
  var val = "";
  if(data != null && data != ""){
     val = " " + name + data;
  }
  return val;
}

function reset()
{
  $("#personalId").val("");
  $("#houseID").val("");
  $("#rcodeDesc").val("");
  $("#houseTypeDesc").val("");
  $("#address").val("");

  $("#type_search").val("");

  result = "";
}


function printPdf()
{
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }
}
