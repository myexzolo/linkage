var result = "";

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });
})

showForm();
function showForm(){
  $.get("ajax/showform.php")
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function readSmartCard(type){
  var pid = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  if(type == "R"){
    $("#pid").val("");
    $("#type_search").val("2");
    pid = "";
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
  }
  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  $.get(url)
  .done(function( data ) {
    console.log(data);
    //$.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
    if(data.Status == "Y")
    {

      if(data.data != "" && data.data.info == null)
      {
        result = data;

        var res = data.data;


        $("#personalId").val(data.PersonalID);
        if(res.result)
        {
          $("#brwFullName").val(res.result.brwFullName);
          $("#brwIdCard").val(res.result.brwIdCard);
          $("#contractCount").val(res.result.contractCount);
          $("#contractDate").val(dateThLinkage(res.result.contractDate));
          $("#contractStatus").val(res.result.contractStatus);
          $("#reqOcc").val(res.result.reqOcc);

        }else{
          $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
          reset();
        }


        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();
        var pid          = $("#personalId").val();
        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});
      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      if(data.Message == ""){
        data.Message = "ไม่พบข้อมูล";
      }
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
      reset();
    }

  })
  .fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  });
}

function reset()
{
  $("#personalId").val("");

  $("#brwFullName").val("");
  $("#brwIdCard").val("");
  $("#contractCount").val("");
  $("#contractDate").val("");
  $("#contractStatus").val("");
  $("#reqOcc").val("");

  $("#type_search").val("");

  result = "";
}

function printPdf()
{
  //console.log(result);
  //result = "xxx";
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}
