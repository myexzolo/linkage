<div class="box-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขที่ประจำตัวประชาชน</label>
        <input value="" id="personalId" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ - สกุล (ภาษาไทย)</label>
        <input value="" id="fullname" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ - สกุล (ภาษาอังกฤษ)</label>
        <input value="" id="fullname_en" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันเดือนปี เกิด</label>
        <input value="" id="dateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>อายุ</label>
        <input value="" id="age" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เพศ</label>
        <input value="" id="genderDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สัญชาติ</label>
        <input value="" id="nationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานภาพบุคคล</label>
        <input value="" id="statusOfPersonDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะภาพเจ้าบ้าน</label>
        <input value="" id="ownerStatusDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันเดือนปี ที่ย้ายเข้ามาในบ้าน</label>
        <input value="" id="dateOfMoveIn" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขประจำตัวประชาชน บิดา</label>
        <input value="" id="fatherPersonalID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อบิดา</label>
        <input value="" id="fatherName" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สัญชาติ บิดา</label>
        <input value="" id="fatherNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขประจำตัวประชาชน มารดา</label>
        <input value="" id="motherPersonalID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อมารดา</label>
        <input value="" id="motherName" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สัญชาติ มารดา</label>
        <input value="" id="motherNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
  </div>
</div>
  <div class="box-footer">
    <div class="col-md-12" style="text-align:center">
      <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
      <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
      <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
    </div>
  </div>
