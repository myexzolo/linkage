<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ระบบ Back Office - เมนูหลัก</title>
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>

  <?php
  include('../../inc/css-header.php');
  session_destroy();
  include('../../inc/function/mainFunc.php');
  $hostName = getUrlHost();
  ?>
  <link rel="stylesheet" href="css/index.css">

</head>
<body class="hold-transition login-page" onload="showProcessbar();">
<div class="login-box">
  <div class="login-logo">
    <img src="../../image/dpe_logo.png" style="height:27vh;">
    <h2 style="color:#fff">ระบบจัดเก็บและรายงานผลการทดสอบสมรรถภาพทางกาย</h2>
  </div>
</div>
<div class="index-box">
<div align="center">
  <div class="row">
    <div class="col-md-4 col-sm-6">
      <a class="btn btn-app btn-shortcut" style="font-size:24px;" href="../login">
        <i class="fa fa-key" style="font-size:35px;"></i>
        <div style="margin:10px;">เข้าสู่ระบบจัดเก็บและรายงานผล<br>สมรรถภาพทางกาย</div>
      </a>
    </div>
    <div class="col-md-4 col-sm-6">
      <a class="btn btn-app btn-shortcut" style="font-size:24px;" onclick="showFormReg()">
      <i class="fa fa-clipboard" style="font-size:35px;"></i>
      <div style="margin:10px;">ลงทะเบียนใช้งานระบบจัดเก็บ<br>และรายงานผลสมรรถภาพทางกาย</div>
      </a>
    </div>
    <div class="col-md-4 col-sm-6">
      <a class="btn btn-app btn-shortcut" style="font-size:24px;" href="<?=$hostName."upload/doc/userManual.pdf"?>" target="_blank">
        <i class="fa fa-book" style="font-size:35px;"></i>
        <div style="margin:10px;">คู่มือการใช้งานระบบจัดเก็บ<br>และรายงานผลสมรรถภาพทางกาย</div>
      </a>
    </div>
    <div class="col-md-4 col-sm-6">
    <a class="btn btn-app btn-shortcut" style="font-size:24px;" onclick="showFormMedia('E')">
      <i class="glyphicon glyphicon-book" style="font-size:35px;"></i>
      <div style="margin:10px;">หนังสือแบบทดสอบและ<br>เกณฑ์มาตรฐานสมรรถภาพทางกาย</div>
    </a>
    </div>
    <div class="col-md-4 col-sm-6">
      <a class="btn btn-app btn-shortcut" style="font-size:25px;" onclick="showFormMedia('V')">
        <i class="glyphicon glyphicon-facetime-video" style="font-size:35px;"></i>
        <div style="margin:10px;">วิดีโอการทดสอบ<br>สมรรถภาพทางกาย</div>
      </a>
    </div>
    <div class="col-md-4 col-sm-6">
      <a class="btn btn-app btn-shortcut" style="font-size:25px;" onclick="exportExcel()">
        <i class="fa fa-file-excel-o" style="font-size:35px;"></i>
        <div style="margin:10px;">Excel Template<br></div>
      </a>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModalReg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">ลงทะเบียนใช้งานระบบ</h4>
      </div>
      <form id="formReg" novalidate enctype="multipart/form-data">
      <!-- <form novalidate action="ajax/AED.php" method="post" enctype="multipart/form-data"> -->
        <div id="show-form-reg"></div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="myModalMedia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">สื่อการทดสอบ</h4>
      </div>
        <div id="show-form-media"></div>
    </div>
  </div>
</div>

<div class="modal fade bs-example-modal" id="myModalExcel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Excel template</h4>
      </div>
        <div class="modal-body">
          <table id="tableExcel" class="table table-bordered table-striped">
          <thead>
            <tr>
                <th data-align="center">ลำดับ</th>
                <th data-align="center">รายการ Excel template</th>
                <th data-align="center">DownLoad</th>
            </tr>
          </thead>
          <tbody>
            <tr>
                <td style="text-align: center;">1</td>
                <td>นักเรียน (7 - 18 ปี)</td>
                <td style="text-align: center;">
                  <a onclick="loadExcel('../../template/person_template.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                </td>
            </tr>
            <tr>
                <td align="center">2</td>
                <td>ประชาชนทั่วไป ช่วงอายุ (7 - 18 ปี)</td>
                <td align="center">
                    <a onclick="loadExcel('../../template/person_7_18.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                </td>
            </tr>
            <tr>
                <td align="center">3</td>
                <td>ประชาชนทั่วไป ช่วงอายุ (19 - 59 ปี)</td>
                <td align="center">
                    <a onclick="loadExcel('../../template/person_19_59.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                </td>
            </tr>
            <tr>
                <td align="center">4</td>
                <td>ประชาชนทั่วไป ช่วงอายุ (60 ปีขึ้นไป)</td>
                <td align="center">
                  <a onclick="loadExcel('../../template/person_60.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                </td>
            </tr>
          </tbody>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
        </div>
    </div>
  </div>
</div>

<?php include('../../inc/js-footer.php'); ?>
<script src="js/index.js"></script>
</body>
</html>
