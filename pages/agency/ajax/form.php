<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id = $_POST['id'];

if($action == 'EDIT'){
  $btn = 'Update changes';

  $sqls   = "SELECT * FROM t_agency WHERE agency_id = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];


  $agency_code = $rows[0]['agency_code'];
  $agency_name = $rows[0]['agency_name'];
  $agency_seq = $rows[0]['agency_seq'];
  $is_active = $rows[0]['is_active'];
}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="agency_id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>OfficeCode</label>
        <input value="<?=@$agency_code?>" name="agency_code" type="text" maxlength="6" class="form-control" placeholder="Code" required>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        <label>ชื่อหน่วยงาน</label>
        <input value="<?=@$agency_name?>" name="agency_name" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>ลำดับการแสดง</label>
        <input value="<?=@$agency_seq?>" name="agency_seq" type="number" maxlength="3" class="form-control" placeholder="Sequence" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat"><?=$btn?></button>
</div>
