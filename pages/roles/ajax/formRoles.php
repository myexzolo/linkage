<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['value'];
$id = $_POST['id'];
$page_icon    = 'fa fa-circle-o';
$is_insert  = '';
$is_update  = '';
$is_delete  = '';
$is_print     = "";
$is_import    = "";
$is_export    = "";
$is_approve   = "";
$is_cancel    = "";
$arr_page_list = array();
if($action == 'EDIT'){
  $btn = 'Update changes';

  $sql   = "SELECT * FROM t_role WHERE role_id = '$id' ORDER BY role_id DESC";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  $role_code    = $row[0]['role_code'];
  $role_name    = $row[0]['role_name'];
  $role_desc    = $row[0]['role_desc'];
  $page_list    = $row[0]['page_list'];
  $is_insert    = $row[0]['is_insert'];
  $is_update    = $row[0]['is_update'];
  $is_delete    = $row[0]['is_delete'];
  $is_print     = $row[0]['is_print'];
  $is_import    = $row[0]['is_import'];
  $is_export    = $row[0]['is_export'];
  $is_approve   = $row[0]['is_approve'];
  $is_cancel    = $row[0]['is_cancel'];
  $is_active    = $row[0]['is_active'];

  if(!empty($page_list)){
      $arr_page_list = explode(",",$page_list);
  }

}
if($action == 'ADD'){
 $btn = 'Save changes';
}
?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="role_id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>Roles Code</label>
        <input value="<?=@$role_code?>" name="role_code" type="text" maxlength="6" class="form-control text-uppercase" placeholder="Code" required>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>Roles Name</label>
        <input value="<?=@$role_name?>" name="role_name" type="text" class="form-control" placeholder="Name" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Status</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required>
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        <label>Roles DESC</label>
        <input value="<?=@$role_desc?>" name="role_desc" type="text" class="form-control" placeholder="" required>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-info">
        <div class="box-header with-border">
          <label>Role Access</label>
        </div>
        <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_insert" value="Y"  <?=$is_insert=='Y'?"checked":""?>> ADD
                  </label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_update" value="Y" <?=$is_update=='Y'?"checked":""?>> Edit
                  </label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_delete" value="Y" <?=$is_delete=='Y'?"checked":""?>> Delete
                  </label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_print" value="Y" <?=$is_print=='Y'?"checked":""?>> Print
                  </label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_import" value="Y" <?=$is_import=='Y'?"checked":""?>> Import
                  </label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_export" value="Y" <?=$is_export=='Y'?"checked":""?>> Export
                  </label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_approve" value="Y" <?=$is_approve=='Y'?"checked":""?>> Approve
                  </label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="is_cancel" value="Y" <?=$is_cancel=='Y'?"checked":""?>> Cancel
                  </label>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
    <br>
    <div class="col-md-12" >
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title"><label>Select Pages</label></h3>
        </div>
        <div class="box-body">
            <div class="row">
              <?php
                $sqlm   = "SELECT * FROM t_module where is_active='Y' or module_id = 2 ";

                $querym     = DbQuery($sqlm,null);
                $json       = json_decode($querym, true);
                $errorInfo  = $json['errorInfo'];
                $dataCount  = $json['dataCount'];
                $rowm       = $json['data'];

                foreach ($rowm as $valuem) {
                  $module_name = $valuem['module_name'];
                  $module_id   = $valuem['module_id'];
              ?>
              <div class="col-md-12" style="margin-bottom:5px;">
                <label><?=$module_name?></label>
                <div class="form-group">
                  <?php
                    $sqlp   = "SELECT * FROM t_page WHERE module_id = '$module_id' and is_active = 'Y'";

                    $queryp     = DbQuery($sqlp,null);
                    $json       = json_decode($queryp, true);
                    $errorInfo  = $json['errorInfo'];
                    $dataCount  = $json['dataCount'];
                    $rowp       = $json['data'];

                    if($dataCount > 0){
                    foreach ($rowp as $key => $value) {
                  ?>
                  <div class="col-md-4">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="pagelist[]" value="<?=$value['page_id']?>" <?php if (@in_array($value['page_id'], @$arr_page_list)){ echo 'checked'; } ?> required>
                        <?=$value['page_name']?>
                      </label>
                    </div>
                  </div>
                <?php } }?>
                </div>
              </div>
              <?php } ?>
            </div>
        </div>
      </div>
    </div>

  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>
