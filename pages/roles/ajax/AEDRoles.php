<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action       = isset($_POST['action'])?$_POST['action']:"ADD";
$role_id      = isset($_POST['role_id'])?$_POST['role_id']:"";
$role_code    = strtoupper(isset($_POST['role_code'])?$_POST['role_code']:"");
$role_name    = isset($_POST['role_name'])?$_POST['role_name']:"";
$role_desc    = isset($_POST['role_desc'])?$_POST['role_desc']:"";
$role_access  = isset($_POST['role_access'])?$_POST['role_access']:"";
$is_insert    = isset($_POST['is_insert'])?$_POST['is_insert']:null;
$is_update    = isset($_POST['is_update'])?$_POST['is_update']:null;
$is_delete    = isset($_POST['is_delete'])?$_POST['is_delete']:null;
$is_print     = isset($_POST['is_print'])?$_POST['is_print']:null;
$is_import    = isset($_POST['is_import'])?$_POST['is_import']:null;
$is_export    = isset($_POST['is_export'])?$_POST['is_export']:null;
$is_approve   = isset($_POST['is_approve'])?$_POST['is_approve']:null;
$is_cancel    = isset($_POST['is_cancel'])?$_POST['is_cancel']:null;
$is_active    = isset($_POST['is_active'])?$_POST['is_active']:"is_active";
$page_list    = isset($_POST['pagelist'])?$_POST['pagelist']:array();
if(count($page_list) > 0){
  sort($page_list);
}

$page_list   = @implode(",",$page_list);

$dateNow  =  date('Y/m/d H:i:s');

// --ADD EDIT DELETE Module-- //
if(empty($role_id) && $action == 'ADD'){
  $sql   = "INSERT INTO t_role
  (role_name,role_desc,role_code,is_active,page_list,update_date,user_id_update,
    is_insert,is_update,is_delete,is_print,is_import,is_export,is_approve,is_cancel)
  VALUES
  ('$role_name','$role_desc','$role_code','$is_active','$page_list',NOW(),'1',
   '$is_insert','$is_update','$is_delete','$is_print','$is_import','$is_export','$is_approve','$is_cancel')";
}else if($action == 'EDIT'){
  $sql = "UPDATE t_role SET
            role_name      = '$role_name',
            role_desc      = '$role_desc',
            role_code      = '$role_code',
            is_active      = '$is_active',
            page_list      = '$page_list',
            update_date    = '$dateNow',
            user_id_update = '1',
            is_insert      = '$is_insert',
            is_update      = '$is_update',
            is_delete      = '$is_delete',
            is_print       = '$is_print',
            is_import      = '$is_import',
            is_export      = '$is_export',
            is_approve      = '$is_approve',
            is_cancel      = '$is_cancel'
            WHERE role_id = '$role_id'";
}else if($action == 'DEL'){
  $sql   = "DELETE FROM t_role WHERE role_id = '$role_id'";
}

// --ADD EDIT Roles-- //
//echo $sql;
$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){
  //echo $sqlAcc;
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
