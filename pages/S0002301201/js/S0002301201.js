var result = "";

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });
})

showForm();
function showForm(){
  $.get("ajax/showform.php")
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function readSmartCard(type){
  var pid = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  if(type == "R"){
    $("#pid").val("");
    $("#type_search").val("2");
    pid = "";
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
  }
  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  $.get(url)
  .done(function( data ) {
    console.log(data);
    //$.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
    if(data.Status == "Y")
    {

      if(data.data != "" && data.data.info == null)
      {
        result = data;

        // address.houseAlley
        // address.houseAlleyWay
        // address.houseDistrict
        // address.houseNo
        // address.houseProvince
        // address.houseRoad
        // address.houseSubDistrict
        // address.houseVillageNo
        // cardExpireDate
        // cardIssueDate
        // dateOfBirth
        // firstName
        // genderText
        // lastName
        // nationalityCode
        // nationalityText
        // title

        // Message: "HTTP Status Code 404"
        // PersonalID: "1580400006441"
        // ReturnCode: "00404"
        // Status: "Y"
        // data: {info: "ไม่พบข้อมูลของเลขประจำตัวประชาชน 1580400006441"}


        var titleProvince = "";
        var titleDistrict = "";
        var titleSubdistrict = "";

        var province = data.data.address.houseProvince;

        if(province.indexOf("กรุงเทพ") >= 0){
          titleProvince = "";
          titleDistrict = "เขต";
          titleSubdistrict = "แขวง";
        }else{
          titleProvince = "จังหวัด";
          titleDistrict = "อำเภอ";
          titleSubdistrict = "ตำบล";
        }

        var alleyDesc        = checkdataAddress(data.data.address.houseAlley,"ซอย");//ชื่อซอย
        var alleyWayDesc     = checkdataAddress(data.data.address.houseAlleyWay,"ตรอก");//ชื่อตรอก
        var districtDesc     = checkdataAddress(data.data.address.houseDistrict,titleDistrict);//ชื่ออำเภอ
        var houseNo          = checkdataAddress(data.data.address.houseNo,"เลขที่ ");//บ้านเลขที่
        var provinceDesc     = checkdataAddress(data.data.address.houseProvince,titleProvince);//ชื่อจังหวัด
        var roadDesc         = checkdataAddress(data.data.address.houseRoad,"ถนน");//ชื่อถนน
        var subdistrictDesc  = checkdataAddress(data.data.address.houseSubDistrict,titleSubdistrict);//ชื่อตำบล
        var villageNo        = checkdataAddress(data.data.address.houseVillageNo,"หมู่ ");//หมู่ที่

        var address =  houseNo + villageNo + alleyWayDesc + alleyDesc + roadDesc + subdistrictDesc + districtDesc + provinceDesc;
        result.data.address  = address;


        var fullname = data.data.title + data.data.firstName +" "+ data.data.lastName;

        result.data.fullname  = fullname;


        $("#personalId").val(data.PersonalID);
        $("#fullname").val(fullname);
        $("#address").val(address);
        $("#dateOfBirth").val(dateThLinkage(data.data.dateOfBirth));
        $("#cardExpireDate").val(dateThLinkage(data.data.cardExpireDate));
        $("#cardIssueDate").val(dateThLinkage(data.data.cardIssueDate));
        $("#genderText").val(data.data.genderText);
        $("#nationalityText").val(data.data.nationalityText);


        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();
        var pid          = $("#personalId").val();
        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});
      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      if(data.Message == ""){
        data.Message = "ไม่พบข้อมูล";
      }
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
      reset();
    }

  })
  .fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  });
}

function reset()
{
  $("#pid").val("");
  $("#personalId").val("");
  $("#fullname").val("");
  $("#address").val("");
  $("#dateOfBirth").val("");
  $("#cardExpireDate").val("");
  $("#cardIssueDate").val("");
  $("#genderText").val("");
  $("#nationalityText").val("");

  $("#type_search").val("");

  result = "";
}

function printPdf()
{
  //console.log(result);
  //result="xxxx";
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}
