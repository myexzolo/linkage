var result = "";

$(function () {
  // $("#pid").bind("enterKey",function(e){
  //   readSmartCard("");
  // });
  // $("#pid").keyup(function(e){
  //     if(e.keyCode == 13)
  //     {
  //       $(this).trigger("enterKey");
  //     }
  // });

  showTable("");
})

function showTable(result)
{
  var res  = "";
  if(result != "")
  {
    res = JSON.stringify(result);
  }
  $.post( "ajax/showTable.php",{result:res})
  .done(function( data ) {
    $("#showTable").html( data );
  });
}


$('#formSearch').on('submit', function(event) {
  event.preventDefault();
  if ($('#formSearch').smkValidate()) {
    $.ajax({
        url: 'ajax/search.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      //console.log(data);
      if(data.status == "danger")
      {
          $.smkAlert({text: data.message,type: "warning"});
          showTable("");
      }else{
          //console.log(data);
          //data = "";
          setData(data);
      }
    });
  }
});


async function setData(datas)
{
  //console.log(datas);
  var pid          = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  var type_search  = "2";
  var data         = datas;

  for(var i=0; i < datas.data.length; i++)
  {
    var arr  = {};
    //console.log(data.data[i].pid+","+data.data[i].fullname );
    pid = datas.data[i].pid;

    var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
    await $.get(url)
    .done(function( res ) {
      //console.log(res);
      //$.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
      arr.pid   = res.PersonalID;
      if(res.Status != "Y"){
        arr.fullname           = "";
        arr.statusPerson       = "";
        arr.statusPersonDesc   = "ไม่พบข้อมูล";
      }else{
        //print_r($data);
        if(res.data.info != null)
        {
          arr.fullname           = "";
          arr.statusPerson       = "";
          arr.statusPersonDesc   = res.data.info;
        }else if(res.ReturnCode != "00000"){
          arr.fullname           = "";
          arr.statusPerson       = "";
          arr.statusPersonDesc   = "ไม่พบข้อมูล";
        }else{
          var titleDesc   = res.data.titleDesc?res.data.titleDesc:"";
          var firstName   = res.data.firstName?res.data.firstName:"";
          var middleName  = res.data.middleName?res.data.middleName:"";
          var lastName    = res.data.lastName?res.data.lastName:"";
          var statusOfPersonCode = res.data.statusOfPersonCode?res.data.statusOfPersonCode:"";
          var statusOfPersonDesc = res.data.statusOfPersonDesc?res.data.statusOfPersonDesc:"";

          var fullName = titleDesc+firstName;

          if(middleName != ""){
            fullName += " "+middleName;
          }
          fullName += " "+lastName;

          arr.fullname           = fullName;
          //data.data[i].pid']                = pid;

          arr.statusPerson       = statusOfPersonCode;
          arr.statusPersonDesc   = statusOfPersonDesc;

          if(res.data.statusOfPersonCode == "0")
          {
            arr.statusPerson = "ยังมีชีวิตอยู่";
          }else if(res.data.statusOfPersonCode == "1")
          {
            arr.statusPerson = "เสียชีวิตแล้ว";
          }else{
            arr.statusPersonDesc = res.Message;
          }
        }
        //var page_id  = $("#pageId").val();
        //$.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});
      }
       //console.log(arr);
       data.data[i] = arr;
       //console.log(data.data);
    })
    .fail(function (jqXHR, textStatus)
    {
        //console.log(jqXHR);
        $.smkAlert({text: "Service Error",type: "danger"});
    });
  }
  //console.log(data);
  result = data;
  showTable(data);
}

function reset()
{
  $("#filepath").val("");
  $("#type_search").val("");
  result = "";
  showTable(result);
}

function printPdf()
{
  //console.log(result);
  var res  = "";
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    res = result;

    res.page_id      = page_id;
    res.type_search  = type_search;

    res = JSON.stringify(res);

    var pram = "?result="+ res;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}

function exportExcel()
{
  var res  = "";
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    res = result;

    res.page_id      = page_id;
    res.type_search  = type_search;

    res = JSON.stringify(res);
    //console.log(result);
    var pram = "?result="+ res;
    var url = 'excelexport.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }
}
