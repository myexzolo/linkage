<?php
require_once '../../../Classes/PHPExcel.php';

include '../../../Classes/PHPExcel/IOFactory.php';
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

set_time_limit(1000);

$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

$officeCode     = $_POST['officeCode'];
$serviceCode    = $_POST['serviceCode'];
$versionCode    = $_POST['versionCode'];

$Message       = "";

if (file_exists($target_file)) {
  unlink($target_file);
}

$success = 0;
$fail    = 0;
$total   = 0;

$type_test = 1;
// {
//     "data": {
//         "titleCode": 3,
//         "titleDesc": "นาย",
//         "titleName": "นาย",
//         "titleSex": 1,
//         "firstName": "จรัล",
//         "middleName": "",
//         "lastName": "ผาสุขขี",
//         "genderCode": 1,
//         "genderDesc": "ชาย",
//         "dateOfBirth": 25210701,
//         "nationalityCode": 99,
//         "nationalityDesc": "ไทย",
//         "ownerStatusDesc": "เจ้าบ้าน",
//         "statusOfPersonCode": 0,
//         "statusOfPersonDesc": "บุคคลนี้มีภูมิลำเนาอยู่ในบ้านนี้",
//         "dateOfMoveIn": 25540725,
//         "age": 42,
//         "fatherPersonalID": 0,
//         "fatherName": "สมศักดิ์",
//         "fatherNationalityCode": 99,
//         "fatherNationalityDesc": "ไทย",
//         "motherPersonalID": 0,
//         "motherName": "แพงสี",
//         "motherNationalityCode": 99,
//         "motherNationalityDesc": "ไทย",
//         "fullnameAndRank": "นายจรัล ผาสุขขี",
//         "englishTitleDesc": "Mr.",
//         "englishFirstName": "Jaran",
//         "englishMiddleName": "",
//         "englishLastName": "Phasukkhee"
//     },
//     "PersonalID": "3440800530412",
//     "Message": "สำเร็จ",
//     "Status": "Y",
//     "ReturnCode": "00000"
// }

if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
   try
   {
       chmod($target_file,0777);

       $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
       $objReader = PHPExcel_IOFactory::createReader($inputFileType);
       $objReader->setReadDataOnly(true);
       $objPHPExcel = $objReader->load($inputFileName);

       $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
       $highestRow = $objWorksheet->getHighestRow();
       $highestColumn = $objWorksheet->getHighestColumn();

       $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
       $headingsArray = $headingsArray[1];

       $text = $headingsArray['A'];

          $headingsArray = array("A" => "pid");
          $r = -1;
          $namedDataArray = array();
          for ($row = 2; $row <= $highestRow; ++$row) {
              $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
              if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                  ++$r;
                  foreach($headingsArray as $columnKey => $columnHeading) {
                      $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                  }
              }
          }
          $i = 0;
          $arrData = array();
          if(count($namedDataArray) > 0)
          {
            foreach ($namedDataArray as $result) {
               try
               {
                     $total++;
                     $pid             = trim($result["pid"]);
                     $dataRes['pid']  = $pid;
                     $dataRes['fullname']           = "";
                     $dataRes['statusPerson']       = "";
                     $dataRes['statusPersonDesc']   = "";

                     $arrData[$i]     = $dataRes;
                     $i++;
                     $success++;
                     // $url = "http://localhost:20001/Readcard/";
                     // $Message = "";
                     //
                     // $params = array(
                     //    "pid" => $pid,
                     //    "officeCode" => $officeCode,
                     //    "versionCode" => $versionCode,
                     //    "serviceCode" => $serviceCode,
                     // );
                     // //print_r($params);
                     // $res   = httpPost($url,$params);
                     // //echo $res;
                     // if($res != ""){
                     //   $json  = json_decode($res, true);
                     //   //print_r($json);
                     //   $status  = $json['Status'];
                     //   $Message = $json['Message'];
                     //   $data    = $json['data'];
                     //   $ReturnCode   = $json['ReturnCode'];
                     //   //print_r($data);
                     //   //echo "status :".$status;
                     //   if($status != "Y"){
                     //     $fail++;
                     //     header('Content-Type: application/json');
                     //     exit(json_encode(array('status' => 'danger', 'message' => $Message , 'success'=>$success, 'fail'=>$fail, 'total'=>$total, 'detail1'=>$params)));
                     //   }else{
                     //     //print_r($data);
                     //     if(isset($data['info']))
                     //     {
                     //       $dataRes['fullname']           = "";
                     //       $dataRes['pid']                = $pid;
                     //       $dataRes['statusPerson']       = "";
                     //       $dataRes['statusPersonDesc']   = $data['info'];
                     //     }else if($ReturnCode != "00000"){
                     //       $fail++;
                     //       header('Content-Type: application/json');
                     //       exit(json_encode(array('status' => 'danger', 'message' => $Message , 'success'=>$success, 'fail'=>$fail, 'total'=>$total, 'detail2'=>$params)));
                     //     }else{
                     //       $titleDesc   = isset($data['titleDesc'])?$data['titleDesc']:"";
                     //       $firstName   = isset($data['firstName'])?$data['firstName']:"";
                     //       $middleName  = isset($data['middleName'])?$data['middleName']:"";
                     //       $lastName    = isset($data['lastName'])?$data['lastName']:"";
                     //       $statusOfPersonCode = isset($data['statusOfPersonCode'])?$data['statusOfPersonCode']:"";
                     //       $statusOfPersonDesc = isset($data['statusOfPersonDesc'])?$data['statusOfPersonDesc']:"";
                     //
                     //       $fullName = $titleDesc.$firstName;
                     //       if($middleName != ""){
                     //         $fullName .= " ".$middleName;
                     //       }
                     //       $fullName .= " ".$lastName;
                     //
                     //       $dataRes['fullname']           = $fullName;
                     //       $dataRes['pid']                = $pid;
                     //       $dataRes['statusPerson']       = $statusOfPersonCode;
                     //       $dataRes['statusPersonDesc']   = $statusOfPersonDesc;
                     //
                     //       if($dataRes['statusPerson'] == "0")
                     //       {
                     //         $dataRes['statusPerson'] = "ยังมีชีวิตอยู่";
                     //       }else if($dataRes['statusPerson'] == "1")
                     //       {
                     //         $dataRes['statusPerson'] = "เสียชีวิตแล้ว";
                     //       }else{
                     //         $dataRes['statusPersonDesc'] = $Message;
                     //       }
                     //
                     //     }
                     //
                     //
                     //     $arrData[$i] = $dataRes;
                     //   }
                     // }else{
                     //   $fail++;
                     //   header('Content-Type: application/json');
                     //   exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบโปรแกรม Agent' , 'data' => $arrData, 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
                     // }
               }catch (Exception $ex) {
                 $fail++;
               }
            }
            header('Content-Type: application/json');
            exit(json_encode(array('status' => 'success', 'message' => $Message , 'data' => $arrData, 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
          }else{
            header('Content-Type: application/json');
            exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบข้อมูล', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
          }
   } catch (Exception $e) {
     header('Content-Type: application/json');
     exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));
   }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger', 'message' => 'ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง', 'success'=>$success, 'fail'=>$fail, 'total'=>$total )));
  //echo "ไม่พบไฟล์หรือรูปแบบไฟล์ไม่ถูกต้อง";
}
?>
