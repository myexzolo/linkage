<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<div class="box-body">
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:80px;">ลำดับ</th>
      <th style="width:200px;">เลขที่ประจำตัวประชาชน</th>
      <th>ชื่อ - สกุล</th>
      <th style="width:200px;">สถานะ</th>
      <th>รายละเอียด</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //echo $_POST['result'];
    if(isset($_POST['result']) && $_POST['result'] != "")
    {
      $result = json_decode($_POST['result'], true);

      if(isset($result['data']))
      {

        $data   = $result['data'];

        for($i = 0; $i < count($data); $i++)
        {

        ?>
          <tr class="text-center">
            <td><?=$i+1;?></td>
            <td><?= $data[$i]['pid'];?></td>
            <td style="text-align:left;"><?=$data[$i]['fullname'];?></td>
            <td style="text-align:center;"><?= $data[$i]['statusPerson'];?></td>
            <td style="text-align:left;"><?= $data[$i]['statusPersonDesc'];?></td>
          </tr>
        <?php
          }
        }
      }
    ?>
  </tbody>
</table>
</div>
<div class="box-footer">  
  <div class="col-md-12" style="text-align:center">
    <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
    <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
    <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
    <button type="button" class="btn btn-info btn-flat" onclick="exportExcel()" style="width:160px;font-size:20px;height:40px;">Export Excel</button>
  </div>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
