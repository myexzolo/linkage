<?php
include('../../inc/function/mainFunc.php');
include('../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

/** Error reporting */
error_reporting(E_ALL);

//date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../../Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("TCL")
							 ->setLastModifiedBy("TCL")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Report01 file");

//$dateStart  	= $_POST['dateStart'];
$r 	 = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG");

// $sheet = $objPHPExcel->getActiveSheet();
//
// $sheet->getStyle('A1')->applyFromArray(array(
//     'borders' => array(
//         'left' => array(
//             'style' => PHPExcel_Style_Border::BORDER_THICK,
//             'color' => array(
//                 'rgb' => 'FF0000',
//             ),
//         ),
//     ),
// ));

$name = "รายงานข้อมูลตรวจสอบสถานะการเสียชีวิต";

$objPHPExcel->getActiveSheet()->setTitle("ข้อมูลตรวจสอบสถานะการเสียชีวิต");
$objPHPExcel->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
$objPHPExcel->getActiveSheet()->mergeCells('A1:E1')->setCellValue('A1',$name)->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue('A2','ลำดับ')->getStyle('A2:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue('B2','เลขที่ประจำตัวประชาชน')->getStyle('B2:B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue('C2','ชื่อ - สกุล')->getStyle('C2:C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue('D2','สถานะ')->getStyle('D2:D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue('E2','รายละเอียด')->getStyle('E2:E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);



if(isset($_POST['result']) && $_POST['result'] != "")
{
	$result = json_decode($_POST['result'], true);

	if(isset($result['data']))
	{
		$c = 3;
		$data   = $result['data'];
		for($i = 0; $i < count($data); $i++)
		{

			  $pid = $data[$i]['pid']." ";
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$c, $i+1);
		    $objPHPExcel->getActiveSheet()->setCellValue('B'.$c, $pid);
		    $objPHPExcel->getActiveSheet()->setCellValue('C'.$c, $data[$i]['fullname']);
		    $objPHPExcel->getActiveSheet()->setCellValue('D'.$c, $data[$i]['statusPerson']);
		    $objPHPExcel->getActiveSheet()->setCellValue('E'.$c, $data[$i]['statusPersonDesc']);
				$c++;
		}
	}
}

foreach(range('A','E') as $columnID) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
        ->setAutoSize(true);
}

$objPHPExcel->setActiveSheetIndex(0);
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');



?>
