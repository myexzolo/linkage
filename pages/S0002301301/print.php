  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';
  $nameUser    = $_SESSION['member'][0]['user_name'];

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
  ?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลการจดทะเบียนเปลี่ยนชื่อตัว</div>
      <br>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
        <thead>
          <tr class="text-center">
            <th class="thStyle" style="width:40px;">ลำดับ</th>
            <th class="thStyle" style="width:120px;">เลขประจำตัวประชาชน</th>
            <th class="thStyle" >ชื่อ-นามสกุล</th>
            <th class="thStyle">ชื่อใหม่</th>
            <th class="thStyle" style="width:90px;">วันเดือนปี เกิด</th>
            <th class="thStyle" style="width:60px;">เพศ</th>
            <th class="thStyle" style="width:60px;">สัญชาติ</th>
            <th class="thStyle">ชื่อบิดา</th>
            <th class="thStyle">ชื่อมารดา</th>
            <!-- <th class="thStyle">ที่อยู่</th> -->
            <th class="thStyle" style="width:120px;">สนท.ที่ออกหนังสือ</th>
            <th class="thStyle" style="width:90px;">วันที่ออกหนังสือ</th>
          </tr>
        </thead>
        <tbody>
          <?php
          //echo $_POST['result'];
          if($_POST['result'] != "")
          {
            $result = json_decode($_POST['result'], true);
            $data   = $result['data'];
            if(isset($data['allName']))
            {
                $allNameArr = $data['allName'];
                for($i = 0; $i < count($allNameArr); $i++)
                {
                $allName = $allNameArr[$i];

                $address = "";

                if($allName['hno'] != "")
                {
                  $address .= "เลขที่ ".$allName['hno'];
                }

                if($allName['trokDesc'] != "")
                {
                  $address .= " ".$allName['trokDesc'];
                }

                if($allName['soiDesc'] != "")
                {
                  $address .= " ".$allName['soiDesc'];
                }

                if($allName['thanonDesc'] != "")
                {
                  $address .= " ".$allName['thanonDesc'];
                }

                if($allName['districtDesc'] != "")
                {
                  $address .= " ".$allName['districtDesc'];
                }

                if($allName['amphorDesc'] != "")
                {
                  $address .= " ".$allName['amphorDesc'];
                }

                if($allName['provinceDesc'] != "")
                {
                  $address .= " ".$allName['provinceDesc'];
                }

            ?>
              <tr class="text-center">
                <td class="content" align="center"><?=$i+1;?></td>
                <td class="content" ><?= $allName['pid'];?></td>
                <td class="content" style="text-align:left;"><?= $allName['fullNameAndRank'] ?></td>
                <td class="content" style="text-align:left;"><?= $allName['newName'] ?></td>
                <td class="content" style="text-align:left;"><?= dateThLinkage($allName['dateOfBirth']) ?></td>
                <td class="content" align="center"><?= $allName['genderDesc'];?></td>
                <td class="content" align="center"><?= $allName['nationalityDesc'];?></td>
                <td class="content" ><?= $allName['fatherFirstName'];?></td>
                <td class="content" ><?= $allName['motherFirstName'];?></td>
                <!-- <td style="text-align:left;"><?= $address;?></td> -->
                <td class="content" align="center"><?= $allName['docPlaceDesc'];?></td>
                <td class="content" align="center"><?= dateThLinkage($allName['docDate']) ?></td>
              </tr>
            <?php
                }
              }
            }
          ?>
        </tbody>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลการจดทะเบียนเปลี่ยนชื่อตัว');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('L','','','','',5,5,5,5,5,5);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
