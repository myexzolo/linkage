<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">ลำดับ</th>
      <th>เลขประจำตัวประชาชน</th>
      <th>ชื่อ-นามสกุล</th>
      <th>ชื่อใหม่</th>
      <th>วันเดือนปี เกิด</th>
      <th>เพศ</th>
      <th>สัญชาติ</th>
      <th>ชื่อบิดา</th>
      <th>ชื่อมารดา</th>
      <!-- <th>ที่อยู่</th> -->
      <th>สนท.ที่ออกหนังสือ</th>
      <th>วันที่ ออกหนังสืออนุญาต</th>
    </tr>
  </thead>
  <tbody>
    <?php
    //echo $_POST['result'];
    if($_POST['result'] != "")
    {
      $result = json_decode($_POST['result'], true);
      $data   = $result['data'];
      if(isset($data['allName']))
      {

        $allNameArr = $data['allName'];
      //print_r($data);
        for($i = 0; $i < count($allNameArr); $i++)
        {
            $allName = $allNameArr[$i];

            // age: 7     //อายุผู้ขอใบอนุญาต ณ วันที่ออกใบอนุญาต
            // amphorDesc: "เขตคันนายาว"  //ชื่ออำเภอ ตามที่อยู่ของผู้ขอใบอนุญาต
            // dateOfBirth: 25331019 //ปี เดือน วันเกิดของผู้ขอใบอนุญาต
            // districtDesc: "คันนายาว"
            // docDate: 25411001
            // docID: 6142541
            // docPlace: "1043"
            // docPlaceDesc: "ท้องถิ่นเขตคันนายาว"
            // docPlaceProvince: "กรุงเทพมหานคร"
            // fatherFirstName: "สมศักดิ์"
            // firstName: "คทายุทธ์"
            // fullNameAndRank: "ด.ช.คทายุทธ์ สมบัติกำจร"
            // genderCode: 1
            // genderDesc: "ชาย"
            // hno: "74/240"
            // hrcode: "1043"
            // hrcodeDesc: "ท้องถิ่นเขตคันนายาว"
            // lastName: "สมบัติกำจร"
            // middleName: ""
            // motherFirstName: "อัจฉรา"
            // nationalityCode: 99
            // nationalityDesc: "ไทย"
            // newName: "คงอุดม"
            // pid: 1103700282131
            // provinceDesc: "กรุงเทพมหานคร"
            // requestDate: 0
            // requestID: 0
            // requestYear: 0
            // soiDesc: ""
            // thanonDesc: ""
            // titleCode: 1
            // titleDesc: "ด.ช."
            // trokDesc: ""


            // age: 12
            // amphorDesc: "เขตคันนายาว"
            // dateOfBirth: 25331019
            // districtDesc: "คันนายาว"
            // docDate: 25460606
            // docID: 50532546
            // docPlace: "1043"
            // docPlaceDesc: "ท้องถิ่นเขตคันนายาว"
            // docPlaceProvince: "กรุงเทพมหานคร"
            // fatherFirstName: "พันธ์ศักดิ์"
            // firstName: "คงอุดม"
            // fullNameAndRank: "ด.ช.คงอุดม สมบัติกำจร"
            // genderCode: 1
            // genderDesc: "ชาย"
            // hno: "74/240"
            // hrcode: "1043"
            // hrcodeDesc: "ท้องถิ่นเขตคันนายาว"
            // lastName: "สมบัติกำจร"
            // middleName: ""
            // motherFirstName: "อัจฉราทิพย์"
            // nationalityCode: 99
            // nationalityDesc: "ไทย"
            // newName: "นนทศักดิ์"
            // pid: 1103700282131
            // provinceDesc: "กรุงเทพมหานคร"
            // requestDate: 0
            // requestID: 0
            // requestYear: 0
            // soiDesc: ""
            // thanonDesc: ""
            // titleCode: 1
            // titleDesc: "ด.ช."
            // trokDesc: ""



            $address = "";

            if($allName['hno'] != "")
            {
              $address .= "เลขที่ ".$allName['hno'];
            }

            if($allName['trokDesc'] != "")
            {
              $address .= " ".$allName['trokDesc'];
            }

            if($allName['soiDesc'] != "")
            {
              $address .= " ".$allName['soiDesc'];
            }

            if($allName['thanonDesc'] != "")
            {
              $address .= " ".$allName['thanonDesc'];
            }

            if($allName['districtDesc'] != "")
            {
              $address .= " ".$allName['districtDesc'];
            }

            if($allName['amphorDesc'] != "")
            {
              $address .= " ".$allName['amphorDesc'];
            }

            if($allName['provinceDesc'] != "")
            {
              $address .= " ".$allName['provinceDesc'];
            }

      ?>
          <tr class="text-center">
            <td><?=$i+1;?></td>
            <td><?= $allName['pid'];?></td>
            <td style="text-align:left;"><?= $allName['fullNameAndRank'] ?></td>
            <td style="text-align:left;"><?= $allName['newName'] ?></td>
            <td style="text-align:left;"><?= dateThLinkage($allName['dateOfBirth']) ?></td>
            <td ><?= $allName['genderDesc'];?></td>
            <td ><?= $allName['nationalityDesc'];?></td>
            <td ><?= $allName['fatherFirstName'];?></td>
            <td ><?= $allName['motherFirstName'];?></td>
            <!-- <td style="text-align:left;"><?= $address;?></td> -->
            <td><?= $allName['docPlaceDesc'];?></td>
            <td><?= dateThLinkage($allName['docDate']) ?></td>
          </tr>
      <?php
          }
        }
      }
    ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
