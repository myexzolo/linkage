  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';
  $nameUser    = $_SESSION['member'][0]['user_name'];

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
  ?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลมูลนิธิ</div>
      <br>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
        <thead>
          <tr class="text-center">
            <th class="thStyle" style="width:40px;">ลำดับ</th>
            <th class="thStyle">ชื่อสมาคม</th>
            <th class="thStyle">ชื่อ-นามสกุลผู้ลงนาม</th>
            <th class="thStyle">เลขที่ใบจัดตั้ง</th>
            <th class="thStyle">ประเภทใบจัดตั้ง</th>
            <th class="thStyle">ตำแหน่ง</th>
            <th class="thStyle">วันที่ออกใบจัดตั้ง</th>
            <th class="thStyle">เลขรหัสประจำบ้าน</th>
            <th class="thStyle">ที่อยู่</th>
            <th class="thStyle">สำนักทะเบียน</th>
          </tr>
        </thead>
        <tbody>
          <?php
          //echo $_POST['result'];
          if($_POST['result'] != "")
          {
            $result = json_decode($_POST['result'], true);
            $data   = isset($result['data'])?$result['data']:"";
            if($data != "")
            {
              for($i = 0; $i < count($data); $i++)
              {
                  $allName = $data[$i]['allName'];

                  $address = "";

                  if($allName['hno'] != "")
                  {
                    $address .= "เลขที่ ".$allName['hno'];
                  }

                  if($allName['trok'] != "")
                  {
                    $address .= " ".$allName['trok'];
                  }

                  if($allName['soi'] != "")
                  {
                    $address .= " ".$allName['soi'];
                  }

                  if($allName['thanon'] != "")
                  {
                    $address .= " ".$allName['thanon'];
                  }

                  if($allName['districtDesc'] != "")
                  {
                    $address .= " ".$allName['districtDesc'];
                  }

                  if($allName['amphorDesc'] != "")
                  {
                    $address .= " ".$allName['amphorDesc'];
                  }

                  if($allName['provinceDesc'] != "")
                  {
                    $address .= " ".$allName['provinceDesc'];
                  }


              ?>
                <tr class="text-center">
                  <td class="content" ><?=$i+1;?></td>
                  <td class="content" ><?= $allName['associationName'];?></td>
                  <td class="content" style="text-align:left;"><?=$driverLicenceInfo['signTitleDesc']."".$driverLicenceInfo['signFullName']?></td>
                  <td class="content" style="text-align:left;"><?= $allName['docID'];?></td>
                  <td class="content" style="text-align:left;"><?= $allName['associationType'];?></td>
                  <td class="content" style="text-align:left;"><?= $allName['associationPosition'];?></td>
                  <td class="content" ><?= dateThLinkage2($allName['docDate']);?></td>
                  <td class="content" ><?= $allName['hid'];?></td>
                  <td class="content" style="text-align:left;"><?= $address;?></td>
                  <td class="content" ><?= $allName['docPlaceDesc'];?></td>
                </tr>
              <?php
              }
            }else{
          ?>
            <tr class="text-center">
              <td class="content" >&nbsp;</td>
              <td class="content" ></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" ></td>
              <td class="content" ></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" ></td>
            </tr>
          <?php
            }
          }else{
            ?>
            <tr class="text-center">
              <td class="content" >&nbsp;</td>
              <td class="content" ></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" ></td>
              <td class="content" ></td>
              <td class="content" style="text-align:left;"></td>
              <td class="content" ></td>
            </tr>
            <?php
            }
          ?>
        </tbody>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลมูลนิธิ');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('L','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
