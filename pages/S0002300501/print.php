  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $result                 = "";
  $personalId             = "";
  $femaleAge              = "";
  $femaleDateOfBirth      = "";
  $femaleFullnameAndRank  = "";
  $femaleNationalityDesc  = "";
  $femalePID              = "";
  $maleAge                = "";
  $maleDateOfBirth        = "";
  $maleFullnameAndRank    = "";
  $maleNationalityDesc    = "";
  $malePID                = "";
  $marryDate              = "";
  $marryID                = "";
  $marryPlaceDesc         = "";
  $marryPlaceProvince     = "";
  $marryTime              = "";
  $marryType              = "";

  $fullname       = str_repeat('&nbsp;', 40);

  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];


  if(isset($_POST['result']))
  {

    $result     = json_decode($_POST['result'], true);
    //print_r($result);

    if(isset($result['data']))
    {
      $personalID             = $result['PersonalID'];
      $address                = $result['data']['address'];
      $femaleAge              = $result['data']['femaleAge'];
      $femaleDateOfBirth      = $result['data']['femaleDateOfBirth'];
      $femaleFullnameAndRank  = $result['data']['femaleFullnameAndRank'];
      $femaleNationalityDesc  = $result['data']['femaleNationalityDesc'];
      $femalePID              = $result['data']['femalePID'];
      $maleAge                = $result['data']['maleAge'];
      $maleDateOfBirth        = $result['data']['maleDateOfBirth'];
      $maleFullnameAndRank    = $result['data']['maleFullnameAndRank'];
      $maleNationalityDesc    = $result['data']['maleNationalityDesc'];
      $malePID                = $result['data']['malePID'];
      $marryDate              = $result['data']['marryDate'];
      $marryID                = $result['data']['marryID'];
      $marryPlaceDesc         = $result['data']['marryPlaceDesc'];
      $marryPlaceProvince     = $result['data']['marryPlaceProvince'];
      $marryTime              = $result['data']['marryTime'];
      $marryType              = $result['data']['marryType'];

      $page_id                = $result['page_id'];
      $type_search            = $result['type_search'];

      if($personalID == $femalePID){
        $fullname = $femaleFullnameAndRank;
      }else if($personalID == $malePID){
        $fullname = $maleFullnameAndRank;
      }

      if($personalId != "")
      {
            $sql = "INSERT INTO t_history_search
            (user_id,pid,page_id,type_search,type_report)
            VALUES
            ('$user_id','$personalId','$page_id','$type_search','P')";
            DbQuery($sql,null);
      }
    }
  }

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลทะเบียนสมรส </div>
      <br>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td>เลขจดทะเบียนสมรส</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $marryID ?></td>
        </tr>
        <tr>
          <td>วันที่จดทะเบียนสมรส</td>
          <td> : </td>
          <td><?= $marryDate ?></td>
        </tr>
        <tr>
          <td>เวลาจดทะเบียนสมรส</td>
          <td> : </td>
          <td><?= $marryTime ?></td>
        </tr>
        <tr>
          <td>สนท.ที่จดทะเบียนสมรส</td>
          <td> : </td>
          <td><?= $marryPlaceDesc ?></td>
        </tr>
        <tr>
          <td>จังหวัดที่จดทะเบียนสมรส</td>
          <td> : </td>
          <td><?= $marryPlaceProvince ?></td>
        </tr>
        <tr>
          <td>ประเภทของการสมรส</td>
          <td> : </td>
          <td><?= $marryType ?></td>
        </tr>
        <tr>
          <td>เลขประจำตัวประชาชน (ชาย)</td>
          <td> : </td>
          <td><?= $malePID ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล (ชาย)</td>
          <td> : </td>
          <td><?= $maleFullnameAndRank ?></td>
        </tr>
        <tr>
          <td>อายุ (ชาย)</td>
          <td> : </td>
          <td><?= $maleAge ?></td>
        </tr>
        <tr>
          <td>วัน เดือน ปีเกิด (ชาย)</td>
          <td> : </td>
          <td><?= $maleDateOfBirth ?></td>
        </tr>
        <tr>
          <td>สัญชาติ (ชาย)</td>
          <td> : </td>
          <td><?= $maleNationalityDesc ?></td>
        </tr>
        <tr>
          <td>เลขประจำตัวประชาชน (หญิง)</td>
          <td> : </td>
          <td><?= $femalePID ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล (หญิง)</td>
          <td> : </td>
          <td><?= $femaleFullnameAndRank ?></td>
        </tr>
        <tr>
          <td>อายุ (หญิง)</td>
          <td> : </td>
          <td><?= $femaleAge ?></td>
        </tr>
        <tr>
          <td>วัน เดือน ปีเกิด (หญิง)</td>
          <td> : </td>
          <td><?= $femaleDateOfBirth ?></td>
        </tr>
        <tr>
          <td>สัญชาติ (หญิง)</td>
          <td> : </td>
          <td><?= $femaleNationalityDesc ?></td>
        </tr>
      </table>
      <div style="height:100px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $fullname ?> )</td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลทะเบียนสมรส');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
