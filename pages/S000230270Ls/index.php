<!DOCTYPE html>
  <?php
  $officeCode   = @$_POST["officeCode"];
  $serviceCode  = @$_POST["serviceCode"];
  $versionCode  = @$_POST["versionCode"];
  $pageDetail   = @$_POST["pageDetail"];
  $pageId       = @$_POST["pageId"];

  $code = $officeCode.$serviceCode.$versionCode;
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบฐานข้อมูลประชาชนและการบริการภาครัฐ(Linkage Center) - <?=$pageDetail ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/S000230030L.css">
    </head>
      <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->

          <section class="content-header">
            <h1>
              <?=$pageDetail ?>
              <small><?=$code ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"></i> <?=$pageDetail ?></a></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <!-- Main row -->
            <div class="row">
              <!-- Left col -->
              <div class="col-md-12">
                <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
                  <div class="box-header with-border">
                    <div>
                      <!-- <form id="formSearch" novalidate enctype="multipart/form-data"> -->
                      <form enctype="multipart/form-data" action="ajax/search.php" method="post">
                      <div class="row">
                        <div class="col-md-12">
                          <button type="button" class="btn btn-success btn-flat" onclick="postURL_blank('../../upload/check_death_TM.xlsx')" style="width:200px;font-size:20px;height:40px;">Excel Template</button>
                          <button type="submit" class="pull-right btn btn-primary btn-flat" style="width:100px;font-size:20px;height:40px;margin-left:10px;">ค้นหา</button>
                          <div class="pull-right">
                            <input type="file" name="filepath" class="form-control custom-file-input" id='filepath' style="width:250px" accept=".xlsx,.xls" >
                            <input value="<?=$officeCode ?>" id="officeCode" name="officeCode" type="hidden">
                            <input value="<?=$serviceCode ?>" id="serviceCode" name="serviceCode" type="hidden">
                            <input value="<?=$versionCode ?>" id="versionCode" name="versionCode" type="hidden">
                            <input value="<?=$pageId ?>" id="pageId" type="hidden">
                            <input value="" id="type_search" type="hidden">
                          </div>
                          <label for="filepath" class="pull-right control-label" style="width:80px;line-height:40px;">File Excel :</label>
                        </div>
                      </div>
                    </form>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div id="showTable"></div>
                </div>
              </div>
            </div>
            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/S000230030L.js"></script>
    </body>
  </html>
