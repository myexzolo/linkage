  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");
  // echo $imagePath;
  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
  ?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลตรวจสอบสถานะการเสียชีวิต</div>
      <br>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
        <thead>
          <tr class="text-center">
            <th class="thStyle" style="width:55px;">ลำดับ</th>
            <th class="thStyle" style="width:160px;">เลขที่ประจำตัวประชาชน</th>
            <th class="thStyle">ชื่อ - สกุล</th>
            <th class="thStyle" style="width:100px;">สถานะ</th>
            <th class="thStyle" >รายละเอียด</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(isset($_POST['result']) && $_POST['result'] != "")
          {
          	$result = json_decode($_POST['result'], true);

          	if(isset($result['data']))
          	{
          		$c = 3;
          		$data           = $result['data'];
              $page_id        = $result['page_id'];
              $type_search    = $result['type_search'];
          		for($i = 0; $i < count($data); $i++)
          		{
                $personalId = $data[$i]['pid'];
                ?>
                <tr class="text-center">
                  <td class="content"><?=$i+1;?></td>
                  <td class="content"><?= $personalId;?></td>
                  <td class="content" style="text-align:left;"><?=$data[$i]['fullname'];?></td>
                  <td class="content" style="text-align:left;"><?= $data[$i]['statusPerson'];?></td>
                  <td class="content"style="text-align:left;"><?= $data[$i]['statusPersonDesc'];?></td>
                </tr>
                <?php
                  if($personalId != "")
                  {
                        $sql = "INSERT INTO t_history_search
                        (user_id,pid,page_id,type_search,type_report)
                        VALUES
                        ('$user_id','$personalId','$page_id','$type_search','P')";
                        DbQuery($sql,null);
                  }
          		}
          	}
          }
          ?>
        </tbody>
      </table>
      <div style="height:100px;"></div>
      <!-- <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $fullname ?> )</td>
        </tr>
      </table> -->
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลตรวจสอบสถานะการเสียชีวิต');
    // $mpdf->SetWatermarkImage('../../image/logoWM.jpg');
    // $mpdf->showWatermarkImage = true;
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
