var result = "";

$(function () {
  // $("#pid").bind("enterKey",function(e){
  //   readSmartCard("");
  // });
  // $("#pid").keyup(function(e){
  //     if(e.keyCode == 13)
  //     {
  //       $(this).trigger("enterKey");
  //     }
  // });

  showTable("");
})

function showTable(result)
{
  var res  = "";
  if(result != "")
  {
    res = JSON.stringify(result);
  }
  $.post( "ajax/showTable.php",{result:res})
  .done(function( data ) {
    $("#showTable").html( data );
  });
}


$('#formSearch').on('submit', function(event) {
  event.preventDefault();
  if ($('#formSearch').smkValidate()) {
    $.ajax({
        url: 'ajax/search.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      //console.log(data);
      if(data.status == "danger")
      {
          $.smkAlert({text: data.message,type: "warning"});
          showTable("");
      }else{
        result = data;
        showTable(result);
      }
    });
  }
});

function reset()
{
  $("#filepath").val("");
  $("#type_search").val("");
  result = "";
  showTable(result);
}

function printPdf()
{
  //console.log(result);
  var res  = "";
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    res = result;

    res.page_id      = page_id;
    res.type_search  = type_search;

    res = JSON.stringify(res);

    var pram = "?result="+ res;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}

function exportExcel()
{
  var res  = "";
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    res = result;

    res.page_id      = page_id;
    res.type_search  = type_search;

    res = JSON.stringify(res);
    //console.log(result);
    var pram = "?result="+ res;
    var url = 'excelexport.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }
}
