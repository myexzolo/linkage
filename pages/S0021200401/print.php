  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';
  $nameUser    = $_SESSION['member'][0]['user_name'];

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
  ?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลใบอนุญาตขับรถ กรมการขนส่งทางบก</div>
      <br>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;" width = "100%">
        <thead>
          <tr class="text-center">
            <th class="thStyle" style="width:40px;">ลำดับ</th>
            <th class="thStyle" style="width:100px;">เลขที่เอกสาร</th>
            <th class="thStyle" style="width:170px;">ชื่อ - สกุล</th>
            <th class="thStyle" style="width:40px;">เพศ</th>
            <th class="thStyle" style="width:50px;">สัญชาติ</th>
            <th class="thStyle" >ที่อยู่</th>
            <th class="thStyle" style="width:90px;">เลขที่ใบอนุญาต</th>
            <th class="thStyle" style="width:140px;">ชนิดใบอนุญาต</th>
            <th class="thStyle" style="width:105px;">วันที่ออกใบอนุญาต</th>
            <th class="thStyle" style="width:115px;">วันที่สิ้นอายุใบอนุญาต</th>
          </tr>
        </thead>
        <tbody>
          <?php
          //echo $_POST['result'];
          if($_POST['result'] != "")
          {
            $result = json_decode($_POST['result'], true);
            $data   = $result['data'];
            for($i = 0; $i < count($data); $i++)
            {
              $driverLicenceInfo = $data[$i]['driverLicenceInfo'];
              // print_r($driverLicenceInfo);
              $gender = "";
              if($driverLicenceInfo['sex'] == '1')
              {
                $gender = "ชาย";
              }else if($driverLicenceInfo['sex'] == '2'){
                $gender = "หญิง";
              }else if($driverLicenceInfo['sex'] == '0'){
                $gender = "ไม่ระบุ";
              }

              $address = " ";
              if($driverLicenceInfo['bldName'] != "")
              {
                $address .= $driverLicenceInfo['bldName']." ";
              }

              if($driverLicenceInfo['villageName'] != "")
              {
                $address .= $driverLicenceInfo['villageName']." ";
              }

              if($driverLicenceInfo['villageNo'] != "")
              {
                $address .= "หมู่ที่ ".$driverLicenceInfo['villageNo']." ";
              }

              if($driverLicenceInfo['soi'] != "")
              {
                $address .= $driverLicenceInfo['soi']." ";
              }

              if($driverLicenceInfo['street'] != "")
              {
                $address .= $driverLicenceInfo['street']." ";
              }

              $zipCode = "";
              if($driverLicenceInfo['zipCode'] != "")
              {
                $zipCode = " ".$driverLicenceInfo['zipCode'];
              }
            ?>
              <tr class="text-center">
                <td class="content" align="center"><?=$i+1;?></td>
                <td class="content" align="center"><?= $driverLicenceInfo['docNo'];?></td>
                <td class="content" style="text-align:left;"><?=$driverLicenceInfo['titleDesc']."".$driverLicenceInfo['fName']." ".$driverLicenceInfo['lName'];?></td>
                <td class="content" align="center"><?= $gender;?></td>
                <td class="content" align="center"><?= $driverLicenceInfo['natDesc'];?></td>
                <td class="content" style="text-align:left;"><?= $driverLicenceInfo['addrNo'].$address.$driverLicenceInfo['locFullDesc'].$zipCode;?></td>
                <td class="content" align="center"><?= $driverLicenceInfo['pltNo'];?></td>
                <td class="content" style="text-align:left;"><?= $driverLicenceInfo['pltDesc'];?></td>
                <td class="content" align="center"><?= dateThLinkage2($driverLicenceInfo['issDate']);?></td>
                <td class="content" align="center"><?= dateThLinkage2($driverLicenceInfo['expDate']);?></td>
              </tr>
            <?php
              }
            }
          ?>
        </tbody>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลทะเบียนบ้าน (รายการคนในบ้าน)');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('L','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
