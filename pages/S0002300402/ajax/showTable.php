<div class="box-body">
  <div class="row">
    <div class="col-md-12">
        <label style="font-size:22px;">ลำดับที่ 1</label>
        <div style="border-top:2px #ccc solid;width:100%;margin-bottom:15px;"></div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขประจำตัวประชาชนบุตร</label>
        <input value="" id="childPID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขที่หนังสือเดินทางบุตร</label>
        <input value="" id="childPassport" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อ - สกุล บุตร</label>
        <input value="" id="childFullnameAndRank" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>อายุบุตร</label>
        <input value="" id="childAge" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ปี เดือน วันเกิดของบุตร</label>
        <input value="" id="childDateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เพศบุตร</label>
        <input value="" id="childGenderDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สัญชาติบุตร</label>
        <input value="" id="childNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขทะเบียนในการจดทะเบียนรับรองบุตร</label>
        <input value="" id="docID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>รหัสสถานที่จดทะเบียน</label>
        <input value="" id="docPlace" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สนท.ที่จดทะเบียน</label>
        <input value="" id="docPlaceDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>จังหวัดที่จดทะเบียน</label>
        <input value="" id="docPlaceProvince" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ปี เดือน วันที่จดทะเบียน</label>
        <input value="" id="docDate" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เวลาที่จดทะเบียนรับรองบุตร</label>
        <input value="" id="docTime" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ประเภทของการรับรองบุตร</label>
        <input value="" id="docType" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ลำดับที่ของบุตร ต่อเลขทะเบียน 1 เลข</label>
        <input value="" id="docSequence" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขที่หนังสือเดินทางบิดา</label>
        <input value="" id="fatherPassport" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ - สกุล บิดา</label>
        <input value="" id="fatherFullnameAndRank" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>อายุบิดา</label>
        <input value="" id="fatherAge" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ปี เดือน วันเกิดของบิดา</label>
        <input value="" id="fatherDateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สัญชาติของบิดา</label>
        <input value="" id="fatherNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-12">
        <label style="font-size:22px;">ลำดับที่ 2</label>
        <div style="border-top:2px #ccc solid;width:100%;margin-bottom:15px;"></div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขประจำตัวประชาชนบุตร</label>
        <input value="" id="childPID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขที่หนังสือเดินทางบุตร</label>
        <input value="" id="childPassport" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อ - สกุล บุตร</label>
        <input value="" id="childFullnameAndRank" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>อายุบุตร</label>
        <input value="" id="childAge" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ปี เดือน วันเกิดของบุตร</label>
        <input value="" id="childDateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เพศบุตร</label>
        <input value="" id="childGenderDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สัญชาติบุตร</label>
        <input value="" id="childNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขทะเบียนในการจดทะเบียนรับรองบุตร</label>
        <input value="" id="docID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>รหัสสถานที่จดทะเบียน</label>
        <input value="" id="docPlace" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สนท.ที่จดทะเบียน</label>
        <input value="" id="docPlaceDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>จังหวัดที่จดทะเบียน</label>
        <input value="" id="docPlaceProvince" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ปี เดือน วันที่จดทะเบียน</label>
        <input value="" id="docDate" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เวลาที่จดทะเบียนรับรองบุตร</label>
        <input value="" id="docTime" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ประเภทของการรับรองบุตร</label>
        <input value="" id="docType" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ลำดับที่ของบุตร ต่อเลขทะเบียน 1 เลข</label>
        <input value="" id="docSequence" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขที่หนังสือเดินทางบิดา</label>
        <input value="" id="fatherPassport" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อ - สกุล บิดา</label>
        <input value="" id="fatherFullnameAndRank" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>อายุบิดา</label>
        <input value="" id="fatherAge" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ปี เดือน วันเกิดของบิดา</label>
        <input value="" id="fatherDateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สัญชาติของบิดา</label>
        <input value="" id="fatherNationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
  </div>
</div>
<div class="box-footer">
  <div class="col-md-12" style="text-align:center">
    <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
    <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
    <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
  </div>
</div>
