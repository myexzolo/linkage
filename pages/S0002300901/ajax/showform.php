<div class="box-body">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label>เลขที่ประจำตัวประชาชน</label>
        <input value="" id="personalId" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อ - สกุล (ภาษาไทย)</label>
        <input value="" id="fullname" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อ - สกุล (ภาษาอังกฤษ)</label>
        <input value="" id="fullname_en" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-8">
      <div class="form-group">
        <label>ที่อยู่</label>
        <input value="" id="address" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เมือง (กรณีอยู่ต่างประเทศ)</label>
        <input value="" id="foreignCountryCity" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ประเทศ (กรณีอยู่ต่างประเทศ)</label>
        <input value="" id="foreignCountry" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เพศ</label>
        <input value="" id="gender" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>วันเดือนปี เกิด</label>
        <input value="" id="dateOfBirth" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>หมู่โลหิต</label>
        <input value="" id="blood" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ศาสนา</label>
        <input value="" id="religion" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>ศาสนา (อื่นๆ)</label>
        <input value="" id="religionOther" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>วันเดือนปี ที่ออกบัตร</label>
        <input value="" id="dateIssue" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>วันเดือนปี บัตรหมดอายุ</label>
        <input value="" id="expireDate" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>สาเหตุการยกเลิกบัตร</label>
        <input value="" id="expireDate" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
  </div>
</div>
<div class="box-footer">
    <div class="col-md-12" style="text-align:center">
      <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
      <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
      <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
    </div>
</div>
