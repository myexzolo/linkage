  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $result         = "";
  $personalId     = "";
  $fullname       = "";
  $fullname_en    = "";
  $address        = "";
  $birthDate      = "";
  $blood          = "";
  $cancelCause    = "";
  $documentNumber = "";
  $expireDate     = "";
  $foreignCountry = "";
  $foreignCountryCity = "";
  $issueDate      = "";
  $religion       = "";
  $religionOther  = "";
  $sex            = "";

  $fullname       = str_repeat('&nbsp;', 40);

  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];

  if(isset($_POST['result']))
  {

    $result     = json_decode($_POST['result'], true);
    //print_r($result);

    if(isset($result['data']))
    {

      // address.alleyDesc
      // address.alleyWayDesc
      // address.districtDesc
      // address.houseNo
      // address.provinceDesc
      // address.roadDesc
      // address.subdistrictDesc
      // address.villageNo
      // birthDate
      // blood
      // cancelCause
      // documentNumber
      // expireDate
      // foreignCountry
      // foreignCountryCity
      // issueDate
      // issueTime
      // nameEN.firstName
      // nameEN.lastName
      // nameEN.middleName
      // nameEN.title
      // nameTH.firstName
      // nameTH.lastName
      // nameTH.middleName
      // nameTH.title
      // religion
      // religionOther
      // sex



      $personalId     = $result['PersonalID'];
      $fullname       = $result['data']['fullname'];
      $fullname_en    = $result['data']['fullname_en'];
      $address        = $result['data']['address'];
      $birthDate      = dateThLinkage($result['data']['birthDate']);
      $blood          = $result['data']['blood'];
      $cancelCause    = $result['data']['cancelCause'];
      $documentNumber = $result['data']['documentNumber'];
      $expireDate     = dateThLinkage($result['data']['expireDate']);
      $foreignCountry = $result['data']['foreignCountry'];
      $foreignCountryCity = $result['data']['foreignCountryCity'];
      $issueDate      = dateThLinkage($result['data']['issueDate']);
      $religion       = $result['data']['religion'];
      $religionOther  = $result['data']['religionOther'];
      $sex            = $result['data']['sex'];


      $page_id                = $result['page_id'];
      $type_search            = $result['type_search'];

      if($personalId != "")
      {
            $sql = "INSERT INTO t_history_search
            (user_id,pid,page_id,type_search,type_report)
            VALUES
            ('$user_id','$personalId','$page_id','$type_search','P')";
            DbQuery($sql,null);
      }
    }
  }

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลทะเบียนบ้าน</div>
      <br>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td>เลขที่ประจำตัวประชาชน</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $personalId ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล (ภาษาไทย)</td>
          <td> : </td>
          <td><?= $fullname ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล (ภาษาอังกฤษ)</td>
          <td> : </td>
          <td><?= $fullname_en ?></td>
        </tr>
        <tr>
          <td>ที่อยู่</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $address ?></td>
        </tr>
        <tr>
          <td>เมือง (กรณีอยู่ต่างประเทศ)</td>
          <td> : </td>
          <td><?= $foreignCountryCity ?></td>
        </tr>
        <tr>
          <td>ประเทศ (กรณีอยู่ต่างประเทศ)</td>
          <td> : </td>
          <td><?= $foreignCountry ?></td>
        </tr>
        <tr>
          <td>เพศ</td>
          <td> : </td>
          <td><?= $sex ?></td>
        </tr>
        <tr>
          <td>วันเดือนปี เกิด</td>
          <td> : </td>
          <td><?= $birthDate ?></td>
        </tr>
        <tr>
          <td>หมู่โลหิต</td>
          <td> : </td>
          <td><?= $blood ?></td>
        </tr>
        <tr>
          <td>ศาสนา</td>
          <td> : </td>
          <td><?= $religion ?></td>
        </tr>
        <tr>
          <td>ศาสนา (อื่นๆ)</td>
          <td> : </td>
          <td><?= $religionOther ?></td>
        </tr>
        <tr>
          <td>วันเดือนปี ที่ออกบัตร</td>
          <td> : </td>
          <td><?= $issueDate ?></td>
        </tr>
        <tr>
          <td>วันเดือนปี บัตรหมดอายุ</td>
          <td> : </td>
          <td><?= $expireDate ?></td>
        </tr>
        <tr>
          <td>สาเหตุการยกเลิกบัตร</td>
          <td> : </td>
          <td><?= $cancelCause ?></td>
        </tr>

      </table>
      <div style="height:100px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $fullname ?> )</td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลทะเบียนบ้าน');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
