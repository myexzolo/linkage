<div class="box-body">
  <div class="row">
    <div class="col-md-2">
      <div class="form-group">
        <label>เลขที่ประจำตัวประชาชน</label>
        <input value="" id="personalId" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>ชื่อ - สกุล</label>
        <input value="" id="fullnameAndRank" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>อายุ</label>
        <input value="" id="age" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เพศ</label>
        <input value="" id="genderDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สัญชาติ</label>
        <input value="" id="nationalityDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>สถานะภาพสมรส</label>
        <input value="" id="marriageStatusDesc" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-10">
      <div class="form-group">
        <label>ที่อยู่</label>
        <input value="" id="address" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>รหัสสำนักทะเบียนที่รับแจ้งตาย</label>
        <input value="" id="authorityIssuing" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขที่เอกสาร</label>
        <input value="" id="documentNo" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่แจ้งตาย (ปีพ.ศ. เดือน วัน)</label>
        <input value="" id="dateOfNotifying" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่ตาย (ปีพ.ศ. เดือน วัน)</label>
        <input value="" id="dateOfDeath" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>สาเหตุการตาย</label>
        <input value="" id="causeOfDeath" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-8">
      <div class="form-group">
        <label>สถานที่ตาย</label>
        <input value="" id="placeOfDeath" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขประจำตัวประชาชน บิดา</label>
        <input value="" id="fatherPersonalID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อบิดา</label>
        <input value="" id="fatherName" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขประจำตัวประชาชน มารดา</label>
        <input value="" id="motherPersonalID" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ชื่อมารดา</label>
        <input value="" id="motherName" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
  </div>
</div>
<div class="box-footer">
    <div class="col-md-12" style="text-align:center">
      <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
      <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
      <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
    </div>
</div>
