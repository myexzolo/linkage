var result = "";

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard("");
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });
})

showForm();
function showForm(){
  $.get("ajax/showform.php")
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function checkdataAddress(data, name){
  var val = "";
  if(data != ""){
     val = " " + name + data;
  }
  return val;
}

function subName(name){
  var val = "";
  if(name != "")
  {
      var res = name.split("#");
      var middleName = res[2];
      if(middleName != ""){
         middleName = " " + middleName + " ";
      }else{
        middleName = " ";
      }
      val = res[0] + res[1] + middleName + res[3];
  }
  return val;
}

function readSmartCard(type){
  var pid = "";
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();
  if(type == "R"){
    $("#pid").val("");
    $("#type_search").val("2");
    pid = "";
  }else{
    pid = $("#pid").val();
    $("#type_search").val("1");
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
  }
  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  //console.log(url);
  $.get(url)
  .done(function( data ) {
    //console.log(data);
    if(data.Status == "Y")
    {

      if(data.data != "" && data.data.info == null)
      {
        result = data;


        var titleProvince = "";
        var titleDistrict = "";
        var titleSubdistrict = "";

        var province = data.data.addressProvinceDesc;

        if(province.indexOf("กรุงเทพ") >= 0){
          titleProvince = "";
          titleDistrict = "เขต";
          titleSubdistrict = "แขวง";
        }else{
          titleProvince = "จังหวัด";
          titleDistrict = "อำเภอ";
          titleSubdistrict = "ตำบล";
        }

        var alleyDesc        = checkdataAddress(data.data.addressAlleyDesc,"ซอย");//ชื่อซอย
        var alleyWayDesc     = checkdataAddress(data.data.addressAlleyWayDesc,"ตรอก");//ชื่อตรอก
        var districtDesc     = checkdataAddress(data.data.addressDistrictDesc,titleDistrict);//ชื่ออำเภอ
        var houseNo          = checkdataAddress(data.data.addressHouseNo,"เลขที่ ");//บ้านเลขที่
        var provinceDesc     = checkdataAddress(data.data.addressProvinceDesc,titleProvince);//ชื่อจังหวัด
        var roadDesc         = checkdataAddress(data.data.addressRoadDesc,"ถนน");//ชื่อถนน
        var subdistrictDesc  = checkdataAddress(data.data.addressSubdistrictDesc,titleSubdistrict);//ชื่อตำบล
        var villageNo        = checkdataAddress(data.data.addressVillageNo,"หมู่ ");//หมู่ที่

        var address =  houseNo + villageNo + alleyWayDesc + alleyDesc + roadDesc + subdistrictDesc + districtDesc + provinceDesc;
        result.data.address  = address;

        var causeOfDeath = dateThLinkage(data.data.causeOfDeath);
        result.data.causeOfDeath = causeOfDeath;

        var dateOfDeath = dateThLinkage(data.data.dateOfDeath);
        result.data.dateOfDeath = dateOfDeath;

        var dateOfNotifying = dateThLinkage(data.data.dateOfNotifying);
        result.data.dateOfNotifying = dateOfNotifying;

        var dProvince = data.data.placeOfDeathProvinceDesc;

        if(dProvince.indexOf("กรุงเทพ") >= 0){
          titleProvince = "";
          titleDistrict = "เขต";
          titleSubdistrict = "แขวง";
        }else{
          titleProvince = "จังหวัด";
          titleDistrict = "อำเภอ";
          titleSubdistrict = "ตำบล";
        }

        var dAlleyDesc        = checkdataAddress(data.data.placeOfDeathAlleyDesc,"ซอย");//ชื่อซอย
        var dAlleyWayDesc     = checkdataAddress(data.data.placeOfDeathAlleyWayDesc,"ตรอก");//ชื่อตรอก
        var dDistrictDesc     = checkdataAddress(data.data.placeOfDeathDistrictDesc,titleDistrict);//ชื่ออำเภอ
        var dHouseNo          = checkdataAddress(data.data.placeOfDeathHouseNo,"เลขที่ ");//บ้านเลขที่
        var dProvinceDesc     = checkdataAddress(data.data.placeOfDeathProvinceDesc,titleProvince);//ชื่อจังหวัด
        var dRoadDesc         = checkdataAddress(data.data.placeOfDeathRoadDesc,"ถนน");//ชื่อถนน
        var dSubdistrictDesc  = checkdataAddress(data.data.placeOfDeathSubdistrictDesc,titleSubdistrict);//ชื่อตำบล
        var dVillageNo        = checkdataAddress(data.data.placeOfDeathVillageNo,"หมู่ ");//หมู่ที่

        var dAddress =  dHouseNo + dVillageNo + dAlleyWayDesc + dAlleyDesc + dRoadDesc + dSubdistrictDesc + dDistrictDesc + dProvinceDesc;
        result.data.placeOfDeath  = dAddress;

        result.data.personalID    = data.PersonalID;

        $("#personalId").val(data.PersonalID);

        $("#address").val(data.data.address);
        $("#age").val(data.data.age);
        $("#authorityIssuing").val(data.data.authorityIssuing);
        $("#causeOfDeath").val(data.data.causeOfDeath);
        $("#dateOfDeath").val(dateOfDeath);
        $("#dateOfNotifying").val(dateOfNotifying);
        $("#documentNo").val(data.data.documentNo);
        $("#fatherName").val(data.data.fatherName);
        $("#fatherPersonalID").val(data.data.fatherPersonalID);
        $("#fullnameAndRank").val(data.data.fullnameAndRank);
        $("#genderDesc").val(data.data.genderDesc);
        $("#marriageStatusDesc").val(data.data.marriageStatusDesc);
        $("#motherName").val(data.data.motherName);
        $("#motherPersonalID").val(data.data.motherPersonalID);
        $("#nationalityDesc").val(data.data.nationalityDesc);
        $("#placeOfDeath").val(dAddress);


        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();
        var pid          = $("#personalId").val();
        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});
      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      if(data.Message == ""){
        data.Message = "ไม่พบข้อมูล";
      }
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
      reset();
    }
  })
  .fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  });
}

function reset()
{
  $("#pid").val("");
  $("#personalId").val("");
  $("#address").val("");
  $("#age").val("");
  $("#authorityIssuing").val("");
  $("#causeOfDeath").val("");
  $("#dateOfDeath").val("");
  $("#dateOfNotifying").val("");
  $("#documentNo").val("");
  $("#fatherName").val("");
  $("#fatherPersonalID").val("");
  $("#fullnameAndRank").val("");
  $("#genderDesc").val("");
  $("#marriageStatusDesc").val("");
  $("#motherName").val("");
  $("#motherPersonalID").val("");
  $("#nationalityDesc").val("");
  $("#placeOfDeath").val("");

  $("#type_search").val("");

  result = "";
}

function printPdf()
{
  //console.log(result);
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}
