  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $result             = "";
  $personalId         = "";
  $address            = "";
  $age                = "";
  $authorityIssuing   = "";
  $causeOfDeath       = "";
  $dateOfDeath        = "";
  $dateOfNotifying    = "";
  $documentNo         = "";
  $fatherName         = "";
  $fatherPersonalID   = "";
  $fullnameAndRank    = "";
  $genderDesc         = "";
  $marriageStatusDesc = "";
  $motherName         = "";
  $motherPersonalID   = "";
  $nationalityDesc    = "";
  $placeOfDeath       = "";

  $fullname       = str_repeat('&nbsp;', 40);

  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];


  if(isset($_POST['result']))
  {

    $result     = json_decode($_POST['result'], true);
    //print_r($result);

    if(isset($result['data']))
    {
      $personalID             = $result['data']['personalID'];
      $address                = $result['data']['address'];
      $age                    = $result['data']['age'];
      $authorityIssuing       = $result['data']['authorityIssuing'];
      $causeOfDeath           = $result['data']['causeOfDeath'];
      $dateOfDeath            = $result['data']['dateOfDeath'];
      $dateOfNotifying        = $result['data']['dateOfNotifying'];
      $documentNo             = $result['data']['documentNo'];
      $fatherName             = $result['data']['fatherName'];
      $fatherPersonalID       = $result['data']['fatherPersonalID'];
      $fullnameAndRank        = $result['data']['fullnameAndRank'];
      $genderDesc             = $result['data']['genderDesc'];
      $marriageStatusDesc     = $result['data']['marriageStatusDesc'];
      $motherName             = $result['data']['motherName'];
      $motherPersonalID       = $result['data']['motherPersonalID'];
      $nationalityDesc        = $result['data']['nationalityDesc'];
      $placeOfDeath           = $result['data']['placeOfDeath'];

      $page_id                = $result['page_id'];
      $type_search            = $result['type_search'];

      if($personalId != "")
      {
            $sql = "INSERT INTO t_history_search
            (user_id,pid,page_id,type_search,type_report)
            VALUES
            ('$user_id','$personalId','$page_id','$type_search','P')";
            DbQuery($sql,null);
      }
    }
  }

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลใบมรณบัตร </div>
      <br>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td>เลขที่ประจำตัวประชาชน</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $personalID ?></td>
        </tr>
        <tr>
          <td>ชื่อ - สกุล</td>
          <td> : </td>
          <td><?= $fullnameAndRank ?></td>
        </tr>
        <tr>
          <td>อายุ</td>
          <td> : </td>
          <td><?= $age ?></td>
        </tr>
        <tr>
          <td>เพศ</td>
          <td> : </td>
          <td><?= $genderDesc ?></td>
        </tr>
        <tr>
          <td>สัญชาติ</td>
          <td> : </td>
          <td><?= $nationalityDesc ?></td>
        </tr>
        <tr>
          <td>สถานะภาพสมรส</td>
          <td> : </td>
          <td><?= $marriageStatusDesc ?></td>
        </tr>
        <tr>
          <td style="vertical-align:top;">ที่อยู่</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $address ?></td>
        </tr>
        <tr>
          <td>รหัสสำนักทะเบียนที่รับแจ้งตาย</td>
          <td> : </td>
          <td><?= $authorityIssuing ?></td>
        </tr>
        <tr>
          <td>เลขที่เอกสาร</td>
          <td> : </td>
          <td><?= $documentNo ?></td>
        </tr>
        <tr>
          <td>วันที่แจ้งตาย (ปีพ.ศ. เดือน วัน)</td>
          <td> : </td>
          <td><?= $dateOfNotifying ?></td>
        </tr>
        <tr>
          <td>วันที่ตาย (ปีพ.ศ. เดือน วัน)</td>
          <td> : </td>
          <td><?= $dateOfDeath ?></td>
        </tr>
        <tr>
          <td>สาเหตุการตาย</td>
          <td> : </td>
          <td><?= $causeOfDeath ?></td>
        </tr>
        <tr>
          <td style="vertical-align:top;">สถานที่ตาย</td>
          <td style="vertical-align:top;"> : </td>
          <td><?= $placeOfDeath ?></td>
        </tr>
        <tr>
          <td>เลขประจำตัวประชาชน บิดา</td>
          <td> : </td>
          <td><?= $fatherPersonalID ?></td>
        </tr>
        <tr>
          <td>ชื่อบิดา</td>
          <td> : </td>
          <td><?= $fatherName ?></td>
        </tr>
        <tr>
          <td>เลขประจำตัวประชาชน มารดา</td>
          <td> : </td>
          <td><?= $motherPersonalID ?></td>
        </tr>
        <tr>
          <td>ชื่อมารดา</td>
          <td> : </td>
          <td><?= $motherName ?></td>
        </tr>
      </table>
      <div style="height:100px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td></td>
          <td style="width:300px;" align="center">...............................................</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">( <?= $fullname ?> )</td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลใบมรณบัตร');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
