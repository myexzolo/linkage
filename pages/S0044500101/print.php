  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $data         =  $_POST['result'];

  $personalId   = isset($data['citizenid'])?$data['citizenid']:"";
  $dataRes      = isset($data['data'])?$data['data']:array();


  $nameUser    = $_SESSION['member'][0]['user_name'];

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'chatthai' => [
              'R' => 'CSChatThai.ttf',
              //'I' => 'THSarabunNew Italic.ttf',
              //'B' => 'THSarabunNew Bold.ttf',
          ]
      ],
      'default_font' => 'chatthai'
  ]);
  ob_start();
  // print_r($result);
  ?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลคลังปัญญาผู้สูงอายุ</div>
      <br>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
        <tr>
          <td>เลขที่ประจำตัวประชาชน</td>
          <td style="width:3px"> : </td>
          <td style="width:70%"><?= $personalId ?></td>
        </tr>
      </table>
      <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; overflow: wrap;font-size:14pt;" width = "100%">
        <thead>
          <tr class="text-center">
            <th class="thStyle" style="width:50px;">ลำดับ</th>
            <th class="thStyle" style="width:190px;">วันที่ขึ้นทะเบียนภูมิปัญญา</th>
            <th class="thStyle" >ประเภทภูมิปัญญา</th>
          </tr>
        </thead>
        <tbody>
            <?php
            if(count($dataRes) > 0)
            {
              for($i = 0; $i < count($dataRes); $i++)
              {

              ?>
              <tr class="text-center">
                <td class="content" align="center"><?=$i+1;?></td>
                <td class="content" align="center"><?= dateThLinkage2($dataRes[$i]['date_of_reg']);?></td>
                <td class="content" align="left"><?= $dataRes[$i]['wis_name'];?></td>
              </tr>
            <?php
                }
              }else{
            ?>
            <tr class="text-center">
              <td class="content" align="center">&nbsp;</td>
              <td class="content" align="center"></td>
              <td class="content" align="left"></td>
            </tr>
            <?php
              }
            ?>
          </tbody>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลคลังปัญญาผู้สูงอายุ');
    // $mpdf->StartProgressBarOutput(2);
    $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
    $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
