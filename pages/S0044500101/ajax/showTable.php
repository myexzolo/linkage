<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$personalId = "";
$dataRes    = array();

//print_r($_POST);

if($_POST['result'] != "")
{
  $result = json_decode($_POST['result'], true);
  $data         = $result['data'];

  $personalId   = $data['citizenid'];
  $dataRes      = $data['data'];
}

?>

<div class="box-body">
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        <label>เลขที่ประจำตัวประชาชน</label>
        <input value="" id="personalId" type="text" class="form-control" data-smk-msg="&nbsp;" readonly>
      </div>
    </div>
    <div class="col-md-12">
      <table class="table table-bordered table-striped" id="tableDisplay">
        <thead>
          <tr class="text-center">
            <th style="width:50px;">ลำดับ</th>
            <th style="width:190px;">วันที่ขึ้นทะเบียนภูมิปัญญา</th>
            <th>ประเภทภูมิปัญญา</th>
          </tr>
        </thead>
        <tbody>
          <?php
          for($i = 0; $i < count($dataRes); $i++)
          {
          ?>
          <tr class="text-center">
            <td><?=$i+1;?></td>
            <td><?= dateThLinkage2($dataRes[$i]['date_of_reg']);?></td>
            <td><?= $dataRes[$i]['wis_name'];?></td>
          </tr>
          <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
   <div class="box-footer">  
    <div class="col-md-12" style="text-align:center">
      <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
      <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
      <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
    </div>
  </div>

  <script>
    $(function () {
      $('#tableDisplay').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : true,
        'autoWidth'   : false
      })
    })
  </script>
