<!DOCTYPE html>
<?php
include("../../inc/function/connect.php");
include("../../inc/function/mainFunc.php");
require_once '../../mpdf2/vendor/autoload.php';

//include("../../THSplitLib/segment.php");

//$projectCode = '61100004_1539515202';
$nameUser    = $_SESSION['member'][0]['user_name'];
$user_id     = $_SESSION['member'][0]['user_id'];
$fullname    = "";

// echo $imagePath;

$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'fontDir' => array_merge($fontDirs, [
        '../../font/CSChatThai',
    ]),
    'fontdata' => $fontData + [
        'chatthai' => [
            'R' => 'CSChatThai.ttf',
            //'I' => 'THSarabunNew Italic.ttf',
            //'B' => 'THSarabunNew Bold.ttf',
        ]
    ],
    'default_font' => 'chatthai'
]);
ob_start();
// print_r($result);
?>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
  <link rel="stylesheet" href="css/pdf.css">
</head>
<body>
    <div align="center" style="font-size:18pt;font-weight:bold">ข้อมูลประวัติการจดทะเบียนสมรส</div>
    <br>
        <?php
        if(isset($_POST['result']) && $_POST['result'] != "")
        {
          $result = json_decode($_POST['result'], true);
          $data      = $result['data'];
          $personalID = $result['PersonalID'];
          $allMarry  = $data['allMarry'];
          for($i = 0; $i < count($allMarry); $i++)
          {
            $malePID   = $allMarry[$i]['malePID'];
            $femalePID = $allMarry[$i]['femalePID'];
            $maleFullnameAndRank   = $allMarry[$i]['maleFullnameAndRank'];
            $femaleFullnameAndRank = $allMarry[$i]['femaleFullnameAndRank'];
          ?>
          <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid 1px #000000; overflow: wrap;" width = "100%">
            <thead>
              <tr>
                 <th class="thStyle" colspan="3">
                   <table style="border-collapse:collapse; border:0px; width:100%;">
                     <tbody>
                       <tr>
                         <td class="content2" style="width:20%" align="left"><b>เลขจดทะเบียนสมรส</b></td>
                         <td class="content2" style="width:30%" align="left"><?= $allMarry[$i]['marryID'] ?></td>
                         <td class="content2" style="width:20%" align="left"><b>วันที่จดทะเบียน</b></td>
                         <td class="content2" style="width:30%" align="left"><?= dateThLinkage($allMarry[$i]['marryDate']) ?></td>
                       </tr>
                       <tr>
                         <td class="content2" align="left"><b>สนท.ที่จดทะเบียน</b></td>
                         <td class="content2" align="left"><?= $allMarry[$i]['marryPlaceDesc'] ?></td>
                         <td class="content2" align="left"><b>จังหวัดที่จดทะเบียน</b></td>
                         <td class="content2" align="left"><?= $allMarry[$i]['marryPlaceProvince'] ?></td>
                       </tr>
                     </tbody>
                    </table>
                </th>
              </tr>
              <tr class="text-center">
                <th class="thStyle"></th>
                <th class="thStyle"  style="width:40%;"><b>ชาย</b></th>
                <th class="thStyle"  style="width:40%;"><b>หญิง</b></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="content"><b>เลขประจำตัวประชาชน</b></td>
                <td class="content"><?= $malePID; ?></td>
                <td class="content"><?= $femalePID; ?></td>
              </tr>
              <tr>
                <td class="content"><b>ชื่อ - สกุล</b></td>
                <td class="content"><?= $maleFullnameAndRank; ?></td>
                <td class="content"><?= $femaleFullnameAndRank; ?></td>
             </tr>
             <tr>
                <td class="content"><b>อายุ</b></td>
                <td class="content"><?= $allMarry[$i]['maleAge'] ?></td>
                <td class="content"><?= $allMarry[$i]['marryPlaceProvince'] ?></td>
            </tr>
            <tr>
               <td class="content"><b>วัน เดือน ปีเกิด</b></td>
               <td class="content"><?= dateThLinkage($allMarry[$i]['maleDateOfBirth']) ?></td>
               <td class="content"><?= dateThLinkage($allMarry[$i]['femaleDateOfBirth']) ?></td>
            </tr>
            <tr>
               <td class="content"><b>สัญชาติ</b></td>
               <td class="content"><?= $allMarry[$i]['maleNationalityDesc'] ?></td>
               <td class="content"><?= $allMarry[$i]['femaleNationalityDesc'] ?></td>
            </tr>
          </tbody>
          </table>
          <?php

            if($i > 1){
              echo "<br>";
            }else{
              if($personalID == $femalePID){
                $fullname = $femaleFullnameAndRank;
              }else if($personalID == $malePID){
                $fullname = $maleFullnameAndRank;
              }
            }
          }
        }
        ?>
    <div style="height:100px;"></div>
    <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:16pt;" >
      <tr>
        <td></td>
        <td style="width:300px;" align="center">...............................................</td>
      </tr>
      <tr>
        <td></td>
        <td align="center">( <?= $fullname ?> )</td>
      </tr>
    </table>
</body>
</html>
<?php
$footer = '<div class="foot">
            <div id="leyend_foot" class="center">
              <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
              </div>
              </div>';
  //echo $html;
  $html = ob_get_contents();
  ob_end_clean();
  $stylesheet = file_get_contents('css/pdf.css');
  $mpdf->SetTitle('ข้อมูลประวัติการจดทะเบียนสมรส');
  // $mpdf->StartProgressBarOutput(2);
  $mpdf->AddPage('P','','','','',10,10,10,10,10,10);
  $mpdf->SetHTMLFooter($footer);
  // $mpdf->showWatermarkText = true;
  $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
  $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

  $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
?>
