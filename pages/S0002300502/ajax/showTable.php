<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<div class="box-body">
<?php
//echo $_POST['result'];
if(isset($_POST['result']) && $_POST['result'] != "")
{
  $result = json_decode($_POST['result'], true);
  $data      = $result['data'];
  $allMarry  = $data['allMarry'];
  for($i = 0; $i < count($allMarry); $i++)
  {
  ?>
  <table class="table table-bordered table-striped" style="width:100%">
    <thead>
      <tr>
         <th colspan="3">
           <div class="row">
           <div class="col-md-3" align="left"><b>เลขจดทะเบียนสมรส</b><span style="font-weight: normal;">&nbsp;<?= $allMarry[$i]['marryID'] ?></span></div>
           <div class="col-md-3" align="left"><b>วันที่จดทะเบียน</b><span style="font-weight: normal;">&nbsp;<?= dateThLinkage($allMarry[$i]['marryDate']) ?></span></div>
           <div class="col-md-3" align="left"><b>สนท.ที่จดทะเบียน</b><span style="font-weight: normal;">&nbsp;<?= $allMarry[$i]['marryPlaceDesc'] ?></span></div>
           <div class="col-md-3" align="left"><b>จังหวัดที่จดทะเบียน</b><span style="font-weight: normal;">&nbsp;<?= $allMarry[$i]['marryPlaceProvince'] ?></span></div>
          </div>
        </th>
      </tr>
      <tr class="text-center">
        <th></th>
        <th style="width:40%;">ชาย</th>
        <th style="width:40%;">หญิง</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><b>เลขประจำตัวประชาชน</b></td>
        <td><?= $allMarry[$i]['malePID'] ?></td>
        <td><?= $allMarry[$i]['femalePID'] ?></td>
      </tr>
      <tr>
        <td><b>ชื่อ - สกุล</b></td>
        <td><?= $allMarry[$i]['maleFullnameAndRank'] ?></td>
        <td><?= $allMarry[$i]['femaleFullnameAndRank'] ?></td>
     </tr>
     <tr>
        <td><b>อายุ</b></td>
        <td><?= $allMarry[$i]['maleAge'] ?></td>
        <td><?= $allMarry[$i]['marryPlaceProvince'] ?></td>
    </tr>
    <tr>
       <td><b>วัน เดือน ปีเกิด</b></td>
       <td><?= dateThLinkage($allMarry[$i]['maleDateOfBirth']) ?></td>
       <td><?= dateThLinkage($allMarry[$i]['femaleDateOfBirth']) ?></td>
    </tr>
    <tr>
       <td><b>สัญชาติ</b></td>
       <td><?= $allMarry[$i]['maleNationalityDesc'] ?></td>
       <td><?= $allMarry[$i]['femaleNationalityDesc'] ?></td>
    </tr>
  </tbody>
  </table>
  <?php
    if($i > 1){
      echo "<br>";
    }
  }
}else{
?>
<table class="table table-bordered table-striped">
  <thead>
    <tr>
       <th colspan="3">
         <div class="row">
         <div class="col-md-3" align="left"><b>เลขจดทะเบียนสมรส</b></div>
         <div class="col-md-3" align="left"><b>วันที่จดทะเบียน</b></div>
         <div class="col-md-3" align="left"><b>สนท.ที่จดทะเบียน</b></div>
         <div class="col-md-3" align="left"><b>จังหวัดที่จดทะเบียน</b></div>
        </div>
      </th>
    </tr>
    <tr class="text-center">
      <th></th>
      <th style="width:40%;">ชาย</th>
      <th style="width:40%;">หญิง</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><b>เลขประจำตัวประชาชน</b></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td><b>ชื่อ - สกุล</b></td>
      <td></td>
      <td></td>
   </tr>
   <tr>
      <td><b>อายุ</b></td>
      <td></td>
      <td></td>
  </tr>
  <tr>
     <td><b>วัน เดือน ปีเกิด</b></td>
     <td></td>
     <td></td>
  </tr>
  <tr>
     <td><b>สัญชาติ</b></td>
     <td></td>
     <td></td>
  </tr>
</tbody>
</table>
<?php
}
?>
</div>
<div class="box-footer">
  <div class="col-md-12" style="text-align:center">
    <button type="button" class="btn btn-success btn-flat" onclick="home()" style="width:160px;font-size:20px;height:40px;">กลับ</button>&nbsp;
    <button type="button" class="btn btn-flat" onclick="reset()" style="width:160px;font-size:20px;height:40px;">ยกเลิก</button>&nbsp;
    <button type="button" class="btn btn-primary btn-flat" onclick="printPdf()" style="width:160px;font-size:20px;height:40px;">พิมพ์</button>
  </div>
</div>
<script>
  $(function () {
    $('.table').DataTable({
      'paging'      : false,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    })
  })
</script>
