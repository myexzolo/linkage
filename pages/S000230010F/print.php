  <!DOCTYPE html>
<?php
  include("../../inc/function/connect.php");
  include("../../inc/function/mainFunc.php");
  require_once '../../mpdf2/vendor/autoload.php';

  //include("../../THSplitLib/segment.php");

  //$projectCode = '61100004_1539515202';

  $result         = "";
  $middleName     = "";
  $middleName_en  = "";
  $personalId     = "";
  $fullname       = "";
  $fullname_en    = "";
  $dateOfBirth    = "";
  $dateOfMoveIn   = "";
  $age            = "";
  $genderDesc     = "";
  $nationalityDesc      = "";
  $statusOfPersonDesc   = "";
  $ownerStatusDesc      = "";
  $fatherPersonalID     = "";
  $fatherName           = "";
  $fatherNationalityDesc = "";
  $motherPersonalID       = "";
  $motherName     ="";
  $motherNationalityDesc = "";


  $houseID        = "";
  $rcodeDesc      = "";
  $houseTypeDesc  = "";
  $address1        = "";
  $address2        = "";


  $nameUser    = $_SESSION['member'][0]['user_name'];
  $user_id     = $_SESSION['member'][0]['user_id'];

  $monthNow = monthThaiFull(date("m"));
  $dayNow   = date("d");
  $yearNow   = date("Y");

  if($yearNow < 2500)
  {
    $yearNow += 543;
  }

  // $dayUpdate =  "01";
  // $monthUpdate = monthThaiFull("11");
  // $yearUpdate = "2563";


  $dayUpdate    =  $dayNow;
  $monthUpdate  = $monthNow;
  $yearUpdate   = $yearNow;




  if(isset($_POST['result']))
  {

    $result     = json_decode($_POST['result'], true);
    //print_r($result);

    if(isset($result['data']))
    {
      $middleName = $result['data']['middleName'];
      if($middleName != ""){
         $middleName = " " . $middleName . " ";
      }else{
        $middleName = " ";
      }
      $middleName_en = $result['data']['englishMiddleName'];
      if($middleName_en != ""){
         $middleName_en = " " + $middleName_en + " ";
      }else{
        $middleName_en = " ";
      }


      $personalId     = $result['PersonalID'];
      $fullname       = $result['data']['titleDesc']."".$result['data']['firstName'].$middleName.$result['data']['lastName'];
      $fullname_en    = $result['data']['englishTitleDesc']." ".$result['data']['englishFirstName'].$middleName_en.$result['data']['englishLastName'];
      $dateOfBirth    = dateFulllThLinkage($result['data']['dateOfBirth']);
      $dateOfMoveIn   = dateFulllThLinkage($result['data']['dateOfMoveIn']);
      $age            = $result['data']['age'];
      $genderDesc     = $result['data']['genderDesc'];
      $nationalityDesc      = $result['data']['nationalityDesc'];
      $statusOfPersonDesc   = $result['data']['statusOfPersonDesc'];
      $ownerStatusDesc      = $result['data']['ownerStatusDesc'];
      $fatherPersonalID     = $result['data']['fatherPersonalID'];
      $fatherName           = $result['data']['fatherName'];
      $fatherNationalityDesc  = $result['data']['fatherNationalityDesc'];
      $motherPersonalID       = $result['data']['motherPersonalID'];
      $motherName             = $result['data']['motherName'];
      $motherNationalityDesc  = $result['data']['motherNationalityDesc'];


      $houseID          = $result['data']['houseID'];
      $rcodeDesc        = $result['data']['rcodeDesc'];
      $houseTypeDesc    = $result['data']['houseTypeDesc'];
      $address1         = $result['data']['address1'];
      $address2         = $result['data']['address2'];

      $page_id                = $result['page_id'];
      $type_search            = $result['type_search'];


      $arrDateOfMoveIn = explode(" ", $dateOfMoveIn);

      if($personalId != "")
      {
            $sql = "INSERT INTO t_history_search
            (user_id,pid,page_id,type_search,type_report)
            VALUES
            ('$user_id','$personalId','$page_id','$type_search','P')";
            DbQuery($sql,null);
      }
    }
  }

  // echo $imagePath;

  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'fontDir' => array_merge($fontDirs, [
          '../../font/THSarabun',
      ]),
      'fontdata' => $fontData + [
          'thsarabun' => [
              'R' => 'THSarabun.ttf',
              'I' => 'THSarabun Italic.ttf',
              'B' => 'THSarabun Bold.ttf',
          ]
      ],
      'default_font' => 'thsarabun'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
      <div align="center" style="font-size:18pt;font-weight:bold"><img src="../../image/tk.jpg" style="width:20mm"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;" >
        <tr>
          <td style="width:20%"></td>
          <td></td>
          <td style="width:20%" align="right" style="vertical-align: bottom;font-size:14pt;height:30px;"><b>ท.ร. 14/1</b></td>
        </tr>
        <tr>
          <td></td>
          <td align="center" ><b><span style="font-size:16pt;">แบบรับรองรายการทะเบียนราษฎร  จากฐานข้อมูลการทะเบียน</span></b>
          </td>
          <td></td>
        </tr>
        <tr>
          <td ></td>
          <td align="center"><b><span style="font-size:14pt;">สำนักทะเบียนกลาง  กรมการปกครอง  กระทรวงมหาดไทย</span></b></td>
          <td></td>
        </tr>
        <tr>
          <td ></td>
          <td align="center"><b><span style="font-size:14pt;">( บริษัท ขนส่ง จำกัด )</span></b></td>
          <td></td>
        </tr>
        <tr>
          <td style="border-bottom: 1px solid #000000;height:10px;"></td>
          <td style="border-bottom: 1px solid #000000;"></td>
          <td style="border-bottom: 1px solid #000000;"></td>
        </tr>
      </table>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:14pt;" >
        <tr>
          <td colspan="6" style="height:15px;"></td>
        </tr>
        <tr>
          <td style="height:35px;" colspan="4">เลขที่ประจำตัวประชาชน&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $personalId ?></b></td>
          <td colspan="2">เลขรหัสประจำบ้าน&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $houseID ?></b></td>
        </tr>
        <tr>
          <td style="height:35px;" colspan="4">ชื่อ&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $fullname ?></b></td>
          <td style="width:150px">เพศ&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $genderDesc ?></b></td>
          <td >สัญชาติ&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $nationalityDesc ?></b></td>
        </tr>
        <tr>
          <td style="width:240px;height:35px;">เกิดเมื่อ&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $dateOfBirth ?></b></td>
          <td style="width:40px" align="center">อายุ</td>
          <td style="width:50px" align="center"><b><?= $age ?></b></td>
          <td style="width:50px" align="center">ปี</td>
          <td colspan="2">สถานภาพ&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $ownerStatusDesc ?></b></td>
        </tr>
        <tr>
          <td colspan="6" style="height:15px;"></td>
        </tr>
        <tr>
          <td style="height:35px;" colspan="4">มารดาชื่อ&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $motherName ?></b></td>
          <td colspan="2">สัญชาติ&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $motherNationalityDesc ?></b></td>
        </tr>
        <tr>
          <td style="height:35px;" colspan="4">บิดาชื่อ&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $fatherName ?></b></td>
          <td colspan="2">สัญชาติ&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $fatherNationalityDesc ?></b></td>
        </tr>
        <tr>
          <td style="height:35px;" colspan="6">ที่อยู่&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $address1 ?></b></td>
        </tr>
        <tr>
          <td style="height:35px;" colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $address2 ?></b></td>
        </tr>
        </table>
        <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:14pt;" >
          <tr>
            <td colspan="3" style="height:15px;"></td>
          </tr>
          <tr>
            <td style="height:35px;" colspan="3">สำนักทะเบียน&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $rcodeDesc ?></b></td>
          </tr>
          <tr>
            <td style="height:35px;width:180px;" >เข้ามาอยู่เมื่อวันที่&nbsp;&nbsp;&nbsp;&nbsp;<b><?= @$arrDateOfMoveIn[0] ?></b></td>
            <td style="width:170px;" >เดือน&nbsp;&nbsp;&nbsp;&nbsp;<b><?= @$arrDateOfMoveIn[1] ?></b></td>
            <td>พ.ศ.&nbsp;&nbsp;&nbsp;&nbsp;<b><?= @$arrDateOfMoveIn[2] ?></b></td>
          </tr>
          <tr>
            <td style="height:35px;" colspan="3">บันทึกเพิ่มเติม</td>
          </tr>
        </table>
        <div style="height:120px"></div>
        <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:14pt;" >
          <tr>
            <td style="border-bottom: 1px solid #000000;border-top: 1px solid #000000;height:50px;vertical-align:middle;width:350px">
              ปรับปรุงข้อมูลครั้งสุดท้าย</td>
            <td style="border-bottom: 1px solid #000000;border-top: 1px solid #000000;width:80px">
              วันที่&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $dayUpdate ?></b></td>
            <td style="border-bottom: 1px solid #000000;border-top: 1px solid #000000;">
              เดือน&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $monthUpdate ?></b></td>
            <td style="border-bottom: 1px solid #000000;border-top: 1px solid #000000;">
              พ.ศ.&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $yearUpdate ?></b></td>
          </tr>
        </table>
      <div style="height:40px;"></div>
      <table border="0" cellspacing="0" style="border-collapse:collapse; border:solid #333 0px; width:100% ;font-size:14pt;" >

        <tr>
          <td style="width:30%"></td>
          <td style="width:70px;">วันที่&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $dayNow ?></b></td>
          <td style="width:140px;">เดือน&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $monthNow ?></b></td>
          <td>พ.ศ.&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $yearNow ?></b> </td>
          <td ></td>
        </tr>
        <tr>
          <td colspan="5" align="center" style="height:140px;vertical-align:bottom;"><b>นายทะเบียน</b></td>
        </tr>
      </table>
  </body>
</html>
<?php
  $footer = '<div class="foot">
              <div id="leyend_foot" class="center">
                <p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ผู้ทำรายการ '.$nameUser.'</p>
                </div>
                </div>';
    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('แบบรับรองรายการทะเบียนราษฎร ท.ร. 14/1');
    // $mpdf->StartProgressBarOutput(2);
    // $mpdf->AddPage('P','','','','',left,right,top,10,10,10);
    $mpdf->AddPage('P','','','','',10,10,12,10,10,10);
    // $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('result.pdf', \Mpdf\Output\Destination::INLINE);
  ?>
