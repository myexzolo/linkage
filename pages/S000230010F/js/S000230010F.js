var result = "";
var record = 1;
var totalRecord = 0;

$(function () {
  $("#pid").bind("enterKey",function(e){
    readSmartCard();
  });
  $("#pid").keyup(function(e){
      if(e.keyCode == 13)
      {
        $(this).trigger("enterKey");
      }
  });

  $( "#type_search" ).change(function() {
    setTypeSearch();
  });
  setTypeSearch();
  showForm();
})


function calRec(str)
{
  if(str == "M")
  {
      record++;

  }else if(str == "T" && record > 1)
  {
      record--;
  }

  if(totalRecord > 1 & record != totalRecord)
  {
    //console.log("totalRecord > 1 & record != totalRecord");
    $("#btnRight").prop('disabled', false);
  }

  if(record == 1)
  {
    //console.log("record == 1");
    $("#btnleft").prop('disabled', true);
  }

  if(record > 1)
  {
    //console.log("record > 1");
    $("#btnleft").prop('disabled', false);
  }

  if(record == totalRecord)
  {
    //console.log("record == totalRecord");
    $("#btnRight").prop('disabled', true);
  }
  console.log(record + " " + totalRecord);
  readSmartCard("");
}

function readData(type)
{
  record = 1;
  totalRecord = 0;
  readSmartCard();
}

function setTypeSearch(){
  var type_search = $("#type_search").val();
  //console.log(type_search);
  if(type_search == "1")
  {
    $("#div_pid").show();
    $("#div_firstName").hide();
    $("#div_lastName").hide();
    $("#firstName").val("");
    $("#lastName").val("");
  }else if(type_search == "3"){
    $("#div_pid").hide();
    $("#div_firstName").show();
    $("#div_lastName").show();
    $("#pid").val("");
  }else if(type_search == "2"){
    $("#div_pid").hide();
    $("#div_firstName").hide();
    $("#div_lastName").hide();
    $("#firstName").val("");
    $("#lastName").val("");
    $("#pid").val("");
  }
}

function showForm(){
  $.get("ajax/showform.php")
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

async function readSmartCard(){
  var pid = "";
  var type_search  = $("#type_search").val();
  var officeCode   = $("#officeCode").val();
  var serviceCode  = $("#serviceCode").val();
  var versionCode  = $("#versionCode").val();

  var url = "";

  //console.log(">>>>" + type_search);
  if (type_search == "1") {
    pid = $("#pid").val();
    if(pid == "")
    {
        $.smkAlert({text: "ไม่พบเลขที่ประจำตัวประชาชน",type: "warning"});
        return;
    }else if(pid.length < 13){
        $.smkAlert({text: "เลขที่ประจำตัวประชาชนไม่ครบ 13 หลัก",type: "warning"});
        return;
    }
    url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;

  }
  if (type_search == "2")
  {
    pid = "";
    $("#pid").val("");
    url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  }
  else if (type_search == "3")
  {
    pid = "";
    $("#pid").val("");
    if($("#firstName").val() == "")
    {
        $.smkAlert({text: "กรุณาระบุชื่อ",type: "warning"});
        return;
    }else if($("#lastName").val() == ""){
        $.smkAlert({text: "กรุณาระบุนามสกุล",type: "warning"});
        return;
    }else{
      var firstName  = Base64.encode($("#firstName").val());
      var lastName   = Base64.encode($("#lastName").val());
      officeCode   = "00023";
      serviceCode  = "042";
      versionCode  = "01";

      var input = "&limit=1&firstName="+firstName+"&lastName="+lastName+"&recordNumber="+record;
      url = "http://localhost:20001/Readcard/?officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode+input;
      // console.log(url);
    }
  }
  //console.log(url);
  await $.get(url)
  .done(function( data ) {
    //console.log(data);
    //$.post("ajax/logData.php",{page_id:page_id,data:JSON.stringify(data),pid:pid,url:url});
    if(data.Status == "Y")
    {

      if(data.data != "" && data.data.info == null)
      {
        result = data;
        var middleName = data.data.middleName;
        if(middleName != ""){
           middleName = " " + middleName + " ";
        }else{
          middleName = " ";
        }
        var middleName_en = data.data.englishMiddleName;
        if(middleName_en != ""){
           middleName_en = " " + middleName_en + " ";
        }else{
          middleName_en = " ";
        }

        if (data.data.pid)
        {
          $("#personalId").val(data.data.pid);
          if(data.data.totalRecord)
          {
            totalRecord = data.data.totalRecord;
          }
        }else
        {
          $("#personalId").val(data.PersonalID);
        }

        if(totalRecord <= 1)
        {
          $("#btnleft").prop('disabled', true);
          $("#btnRight").prop('disabled', true);
        }else if(totalRecord > record)
        {
          $("#btnRight").prop('disabled', false);
        }

        var pid   = $("#personalId").val();

        //console.log(pid);


        $("#fullname").val(data.data.titleDesc +" "+ data.data.firstName + middleName + data.data.lastName);
        $("#fullname_en").val(data.data.englishTitleDesc +" "+ data.data.englishFirstName + middleName_en + data.data.englishLastName);

        $("#dateOfBirth").val(dateThLinkage(data.data.dateOfBirth));
        $("#dateOfMoveIn").val(dateThLinkage(data.data.dateOfMoveIn));
        $("#age").val(data.data.age);
        $("#genderDesc").val(data.data.genderDesc);
        $("#nationalityDesc").val(data.data.nationalityDesc);
        $("#statusOfPersonDesc").val(data.data.statusOfPersonDesc);
        $("#ownerStatusDesc").val(data.data.ownerStatusDesc);
        $("#fatherPersonalID").val(data.data.fatherPersonalID);
        $("#fatherName").val(data.data.fatherName);
        $("#fatherNationalityDesc").val(data.data.fatherNationalityDesc);
        $("#motherPersonalID").val(data.data.motherPersonalID);
        $("#motherName").val(data.data.motherName);
        $("#motherNationalityDesc").val(data.data.motherNationalityDesc);

        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();



        setAddress(pid);

        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});


      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      if(data.Message == ""){
        data.Message = "ไม่พบข้อมูล";
      }
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
      reset();
    }

  })
  .fail(function (jqXHR, textStatus)
  {
      console.log(jqXHR);
      $.smkAlert({text: "ไม่พบโปรแกรม Agent",type: "danger"});
  });
}


async function setAddress(pid){
  var officeCode   = "00023";
  var serviceCode  = "027";
  var versionCode  = "01";

  var url = "http://localhost:20001/Readcard/?pid="+pid+"&officeCode="+officeCode+"&versionCode="+versionCode+"&serviceCode="+serviceCode;
  //console.log(url);
  await $.get(url)
  .done(function( data ) {
    //console.log(data);
    if(data.Status == "Y")
    {
      if(data.data != "" && data.data.info == null)
      {
        //result = data;

        var titleProvince = "";
        var titleDistrict = "";
        var titleSubdistrict = "";

        var province = data.data.provinceDesc;

        if(province.indexOf("กรุงเทพ") >= 0){
          titleProvince = "";
          titleDistrict = "";
          titleSubdistrict = "แขวง";
        }else{
          titleProvince = "จังหวัด";
          titleDistrict = "อำเภอ";
          titleSubdistrict = "ตำบล";
        }

        var alleyDesc        = checkdataAddress(data.data.alleyDesc,"ซอย");//ชื่อซอย
        var alleyWayDesc     = checkdataAddress(data.data.alleyWayDesc,"ตรอก");//ชื่อตรอก
        var districtDesc     = checkdataAddress(data.data.districtDesc,titleDistrict);//ชื่ออำเภอ
        var houseNo          = checkdataAddress(data.data.houseNo,"เลขที่ ");//บ้านเลขที่
        var provinceDesc     = checkdataAddress(data.data.provinceDesc,titleProvince);//ชื่อจังหวัด
        var roadDesc         = checkdataAddress(data.data.roadDesc,"ถนน");//ชื่อถนน
        var subdistrictDesc  = checkdataAddress(data.data.subdistrictDesc,titleSubdistrict);//ชื่อตำบล
        var villageNo        = checkdataAddress(data.data.villageNo,"หมู่ ");//หมู่ที่

        var address =  houseNo + villageNo + alleyWayDesc + alleyDesc + roadDesc + subdistrictDesc + districtDesc + provinceDesc;
        result.data.address1  = houseNo + villageNo + alleyWayDesc + alleyDesc + roadDesc;
        result.data.address2  = subdistrictDesc + districtDesc + provinceDesc;
        result.data.rcodeDesc = data.data.rcodeDesc;
        result.data.houseID   = data.data.houseID;
        result.data.houseTypeDesc  = data.data.houseTypeDesc;

        $houseID        = "";
        $rcodeDesc      = "";
        $houseTypeDesc  = "";
        $address1        = "";
        $address2        = "";

        $("#houseID").val(data.data.houseID);
        $("#rcodeDesc").val(data.data.rcodeDesc);
        $("#houseTypeDesc").val(data.data.houseTypeDesc);
        $("#address").val(address);

        var page_id      = $("#pageId").val();
        var type_search  = $("#type_search").val();
        var pid          = result.PersonalID;
        //console.log(pid);
        $.post("ajax/saveHistoryReport.php",{page_id:page_id,type_search:type_search,pid:pid});

      }else if(data.data.info != null && data.data.info != ""){
        $.smkAlert({text: data.data.info,type: "warning"});
        reset();
      }else{
        $.smkAlert({text: data.Message,type: "warning"});
        reset();
      }
    }else{
      $.smkAlert({text: data.Message,type: "danger"});
      result = "";
    }
  });
}

function reset()
{
  $("#pid").val("");
  $("#personalId").val("");
  $("#fullname").val("");
  $("#fullname_en").val("");

  $("#dateOfBirth").val("");
  $("#dateOfMoveIn").val("");
  $("#age").val("");
  $("#genderDesc").val("");
  $("#nationalityDesc").val("");
  $("#statusOfPersonDesc").val("");
  $("#ownerStatusDesc").val("");
  $("#fatherPersonalID").val("");
  $("#fatherName").val("");
  $("#fatherNationalityDesc").val("");
  $("#motherPersonalID").val("");
  $("#motherName").val("");
  $("#motherNationalityDesc").val("");

  $("#houseID").val("");
  $("#rcodeDesc").val("");
  $("#houseTypeDesc").val("");
  $("#address").val("");

  //$("#type_search").val("");

  result = "";
}


function checkdataAddress(data, name){
  var val = "";
  if(data != null && data != ""){
     val = " " + name + data;
  }
  return val;
}

function printPdf()
{
  //console.log(result);
  if(result != "")
  {
    var page_id      = $("#pageId").val();
    var type_search  = $("#type_search").val();

    result.page_id      = page_id;
    result.type_search  = type_search;

    result = JSON.stringify(result);

    var pram = "?result="+ result;
    var url = 'print.php'+ pram;
    postURL_blank(url);
  }else{
    $.smkAlert({text: "ไม่พบข้อมูล",type: "warning"});
  }

}
