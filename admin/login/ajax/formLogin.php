<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$getOTP = getOTP();
$disabled = '';
$type = 'text';
$icons = 'glyphicon glyphicon-user';
$placeholder1 = 'Username';
$placeholder2 = 'Password';
$fun = "showForm('OTP')";
//$strongPass = 'strong';
$strongPass = 'weak';
if($action=='OTP'){
  $fun = "";
  $disabled = 'disabled';
  $type = "email";
  $icons = 'glyphicon glyphicon-envelope';
  $placeholder1 = 'Email';
  $placeholder2 = "OTP REF:";
  $strongPass = '';
}

?>

<div class="row mar-btn-20" id="action">
  <!--<div class="col-md-6">
    <a class="btn btn-app btn-block mar-zero" onclick="<?=$fun?>">
      <i class="fa fa-key"></i> OTP
    </a>
  </div>
  <div class="col-md-6">
    <a class="btn btn-app btn-block mar-zero" onclick="showForm('LOGIN')">
      <i class="fa fa-unlock-alt"></i> USER PASS
    </a>
  </div>-->
</div>

<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" id="tl_session" name="tl_session" value="">
<div class="form-group has-feedback">
  <input type="<?=$type?>" id="user" name="user" autocomplete="new-password" data-smk-msg="กรุณากรอกชื่อผู้ใช้งาน"  class="form-control" placeholder="<?=$placeholder1?>" required style="font-size: 22px">
  <span class="<?=$icons?> form-control-feedback icons" style="font-size: 18px"></span>
</div>
<div class="form-group has-feedback">
  <input type="password" id="pass" name="pass" autocomplete="new-password" data-smk-msg="กรุณากรอกรหัสผ่าน" class="form-control" placeholder="<?=$placeholder2?>" <?=$disabled?> required style="font-size: 22px">
  <span class="glyphicon glyphicon-lock form-control-feedback icons" style="font-size: 18px"></span>
</div>

<div class="row">
  <!-- /.col -->
  <div class="col-md-12">
    <button type="submit" id="sendForm" class="btn" <?=$disabled?>>เข้าสู่ระบบ</button>
  </div>
  <!-- <div class="col-md-12">
    <a href="../index" class="pull-left" style="margin-top:5px;color:#8A2BE2">ย้อนกลับ</a>
    <a href="../forget_pw" class="pull-right" style="margin-top:5px;">ลืมรหัสผ่าน</a>
  </div> -->
  <!-- /.col -->
</div>
