<?php


include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action       = isset($_POST['action'])?$_POST['action']:"";
$user         = isset($_POST['user'])?$_POST['user']:"";
$pass         = isset($_POST['pass'])?$_POST['pass']:"";
$tl_session   = isset($_POST['tl_session'])?$_POST['tl_session']:"";


$role_list    = "";

$typeStatus   = 0;
if($action == 'OTP'){

  $sqls   = "SELECT * FROM t_login WHERE tl_otp = '$pass' AND tl_session = '$tl_session' AND tl_date_end >= getdate()";
  $querys = DbQuery($sqls,null);
  $json   = json_decode($querys, true);
  $counts = $json['dataCount'];
  $rows   = $json['data'];

  if($counts == 1){
    $tl_id = $rows[0]['tl_id'];
    $user_id = $rows[0]['user_id'];
    $sql = "UPDATE t_login SET tl_date_login = getdate(), tl_status = '1' WHERE lm_id = '$tl_id'";
    DbQuery($sql,null);
    $typeStatus = 1;
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'OTP TIME UP')));
  }
}else{
  $pass = md5($pass);
  $sqls   = "SELECT * FROM t_user WHERE user_login = '$user' AND user_password = '$pass' and is_active = 'Y' and type_user  = 'ADMIN' ";
  //echo $sqls;
  $querys = DbQuery($sqls,null);
  $json   = json_decode($querys, true);
  $counts = $json['dataCount'];
  $rows   = $json['data'];
  if($counts == 1){
    $user_id  = $rows[0]['user_id'];
    $role_list = isset($rows[0]['role_list'])?$rows[0]['role_list']:'0';

    if($role_list != '0'){
      if(substr($role_list,0,1) == ",")
      {
        $role_list = substr($role_list,1);
      }

    }

    $rows[0]['date_login'] = date("d M. Y H:i");
    $typeStatus = 1;
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'User Or Password Not Macth')));
  }

}

if($typeStatus == 1){
  $ip = getRealIP();
  $dateNow  =  date('Y/m/d H:i:s');
  $sql      = "INSERT INTO t_log_login(user_id,date_login,ip)VALUES('$user_id','$dateNow','$ip')";
  //echo $sql;
  $query = DbQuery($sql,null);

  // $sqlAcc   = "SELECT listagg (page_list,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by page_list) page_list,
  //                     listagg (role_code,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by role_code) role_code,
  //                     listagg (is_insert,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by is_insert) is_insert,
  //                     listagg (is_update,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by is_update) is_update,
  //                     listagg (is_delete,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by is_delete) is_delete,
  //                     listagg (is_print,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by is_print) is_print,
  //                     listagg (is_import,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by is_import) is_import,
  //                     listagg (is_export,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by is_export) is_export,
  //                     listagg (is_approve,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by is_approve) is_approve,
  //                     listagg (is_cancel,',' ON OVERFLOW TRUNCATE WITH COUNT) within group (order by is_cancel) is_cancel
  //                     FROM t_role WHERE role_id in ($role_list)";

  $sqlAcc   = "SELECT GROUP_CONCAT(DISTINCT page_list ORDER BY page_list ASC SEPARATOR ',') as page_list,
                      GROUP_CONCAT(DISTINCT role_code ORDER BY role_code ASC SEPARATOR ',') as role_code,
                      GROUP_CONCAT(DISTINCT is_insert ORDER BY is_insert ASC SEPARATOR ',') as is_insert,
                      GROUP_CONCAT(DISTINCT is_update ORDER BY is_update ASC SEPARATOR ',') as is_update,
                      GROUP_CONCAT(DISTINCT is_delete ORDER BY is_delete ASC SEPARATOR ',') as is_delete,
                      GROUP_CONCAT(DISTINCT is_print ORDER BY is_print ASC SEPARATOR ',') as is_print,
                      GROUP_CONCAT(DISTINCT is_import ORDER BY is_import ASC SEPARATOR ',') as is_import,
                      GROUP_CONCAT(DISTINCT is_export ORDER BY is_export ASC SEPARATOR ',') as is_export,
                      GROUP_CONCAT(DISTINCT is_approve ORDER BY is_approve ASC SEPARATOR ',') as is_approve,
                      GROUP_CONCAT(DISTINCT is_cancel ORDER BY is_cancel ASC SEPARATOR ',') as is_cancel
                      FROM t_role WHERE role_id in ($role_list)";

  $queryAcc  = DbQuery($sqlAcc,null);
  $jsonAcc   = json_decode($queryAcc, true);
  $rowAcc    = $jsonAcc['data'];
  //echo $sqlAcc;
  $roleUser = $rowAcc[0];


  //print_r($roleUser);

  $_SESSION['member']     = $rows;
  //$_SESSION['branchCode'] = $rows[0]['branch_code'];


//---------------------------Menu-----------------------------------------
  $REQUEST_URI  = $_SERVER["REQUEST_URI"];
  $strPage      = $roleUser['page_list'];

  $strArr = explode("/",$REQUEST_URI);
  $inx    = count($strArr) - 2;
  //$page_path = substr(str_replace($baseurl,'',$REQUEST_URI) , 0,-1);
  $page_path = $strArr[$inx];

  $arrPage = array_unique(explode(",",$strPage));
  sort($arrPage);
  $arrPage = implode(",",$arrPage);

  if($arrPage[0] == ','){
    $arrPage = substr($arrPage,1);
  }

  if(strpos($roleUser['is_insert'],"Y") !== false)
  {
    $roleUser['is_insert'] = 'Y';
  }

  if(strpos($roleUser['is_update'],"Y") !== false)
  {
    $roleUser['is_update'] = 'Y';
  }

  if(strpos($roleUser['is_delete'],"Y") !== false)
  {
    $roleUser['is_delete'] = 'Y';
  }

  if(strpos($roleUser['is_print'],"Y") !== false)
  {
    $roleUser['is_print'] = 'Y';
  }

  if(strpos($roleUser['is_import'],"Y") !== false)
  {
    $roleUser['is_import'] = 'Y';
  }

  if(strpos($roleUser['is_approve'],"Y") !== false)
  {
    $roleUser['is_approve'] = 'Y';
  }

  if(strpos($roleUser['is_export'],"Y") !== false)
  {
    $roleUser['is_export'] = 'Y';
  }

  if(strpos($roleUser['is_cancel'],"Y") !== false)
  {
    $roleUser['is_cancel'] = 'Y';
  }

  $roleUser['page_list'] = $arrPage;
  $_SESSION['ROLE_USER']  =  $roleUser;
  //print_r($roleUser);
  // echo $arrPage;

  $_SESSION['MENU']      = getMunu($arrPage);
  //---------------------------------------------------------------------
  //$_SESSION['branch'] = "";
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}



?>
