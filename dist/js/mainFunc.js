$( document ).ajaxStart(function() {
  $(".loadingImg").removeClass('none');
});

$( document ).ajaxStop(function() {
  $(".loadingImg").addClass('none');
});

function showProcessbar(){
  // $.smkProgressBar({
  //   element:'body',
  //   status:'start',
  //   bgColor: '#000',
  //   barColor: '#fff',
  //   content: 'Loading...'
  // });
  // setTimeout(function(){
  //   $.smkProgressBar({
  //     status:'end'
  //   });
  // }, 1000);
}

function home()
{
    postURL("../home/");
}

function dateThDMY(d){
  var date  = new Date(d);
  var dd    = ("0" + (date.getDate())).slice(-2);
  var mm    = ("0" + (date.getMonth() +　1)).slice(-2);
  var yyyy  = date.getFullYear();
  if(yyyy<2450){
      yyyy += 543;
  }

  var today = dd+'/'+mm+'/'+yyyy;
  return today;
}


function dateThDMYHIS(d){
  var date  = new Date(d);
  var dd    = ("0" + (date.getDate())).slice(-2);
  var mm    = ("0" + (date.getMonth() +　1)).slice(-2);
  var yyyy  = date.getFullYear();
  if(yyyy<2450){
      yyyy += 543;
  }

  var dateTh = dd+'/'+mm+'/'+yyyy+' '+date.getHours()+':'+date.getMinutes()+':'+date.getSeconds();
  return dateTh;
}


function postURL_blank(url) {
 var form = document.createElement("FORM");
 form.method = "POST";
 form.target = "_blank";
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}


function chkNumber(ele)
	{
	var vchar = String.fromCharCode(event.keyCode);
	if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
	ele.onKeyPress=vchar;
	}

function GenPass(type=0,num=8) {

  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  if(type == 0){
    text = 'P@ss1234';
  }else{
    for (var i = 0; i < num; i++){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
  }
  return text;
}

function showSlidebar(){
  $.get( "../../inc/function/slidebar.php" )
  .done(function( data ) {
    $("#showSlidebar").html(data);
  });
}

function readURL(input,values) {

  if (input.files) {
      var filesAmount = input.files.length;
      $('#'+values).html('');
      for (i = 0; i < filesAmount; i++) {
          checkTypeImage(input,values);
          var reader = new FileReader();
          reader.onload = function(event) {
              $($.parseHTML("<img width='100'>")).attr('src', event.target.result).appendTo('#'+values);
          }
          reader.readAsDataURL(input.files[i]);
      }

  }
}

function postURLBlank(url, multipart) {
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.target="_blank";
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function gotoPage(url){
  window.location.href = url;
}

function postURL(url, multipart) {
  //alert("url:" + url);
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function isNumeric(num){
    if(isNaN(num) || num == "")
    {
      num = 0;
    }
    return parseFloat(num);
}

function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function checkTypeImage(input,values){
  var file = input.files[0];
  var fileType = file["type"];
  var validImageTypes = ["image/gif", "image/jpeg", "image/png"];
  if ($.inArray(fileType, validImageTypes) < 0) {
      alert('ประเภทไฟล์ไม่ถูกต้อง');
      $('#'+values).html('');
      input.value = '';
  }
}

function dateThToEn(date,format,delimiter)
{
    var formatLowerCase=format.toLowerCase();
    var formatItems=formatLowerCase.split(delimiter);
    var dateItems=date.split(delimiter);
    var monthIndex=formatItems.indexOf("mm");
    var dayIndex=formatItems.indexOf("dd");
    var yearIndex=formatItems.indexOf("yyyy");
    var month=parseInt(dateItems[monthIndex]);
    month-=1;

    var dateth = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
    var yearth = dateth.getFullYear()
    if( yearth > 2450){
      yearth -= 543;
    }
    var dateEN = yearth + "/" + ("0" + (dateth.getMonth() +　1)).slice(-2) + "/" + ("0" + (dateth.getDate())).slice(-2);
    return dateEN;
}

function dateThLinkage(date)
{
   var dateTh = "";
   date =  date.toString();
   // console.log(date + " ," + date.length);
   if(date != "" && date.length == 8){
     var monthIndex  = date.substring(4, 6);
     var dayIndex    = date.substring(6, 8);
     var yearIndex   = date.substring(0, 4);

     var month = parseInt(monthIndex);
     dateTh = dayIndex + " " + monthThai(month) + " " + yearIndex;
   }

    return dateTh
}

function monthThai(month){
  var arr = ['','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];
  return arr[month];
}

function monthThaiFull(month){
  var arr = ['','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
  return arr[month];
}
