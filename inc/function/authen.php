<?php
if(!isset($_SESSION))
{
    session_start();
}

function getHostPath()
{
  $sub = "";
  $http = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
  $hosName =  $http."://$_SERVER[HTTP_HOST]/";
  $sub  = $_SERVER['REQUEST_URI'];
  $subs = explode("/", $sub);
  //print_r($subs);
  if(count($subs) > 2 && $http == 'http'){
    $hosName .= $subs[1]."/";
  }

  return $hosName;
}

$urlLoggin = getHostPath()."pages/login/";

//echo $urlLoggin;

if(!isset($_SESSION['ROLE_USER'])){
  exit("<script>window.location='$urlLoggin'</script>");
}

if(!isset($_SESSION['member'])){
  exit("<script>window.location='$urlLoggin'</script>");
}

?>
