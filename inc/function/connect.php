<?php
if(!isset($_SESSION))
{
    session_start();
}
date_default_timezone_set("Asia/Bangkok");

function check(){
  echo 1;
}

function DbConnect(){
  // $db = "mysql:host=localhost;dbname=ami_db";
  $db = "mysql:host=onesittichok.co.th:3306;dbname=ami_db";
  // $db = "mysql:host=172.3.99.99;dbname=ami_db";
  $user = "ami_db";
  $pass = "p@ssw0rd";

  // $db = "mysql:host=localhost;dbname=ami_db";
  // $user = "root";
  // $pass = "root";

  $options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
  try{
    return new PDO($db,$user,$pass, $options);
  }catch (Exception $e){
    return $e->getMessage();
  }
}

//
// function DbConnect(){
//     try
//     {
//         // $link = 'http://mbsolutions.';
//         $connOracle = new PDO('oci:dbname=192.168.2.22:1521/pfit;charset=utf8', 'sportscidev', 'sportscidev');
//         // $connOracle = new PDO('oci:dbname=mbsolutions.ddns.net:9087/pfit;charset=utf8', 'sportscidev', 'sportscidev');
//         $connOracle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//         $connOracle->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
//         return $connOracle;
//     } catch(PDOException $e) {
//        // echo 'ERROR: ' . $e;
//        $e->getMessage();
//     }
// }



function DbQuery($sql,$ParamData){
  try {
    $obj  = DbConnect();
    //echo $obj;
    $stm = $obj->prepare($sql);
    if($ParamData != null){
      for($x=1; $x<=count($ParamData); $x++)
      {
        $stm->bindParam($x,$ParamData[$x-1]);
      }
    }
    $stm->execute();
    $arr = $stm->errorInfo();
    $id = $obj->lastInsertId();
    $num = 0;
    while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
      $data['data'][] = $row;
      $num++;
    }
    $data['errorInfo'] = $arr;
    $data['dataCount'] = $num;
    $data['id'] = $id;

    if(isset($data)){
      if($num == 0){
        $data['data'] = "";
      }
      $data['status'] = 'success';
      return json_encode($data);
    }else{
      $data['status'] = 'Fail';
      $data['data'] = "";
      return json_encode($data);
    }
  } catch (Exception $e) {
    $data['dataCount'] = 0;
    $data['status'] = 'Fail';
    $data['data'] = "";
    return  json_encode($data);
    $e->getTraceAsString();
  }
}

function DbInsert($sql,$ParamData){
  $obj = DbConnect();
  //$sql  = str_replace("now()","GETDATE()",$sql);
  $stm = $obj->prepare($sql);
  try {
    $stm->execute();
    $id = $obj->lastInsertId();
    return json_encode($id);
  } catch (Exception $e) {
    return  json_encode(array());
    $e->getTraceAsString();
  }
}
?>
