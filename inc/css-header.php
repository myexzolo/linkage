<?php
  include('function/connect.php');
?>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="../../dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="../../dist/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="../../dist/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet" href="../../dist/css/jquery-jvectormap.css">
<!-- select2 -->
<link rel="stylesheet" href="../../dist/css/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
<!-- dataTables -->
<link rel="stylesheet" href="../../dist/css/dataTables.bootstrap.min.css">
<!-- smoke -->
<link rel="stylesheet" href="../../dist/css/smoke.css">
<!-- smoke -->
<link rel="stylesheet" href="../../dist/css/main.css">
<!-- owl -->
<link rel="stylesheet" href="../../dist/css/owl.carousel.css">
<link rel="stylesheet" href="../../dist/css/lightbox.min.css">

<link rel="stylesheet" href="../../dist/css/datepicker.css">

<link rel="stylesheet" href="../../dist/css/daterangepicker.css">

<link rel="stylesheet" href="../../dist/css/dropzone.css" />

<!-- datetimepicker -->
<link rel="stylesheet" href="../../dist/css/bootstrap-datetimepicker.css">

<link rel="stylesheet" href="../../dist/css/bootstrap-select.css">

<link rel="stylesheet" href="../../dist/css/bootstrap-colorpicker.min.css">

<link rel="stylesheet" href="../../dist/css/jquery-jvectormap.css">

<link rel="stylesheet" href="../../dist/css/bootstrap-year-calendar.min.css">

<link rel="stylesheet" href="../../dist/css/cute-alert.css">



<div class="loadingImg none">
  <img src="../../image/loading.svg" alt="Loading..." style="width:100px;">
</div>

<style>
body {
    font-family: "CSChatThai";
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    font-size: 20px;
}
html{
  font-family: "CSChatThai";
}

.h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 , b , a{
  font-family: "CSChatThai" ;
}

.number {
  font-family: "CSChatThai";
}

@font-face {
    font-family: "THSarabun";
    src: url("../../font/THSarabun/THSarabun.ttf");
}

@font-face {
    font-family: "Kanit";
    src: url("../../font/Kanit/Kanit-Light.ttf");
}

@font-face {
    font-family: "rsu";
    src: url("../../font/rsufont/RSU_Regular.ttf");
}

@font-face {
    font-family: "supermarket";
    src: url("../../font/supermarket/RSU_Regular.ttf");
}


@font-face {
    font-family: "CSChatThai";
    src: url("../../font/CSChatThai/CSChatThai.ttf");
}
</style>
