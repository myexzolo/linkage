<?php
if(!isset($_SESSION['member'])){
  exit("<script>window.location='../../pages/login/'</script>");
}
$USERIMG = isset($_SESSION['member'][0]['user_img'])?"../../image/user/".$_SESSION['member'][0]['user_img']:"../../image/user.png";
?>
<style>
.cut-text {
  text-overflow: ellipsis;
  overflow: hidden;
  width: 240px;
  white-space: nowrap;
  margin: auto;
}
</style>
<header class="main-header">

  <!-- Logo -->
  <a href="../home" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><img src="../../image/logo.png" alt="builtland" style="height:40px;"></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">
      ระบบฐานข้อมูลประชาชน
    </span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu" style="background:#303030">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        <!-- <li class="dropdown messages-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-envelope-o"></i>
            <span class="label label-success">4</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">You have 4 messages</li>
            <li>
              <ul class="menu">
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="<?= $USERIMG ?>" onerror="this.onerror='';this.src='../../image/user.png'" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      Support Team
                      <small><i class="fa fa-clock-o"></i> 5 mins</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="../../dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      AdminLTE Design Team
                      <small><i class="fa fa-clock-o"></i> 2 hours</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="../../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      Developers
                      <small><i class="fa fa-clock-o"></i> Today</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="../../dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      Sales Department
                      <small><i class="fa fa-clock-o"></i> Yesterday</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li>
                <li>
                  <a href="#">
                    <div class="pull-left">
                      <img src="../../dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                    </div>
                    <h4>
                      Reviewers
                      <small><i class="fa fa-clock-o"></i> 2 days</small>
                    </h4>
                    <p>Why not buy a new awesome theme?</p>
                  </a>
                </li>
              </ul>
            </li>
            <li class="footer"><a href="#">See All Messages</a></li>
          </ul>
        </li> -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?= $USERIMG ?>" onerror="this.onerror='';this.src='../../image/user.png'" class="user-image" alt="User Image">
            <span class="hidden-xs cut-text"><?= $_SESSION['member'][0]['user_name']?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?= $USERIMG ?>" onerror="this.onerror='';this.src='../../image/user.png'" class="img-circle" alt="User Image">
              <div class="cut-text" style="font-size: 20px;color:#fff;margin-top: 10px;">
                <?= $_SESSION['member'][0]['user_name']?>
              </div>
              <p>
                <small>Member since <?= $_SESSION['member'][0]['date_login']?></small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <!-- <div class="pull-left">
                <a onclick="resetPassword()" class="btn btn-default btn-flat"><i class="fa  fa-key"></i> เปลี่ยนรหัสผ่าน</a>
              </div> -->
              <div class="pull-right">
                <a onclick="logout()" class="btn btn-default btn-flat"><i class="fa fa-power-off"></i> ออกจากระบบ</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
