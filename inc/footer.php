<div class="modal fade" id="myModalResetPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Reset Password</h4>
      </div>
      <form id="formResetPwHeader" novalidate enctype="multipart/form-data">
      <!-- <form id="formAddModule" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" action="ajax/AEDVendors.php" method="post"> -->
      <div id="form-rw"></div>
      </form>
    </div>
  </div>
</div>

<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.0.1
  </div>
  <strong>Copyright &copy; 2020 บริษัท ขนส่ง จำกัด </strong> All rights
  reserved.
</footer>

<div class="control-sidebar-bg"></div>
