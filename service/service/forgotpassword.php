<?php

include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

date_default_timezone_set('Asia/Bangkok');
require '../PHPMailer/PHPMailerAutoload.php';

$status = 200;
$message = 'success';

$email     = isset($request['email'])?$request['email']:"";

$sql   = "SELECT *
          FROM t_user u
          LEFT JOIN pfit_t_member pm ON u.user_id = pm.user_id
          where pm.email = '$email'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$count      = $json['dataCount'];
if($count > 0){
  $row =   $json['data'][0];

  $member_name      = isset($row['member_name'])?$row['member_name']:"";
  $member_lname     = isset($row['member_lname'])?$row['member_lname']:"";
  $department_name  = isset($row['department_name'])?$row['department_name']:"";
  $email            = isset($row['email'])?$row['email']:"";
  $user_login       = isset($row['user_login'])?$row['user_login']:"";
  $password         = isset($row['pw'])?$row['pw']:"";

  $content =  "รหัสผู้ใช้งานระบบจัดเก็บและรายงานผลการทดสอบสมรรถภาพทางกาย (ลืมรหัสผ่าน)<br>".
              "ชื่อ - สกุล : ".$member_name." ".$member_lname."<br />".
              "ชื่อหน่วยงาน/สังกัด : ".$department_name."<br />".
              "Username : $user_login <br />".
              "Password : $password";
  if($email != ""){
    
    $arr['email']    = $email;
    $arr['title']    = 'รหัสผู้ใช้งานระบบ กรมพลศึกษา';
    $arr['content']  = $content;

    if(mailsend2($arr) == 200){
      $status = 200;
      $message = 'send password your email is registered. Please check your Email';
    }else{
      $status = 401;
      $message = 'mail fail';
    }
  }
}else{
  $status = 401;
  $message = 'ไม่พบข้อมูล Email นี้ในการลงทะเบียน ไม่ถูกต้อง';
}

?>
